//
//  AppDelegate.swift
//  ProHardwareDirect
//
//  Created by Yogita on 16/12/16.
//  Copyright © 2016 Mobrill. All rights reserved.
//

import UIKit
import eWAYPaymentsSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,REFrostedViewControllerDelegate {

    var window: UIWindow?
    var navigationController : UINavigationController?
    
    var appDelegateMenuData : DataObject!
    var appDelegateHomeData : DataObject!
    
    func appDelegate () -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let navigationController = UINavigationController()
        let homeViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        navigationController.viewControllers = [homeViewController]
        
        Magento.service().renewSession()
        
        let menuViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let frostedViewController = REFrostedViewController(contentViewController: navigationController, menuViewController: menuViewController )
        frostedViewController?.direction = .left
        frostedViewController?.delegate=self
        self.window!.rootViewController = frostedViewController;
        self.window?.makeKeyAndVisible()
     
        let Endpoint = "https://api.sandbox.ewaypayments.com/"
        let PublicAPIKey = "epk-BCC78E94-D284-441E-B6A0-25AD0D1FB1DB" //"epk-3AFAB78E-92C2-41C7-BEE4-6E811ADACD16"

        (RapidAPI.sharedManager() as! RapidAPI).rapidEndpoint = Endpoint as String!
        (RapidAPI.sharedManager() as! RapidAPI).publicAPIKey = PublicAPIKey as String!

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func navigationConfiguration() -> Void {
         self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 18)!,NSForegroundColorAttributeName    : UIColor.white ]
        let attributes = [NSFontAttributeName: UIFont (name: "Quicksand-Bold", size: 17)!];
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal);
        UINavigationBar.appearance().titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 17)!, NSForegroundColorAttributeName : UIColor.white]
    }
}


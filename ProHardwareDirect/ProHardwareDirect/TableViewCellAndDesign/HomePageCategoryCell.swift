//
//  HomePageCategoryCell.swift
//  ToDool
//
//  Created by Pranav on 9/12/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class HomePageCategoryCell : UITableViewCell
{
    @IBOutlet var backImage : UIImageView?
    @IBOutlet var foreText : UILabel?

    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var horizontalText: NSLayoutConstraint!
}

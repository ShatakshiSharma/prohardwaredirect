//
//  MyOrdersTableViewCell.swift
//  ToDool
//
//  Created by Shatakshi Sharma on 07/11/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class MyOrdersTableViewCell: UITableViewCell
{

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var orderNumb: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var orderStatus: UILabel!
    @IBOutlet weak var orderStatusImage: UIImageView!
    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var sku: UILabel!
}

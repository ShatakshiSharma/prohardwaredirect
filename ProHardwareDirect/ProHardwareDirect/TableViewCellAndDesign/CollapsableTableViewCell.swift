//
//  CollapsableTableViewCell.swift
//  ToDool
//
//  Created by Pranav on 9/12/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class CollapsableTableViewCell : UITableViewCell
{
    @IBOutlet weak var button: UIImageView!
    @IBOutlet weak var customTitleLabel: UILabel!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var leadingSpace: NSLayoutConstraint!
    var increaseSize : Bool = false

    
    override func awakeFromNib() {
        selectedBackgroundView? = UIView.init()
        selectedBackgroundView?.backgroundColor = UIColor.clear
    }
    
    func close( )
    {
        UIView.animate(withDuration: 0.3) {
            self.button.transform = CGAffineTransform.identity
        }
    }
    
    
    func open( )
    {
        UIView.animate(withDuration: 0.3) { 
            self.button.transform = CGAffineTransform.init(rotationAngle: CGFloat( -M_PI_2 ) )
        }
    }
    
    func setup(withTitle title: String, detailsText: String, level : Int, additionalButtonHidden: Bool) {
        customTitleLabel.text = title
        
        let left = -20.0 * CGFloat(level)
        
        self.leadingSpace.constant = left

        }

}

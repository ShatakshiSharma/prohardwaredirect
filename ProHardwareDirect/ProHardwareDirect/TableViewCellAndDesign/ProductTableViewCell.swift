//
//  ProductTableViewCell.swift
//  ToDool
//
//  Created by Yogita on 20/09/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//


import UIKit

class ProductTableViewCell : UITableViewCell
{

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var originalPrice: UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var cartImage: UIImageView!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var cartView: UIView!
}

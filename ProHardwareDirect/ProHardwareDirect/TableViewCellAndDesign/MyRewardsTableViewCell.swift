//
//  CategoryTableViewCell.swift
//  ToDool
//
//  Created by Pranav on 9/14/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class MyRewardsTableViewCell : UITableViewCell
{
    @IBOutlet weak var Rewardtitle : UILabel!
    @IBOutlet weak var expDate : UILabel!
    @IBOutlet weak var points : UILabel!
    @IBOutlet weak var rewardStatusImage : UIImageView!
    @IBOutlet weak var rewardStatus : UILabel!
}

//
//  MyCartTableViewCell.swift
//  ToDool
//
//  Created by Yogita on 20/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class FilterTableViewCell : UITableViewCell
{
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var selectedCategory: UILabel!
    @IBOutlet weak var downImage: UIImageView!
    
    func close( )
    {
        UIView.animate(withDuration: 0.3) {
            self.downImage.transform = CGAffineTransform.identity
        }
    }
    
    
    func open( )
    {
        UIView.animate(withDuration: 0.3) {
            self.downImage.transform = CGAffineTransform.init(rotationAngle: CGFloat( M_PI_2 ) )
        }
    }
    
    
    
}

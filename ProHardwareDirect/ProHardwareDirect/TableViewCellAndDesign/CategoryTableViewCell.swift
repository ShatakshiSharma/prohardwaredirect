//
//  CategoryTableViewCell.swift
//  ToDool
//
//  Created by Pranav on 9/14/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class CategoryTableViewCell : UITableViewCell
{
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var icon : UIImageView!
}

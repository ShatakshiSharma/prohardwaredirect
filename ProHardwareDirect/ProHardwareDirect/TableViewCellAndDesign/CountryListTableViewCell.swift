//
// CountryListTableViewCell.swift
//  ToDool
//
//  Created by Pranay on 21/09/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class CountryListTableViewCell : UITableViewCell
{
    @IBOutlet weak var countryName : UILabel!
}

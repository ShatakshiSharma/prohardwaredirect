//
//  MyCMyCartTableViewCellartTableViewCell.swift
//  ToDool
//
//  Created by Yogita on 20/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class MyCartTableViewCell : UITableViewCell
{
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var originalPrice: UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var productNumber: UILabel!
    @IBOutlet weak var qtyTextField: UITextField!
    @IBOutlet weak var earningLabel: UILabel!
    @IBOutlet weak var detailBtn: UIButton!
    @IBOutlet weak var qtyBtn: UIButton!
    
}

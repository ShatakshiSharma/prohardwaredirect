//
//  AddressBookTableViewCell.swift
//  ToDool
//
//  Created by Yogita on 21/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class AddressBookTableViewCell : UITableViewCell
{
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    
}

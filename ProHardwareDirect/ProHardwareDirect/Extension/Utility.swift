//
//  Utility.swift
//  ToDool
//
//  Created by Pranav on 9/13/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//
import UIKit

class Utility
{    
    struct DateFormat
    {
        static let createdAtFormat = "yyyy-MM-dd'T'HH:mm:ss"
        static let updatedAtFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    
    typealias RestoreNavigationBar = ( Bool?, UIColor?, UIColor?, UIImage?, UIImage?, UIColor? ) -> Void
    
    
    class func validateEmail( _ email : String ) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate( with: email )
    }
    
    class func validateMobile( _ mobile : String ) -> Bool
    {
        let mobRegEx = "[0-9]{6,14}$";
        let mobTest = NSPredicate(format:"SELF MATCHES %@", mobRegEx)
        return mobTest.evaluate( with: mobile )
    }
    
    class func validatePassword( _ password : String ) -> Bool
    {
        return password.characters.count >= 6
    }
    
    class func isEmpty( _ string : String ) -> Bool
    {
        return string.characters.count == 0
    }
    
    class func statusBarHeight( ) -> CGFloat
    {
        return UIApplication.shared.statusBarFrame.size.height
    }
    
    class func navigationBarHeight( controller : UIViewController ) -> CGFloat
    {
        return controller.topLayoutGuide.length - Utility.statusBarHeight()
    }
    
    class func totalNavigationBarHeight( controller : UIViewController ) -> CGFloat
    {
        return controller.topLayoutGuide.length
    }
    
     class func navigationControllerColor( ) -> Void
    {
        let attributes = [NSFontAttributeName: UIFont (name: "Quicksand-Bold", size: 17)!];
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal);
        UINavigationBar.appearance().titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 17)!, NSForegroundColorAttributeName : UIColor.white]
        UINavigationBar.appearance().barTintColor = navigationColor

        /*
        let navigationBarIsTranslucent = controller.navigationController?.navigationBar.isTranslucent
        controller.navigationController?.navigationBar.isTranslucent = true
        let navigationControllerBackgroundColor = controller.navigationController?.view.backgroundColor
        controller.navigationController?.view.backgroundColor = navigationBarColor
        let navigationBarBackgroundColor = controller.navigationController?.navigationBar.backgroundColor
        controller.navigationController?.navigationBar.backgroundColor = navigationBarColor
        let navigationBarBackgroundImage = controller.navigationController?.navigationBar.backgroundImage(for: .default )
        controller.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        let navigationBarShadowImage = controller.navigationController?.navigationBar.shadowImage
        controller.navigationController?.navigationBar.shadowImage = UIImage()
    
        let result : RestoreNavigationBar = { ( it, nbbc, ncbc, nbbi, nbsi, sbc ) in
            if let it = it{
                controller.navigationController?.navigationBar.isTranslucent = it
            }
            if let ncbc = ncbc{
                controller.navigationController?.view.backgroundColor = ncbc
            }
            if let nbbc = nbbc {
                controller.navigationController?.navigationBar.backgroundColor = nbbc
            }
            if let nbbi = nbbi {
                controller.navigationController?.navigationBar.setBackgroundImage( nbbi, for: .default)
            }
            if let nbsi = nbsi {
                controller.navigationController?.navigationBar.shadowImage = nbsi
            }
            if let sbc = sbc {
                let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                statusBar.backgroundColor = sbc
            }
            
        }*/
        }
    
    
    class func textFieldBorderColor(_ view:Any, color:UIColor) -> Void {
        (view as AnyObject).layer.cornerRadius=2.0
        (view as AnyObject).layer.masksToBounds = true
        (view as AnyObject).layer.borderColor = color.cgColor
        (view as AnyObject).layer.borderWidth = 1.0
    }
    
    
    class func addPaddingToTextField (textfield:UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height:textfield.frame.height))
        textfield.leftView = paddingView
        textfield.leftViewMode = UITextFieldViewMode.always
        
    }
    
    class func toInteger( value : Any, defaultValue : Int = -1 ) -> Int
    {
        if let val = value as? NSNumber
        {
            return val.intValue
        }
        else if let val = value as? Int
        {
            return val
        }
        else if let val = value as? Int8
        {
            return Int( val )
        }
        else if let val = value as? Int16
        {
            return Int( val )
        }
        else if let val = value as? Int32
        {
            return Int( val )
        }
        else if let val = value as? Int64
        {
            return Int( val )
        }
        else if let val = value as? UInt
        {
            return Int( val )
        }
        else if let val = value as? UInt8
        {
            return Int( val )
        }
        else if let val = value as? UInt16
        {
            return Int( val )
        }
        else if let val = value as? UInt32
        {
            return Int( val )
        }
        else if let val = value as? UInt64
        {
            return Int( val )
        }
        else if let val = value as? Float
        {
            return Int( val )
        }
        else if let val = value as? Double
        {
            return Int( val )
        }
        else if let val = value as? String
        {
            if let val = Int( val )
            {
                return val
            }
            else
            {
                return defaultValue
            }
        }
        else if let val = value as? Bool
        {
            return Int( val ? 1 : 0 )
        }
        else
        {
            return defaultValue
        }
    }
    
    class func addProductToCart(productId : String, quantity : Int, name : String, sku : String, discountedPrice : String, originalPrice : String, imgUrl : String)
    {
        let productDict = NSMutableDictionary()
        var cartArray : [NSDictionary] = []
        productDict.setValue(productId, forKey: "productId")
        productDict.setValue(quantity, forKey: "quantity")
        productDict.setValue(name, forKey: "name")
        productDict.setValue(sku, forKey: "sku")
        productDict.setValue(originalPrice, forKey: "price")
        productDict.setValue(discountedPrice, forKey: "you_pay")
        productDict.setValue(imgUrl, forKey: "img_url")

        if(UserDefaults.standard.object(forKey: "cartArray") != nil){
         cartArray =  UserDefaults.standard.object(forKey: "cartArray") as! [NSDictionary]
        }
        if(cartArray.count > 0)
        {
        for i in 0...cartArray.count-1
        {
            let product = cartArray[i]
            if (product.value(forKey: "productId") as! String == productId)
            {
                //let productQuantity = quantity + Int(product.value(forKey: "quantity") as! String)!
                //let cartProduct = NSMutableDictionary()
                //cartProduct.setValue(productId, forKey: "productId")
                //cartProduct.setValue(quantity, forKey: "quantity")
                productDict.setValue(quantity, forKey: "quantity")
                cartArray.remove(at: i)
                cartArray.insert(productDict, at: i)
                UserDefaults.standard.set(cartArray, forKey: "cartArray")
                break
            }
            if (i == cartArray.count-1)
            {
                cartArray.append(productDict)
                UserDefaults.standard.set(cartArray, forKey: "cartArray")
            }
        }
        }
        
        else
        {
           cartArray.append(productDict)
           UserDefaults.standard.set(cartArray, forKey: "cartArray")

        }
        UserDefaults.standard.set(cartArray, forKey: "cartArray")
        let array = UserDefaults.standard.value(forKey: "cartArray") as! [NSDictionary]
        UserDefaults.standard.set(array.count, forKey: "cartCount")
        NotificationCenter.default.post(name: .cartCount, object: nil)

        for i in 0...cartArray.count-1
        {
            let array = UserDefaults.standard.value(forKey: "cartArray") as! [NSDictionary]
            print(array[i])
        }
        
    }
    
    class func deleteCartProduct(productId : String)
    {
        var cartArray : [NSDictionary] = []
        if(UserDefaults.standard.object(forKey: "cartArray") != nil){
            cartArray =  UserDefaults.standard.object(forKey: "cartArray") as! [NSDictionary]
        }
        if(cartArray.count > 0)
        {
            for i in 0...cartArray.count-1
            {
                let product = cartArray[i]
                if (product.value(forKey: "productId") as! String == productId)
                {
                    cartArray.remove(at: i)
                    UserDefaults.standard.set(cartArray, forKey: "cartArray")
                    break
                }
            }
        }
        let array = UserDefaults.standard.value(forKey: "cartArray") as! [NSDictionary]
        UserDefaults.standard.set(array.count, forKey: "cartCount")
        NotificationCenter.default.post(name: .cartCount, object: nil)
  
    }
    class func getTotalPrice() -> Float
    {
        var cartArray : [NSDictionary] = []
        var price : Float = 0.0
        
        if(UserDefaults.standard.object(forKey: "cartArray") != nil){
            cartArray =  UserDefaults.standard.object(forKey: "cartArray") as! [NSDictionary]
        }
        
        if(cartArray.count > 0)
        {
            for i in 0...cartArray.count-1
            {
                let product = cartArray[i]
                price = Float(product.value(forKey: "price") as! String)! + price
            }
        }
        return price
    }

    
    class func addItemQuantiity(productId : String, quantity: Int)
    {
        var cartArray : [NSDictionary] = []

        if(UserDefaults.standard.object(forKey: "cartArray") != nil){
            cartArray =  UserDefaults.standard.object(forKey: "cartArray") as! [NSDictionary]
        }
        if(cartArray.count > 0)
        {
            for i in 0...cartArray.count-1
            {
                let product = cartArray[i]
                if (product.value(forKey: "productId") as! String == productId)
                {
                    product.setValue(quantity, forKey: "quantity")
                    cartArray[i] = product
                    UserDefaults.standard.set(cartArray, forKey: "cartArray")
                    break
                }
            }
        }
    }
}

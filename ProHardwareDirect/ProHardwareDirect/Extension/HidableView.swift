//
//  HidableView.swift
//  ToDool
//
//  Created by Pranav Prakash on 14/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


private var hideableUIViewIsVisibleKey: UInt8 = 0
private var hideableUIViewConstraintsKey: UInt8 = 0
private var hideableUIViewParentConstraintsKey: UInt8 = 0


extension UIView
{
    var isVisible : Bool
    {
        get
        {
            return ( objc_getAssociatedObject( self, &hideableUIViewIsVisibleKey ) as? NSNumber ?? NSNumber( value : true ) ).boolValue
        }
        set
        {
            let num = NSNumber( value : newValue )
            objc_setAssociatedObject( self, &hideableUIViewConstraintsKey, num, .OBJC_ASSOCIATION_RETAIN )
            if newValue == true
            {
                show( )
            }
            else
            {
                hide( )
            }
        }
    }
    
    
    private func hide()
    {
        self.isHidden = true
        if _backupConstraints.count == 0
        {
            if #available( iOS 8.0, * )
            {
                for constraint in self.constraints
                {
                    constraint.isActive = false
                }
            }
            else
            {
                for constraint in self.constraints
                {
                    _backupConstraints.append( constraint )
                }
                self.removeConstraints( _backupConstraints )
            }
        }
        if _backupParentConstraints.count == 0
        {
            if let parent = self.superview
            {
                if #available( iOS 8.0, * )
                {
                    for constraint in parent.constraints
                    {
                        if constraint.firstItem === self || constraint.secondItem === self
                        {
                            constraint.isActive = false
                        }
                    }
                }
                else
                {
                    for constraint in parent.constraints
                    {
                        if constraint.firstItem === self || constraint.secondItem === self
                        {
                            _backupParentConstraints.append( constraint )
                        }
                    }
                    parent.removeConstraints( _backupParentConstraints )
                }
            }
        }
    }
    
    
    private func show()
    {
        if _backupConstraints.count > 0
        {
            if #available( iOS 8.0, * )
            {
                for constraint in self.constraints
                {
                    constraint.isActive = true
                }
            }
            else
            {
                for constraint in _backupConstraints
                {
                    self.addConstraint( constraint )
                }
                _backupConstraints = []
            }
        }
        if _backupParentConstraints.count > 0
        {
            if let parent = self.superview
            {
                if #available( iOS 8.0, * )
                {
                    for constraint in parent.constraints
                    {
                        if constraint.firstItem === self || constraint.secondItem === self
                        {
                            constraint.isActive = false
                        }
                    }
                }
                else
                {
                    for constraint in _backupParentConstraints
                    {
                        if constraint.firstItem === self || constraint.secondItem === self
                        {
                            parent.addConstraint( constraint )
                        }
                    }
                    _backupParentConstraints = []
                }
            }
        }
        self.isHidden = false
    }
    
    
    private var _backupConstraints: [NSLayoutConstraint]
    {
        get
        {
            return objc_getAssociatedObject( self, &hideableUIViewConstraintsKey ) as? [ NSLayoutConstraint ] ?? []
        }
        set
        {
            objc_setAssociatedObject( self, &hideableUIViewConstraintsKey, newValue, .OBJC_ASSOCIATION_RETAIN )
        }
    }
    
    
    private var _backupParentConstraints: [NSLayoutConstraint]
    {
        get
        {
            return objc_getAssociatedObject( self, &hideableUIViewParentConstraintsKey ) as? [ NSLayoutConstraint ] ?? []
        }
        set
        {
            objc_setAssociatedObject( self, &hideableUIViewParentConstraintsKey, newValue, .OBJC_ASSOCIATION_RETAIN )
        }
    }
}

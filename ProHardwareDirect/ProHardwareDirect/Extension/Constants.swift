//
//  Constants.swift
//  ToDool
//
//  Created by Yogita on 21/11/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit
//--------------Colors--------------------//
let navigationColor =  UIColor.clear//UIColor( hex : 0x252525 )
let textfieldLineWhiteColor = UIColor(hex: 0xe6eaeb )
let textfieldLineBlackColor = UIColor(hex: 0xc0c0c0 )


//----------------UserDefaultKey-----------//
let k_userID = "UserID"
let k_userProfileDict = "UserProfileDict"
let k_userGroup = "UserGroup"
let k_quoteID = "QuoteID"


//---------------validations--------------//

let k_email_Invalid = "Please enter valid email"
let k_email_Empty = "Please enter your email"
let k_pass_length = "Password should be minimum 6 character long"
let k_pass_Empty = "Please enter a password"
let k_new_pass_Empty = "Please enter a new password"
let k_pass_2_Empty = "Please re-enter password"
let k_pass_Match = "Password doesn't match"
let k_current_Pass_Empty = "Please enter current password"
let k_new_Pass_Empty = "Please enter new password"
let k_Pass_Valid = "Please enter valid password"
let k_current_Pass_Valid = "Please enter valid current password"
let k_invalid_New_Pass = "Current and new password cannot be same"

let k_mob_Invalid = "Please enter valid mobile"
let k_mob_Empty = "Please enter your mobile"
let k_lastname_Empty = "Please enter  last name"
let k_lastname_Invalid = "Please enter valid last name"

let k_firstname_Empty = "Please enter  first name"
let k_firstname_Invalid = "Please enter valid first name"
//let k_firstname_length = "First Name should be min 2 & max 35 Character Length"
let k_group_Empty = "Please select a group"

let k_emailid_notExist = "Email id does not exist"
let k_forgotPassLink = "A link has been sent to your email id to reset your password."



//---------------Address validations--------------//
let k_country_Invalid = "Please enter valid country"
let k_country_Empty = "Please enter country"
let k_state_Invalid = "Please enter valid state"
let k_state_Empty = "Please enter state"
let k_postcode_Invalid = "Please enter valid postcode"
let k_postcode_Empty = "Please enter postcode"
let k_city_Invalid = "Please enter valid city"
let k_city_Empty = "Please enter city"
let k_address_Invalid = "Please enter valid address"
let k_address_Empty = "Please enter address"
let k_telephone_Invalid = "Please enter valid telephone number"
let k_telephone_Empty = "Please enter telephone number"

let k_billing_address_added = "You have already added billing address"
let k_shipping_address_added = "You have already added shipping address"
let k_billing_shipping_address_added = "You have already added billing & shipping address"

let k_please_login = "Please login to continue"
let k_logout_error = "Error while logging out"
let k_network_Error = "Network Error! Please try again."

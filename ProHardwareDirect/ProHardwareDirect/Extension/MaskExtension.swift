//
//  MaskExtension.swift
//  ToDool
//
//  Created by Pranav Prakash on 30/09/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


private var alertControllerKey: String = "alertControllerKey"
private var alertActivityIndicatorKey: String = "alertActivityIndicatorKey"


func associatedObject<ValueType>(
    base: AnyObject,
    key: String, result : inout ValueType? )
    -> ValueType? {
        let data = key.data(using: String.Encoding.utf8)!
        data.withUnsafeBytes {
            result = objc_getAssociatedObject(base, $0) as? ValueType
        }
        return result
}


func associateObject<ValueType>(
    base: AnyObject,
    key: String,
    value: ValueType) {
    let data = key.data(using: String.Encoding.utf8)!
    _ = data.withUnsafeBytes {
        objc_setAssociatedObject(base, $0, value, .OBJC_ASSOCIATION_RETAIN) as? ValueType
    }
}


class CustomAlertController : UIAlertController
{
    var activityIndicator : UIActivityIndicatorView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad( )
        view.tintColor = UIColor.black
        self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        if let ai = self.activityIndicator
        {
            ai.hidesWhenStopped = true
            ai.activityIndicatorViewStyle = .white
            view.addSubview( ai )
            view.addConstraint( NSLayoutConstraint( item: view, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: ai, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0 ) )
            view.addConstraint( NSLayoutConstraint( item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: ai, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 10 ) )
            ai.addConstraint( NSLayoutConstraint( item: ai, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 60 ) )
            ai.addConstraint( NSLayoutConstraint( item: ai, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 60 ) )
            ai.startAnimating();
        }
    }
}


extension UIViewController {
    private func alertController( message : String ) -> CustomAlertController
    {
        return CustomAlertController(title: nil, message: message, preferredStyle: .alert)
    }
    
    
    func mask( _ message : String, activityStyle : UIActivityIndicatorViewStyle = .white )
    {
        let ac = alertController(message: message );
        ac.activityIndicator?.activityIndicatorViewStyle = activityStyle
        self.present( ac, animated: true, completion: nil)
        associateObject(base: self, key: alertControllerKey, value: ac )
    }
    
    
    func unmask( )
    {
        var alert : CustomAlertController?
        _ = associatedObject(base: self, key: alertControllerKey, result : &alert );
        alert?.dismiss(animated: true, completion: nil )
    }
}

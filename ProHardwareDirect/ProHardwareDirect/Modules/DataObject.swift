//
//  DataObject.swift
//  RATreeViewExamples
//
//  Created by Rafal Augustyniak on 22/11/15.
//  Copyright © 2015 com.Augustyniak. All rights reserved.
//

import Foundation


class DataObject
{

    let name : String
    let identifier : Int64
    
    var parent : DataObject?
        {
        get
        {
            return _parent;
        }
        set( value )
        {
            _parent = value
            if let p = _parent
            {
                _level = p.level + 1
            }
            var cp : Int = 0
            for data in children
            {
                data._level = _level + 1
                data._position = cp;
                cp += 1
            }
        }
    }
    
    var level : Int
    {
        return _level
    }
    
    
    var position : Int
    {
        return _position
    }
    
    
    var rootPosition : Int
    {
        if let p = _parent
        {
            return p.position + self.position
        }
        return self.position
    }
    
    
    init(_ name : String, _ identifier : Int64 = 0,  _ children: [DataObject] = [ ], _ parent : DataObject? = nil, _ position: Int = 0 ) {
        self.name = name
        self.children = children
        self._expanded = false
        self._position = position
        self._level = 0
        self._parent = nil
        self.identifier = identifier
        self._parent = parent
        
        for data in children
        {
            data._parent = self
        }
    }
    
    convenience init(_ name : String, _ identifier : Int64 = 0) {
        self.init( name, identifier, [DataObject]())
    }
    
    func addChild(_ child : DataObject) {
        self.children.append(child)
    }
    
    func removeChild(_ child : DataObject) {
        self.children = self.children.filter( {$0 !== child})
    }
    
    func isExpandable( ) -> Bool
    {
        return self.children.count != 0;
    }
    
    
    var expanded : Bool
        {
        get
        {
            return _expanded
        }
        set( newValue )
        {
            _expanded = newValue;
        }
    }
    
    func expandedCount( ) -> Int
    {
        return _expandedCount( self )
    }
    
    
     func _expandedCount( _ obj : DataObject ) -> Int
    {
        var result = 2
        if obj.expanded
        {
            for child in obj.children
            {
                result += _expandedCount( child )
            }
        }
        return result
    }

    internal( set ) var _expanded : Bool
    internal( set ) var children : [DataObject]
    internal var _level : Int
    internal var _position : Int
    internal var _parent : DataObject?
    
}


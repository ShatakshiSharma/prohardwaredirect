//
//  FilterViewController.swift
//  ToDool
//
//  Created by Yogita on 27/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//
import UIKit
import CoreData


protocol Filter
{
    var name : String { get }
    var selectedItem : String? { get }
    var selectedIndex : Int? { get }
    var isExpanded : Bool { get }
    
    
    func itemSelected( name : String, index : Int )
    func forceReset( )
    func forceClose( _ tableView : UITableView, indexPath : IndexPath )
    func registerNib( _ tabelView : UITableView )
    func heightForExpandableCell( ) -> CGFloat
    func cell( _ tableView : UITableView!, cellForRowAt indexPath : IndexPath ) -> UITableViewCell
    func select( _ tableView : UITableView!, didSelectRowAt indexPath : IndexPath )
    func configure( cell : UITableViewCell!, for indexPath : IndexPath )
    func expansionData( _ tableView : UITableView!, for indexPath : IndexPath ) -> DataObject
}


internal class ExpandibleFilter : NSObject, Filter
{
    var name: String
    {
        return _name
    }
    
    
    var selectedItem: String?
    {
        return _selectedItem
    }
    
    
    var isExpanded: Bool
    {
        return _isExpanded
    }
    
    
    var selectedIndex: Int?
    {
        return _selectedIndex;
    }
    

    init( name : String )
    {
        self._name = name
        self._isExpanded = false
    }
    
    
    func itemSelected( name : String, index : Int )
    {
        self._selectedItem = name
        self._selectedIndex = index
    }
    
    
    func heightForExpandableCell( ) -> CGFloat
    {
        return 0.0
    }
    
    
    func forceClose( _ tableView : UITableView, indexPath : IndexPath )
    {
        if self._isExpanded
        {
            self._isExpanded = false
            tableView.deleteRows( at: [ IndexPath( row : 1, section : indexPath.section ) ], with : .none )
            self.animateClose( )
        }
    }


    func animateClose( )
    {

    }
    
    
    func forceReset( )
    {
        self._selectedIndex = nil
        self._selectedItem = nil
        self._isExpanded = false
    }
    
    
    func registerNib( _ tableView : UITableView )
    {
        tableView.register( UINib.init( nibName : "FilterTableViewCell", bundle : nil ), forCellReuseIdentifier: "FilterTableViewCell" )
        tableView.register( UINib.init( nibName : "FilterTableViewTreeCell", bundle : nil ), forCellReuseIdentifier: "FilterTableViewTreeCell" )
        tableView.register( UINib.init( nibName : "FilterTableViewTableCell", bundle : nil ), forCellReuseIdentifier: "FilterTableViewTableCell" )
    }
    
    
    func cell( _ tableView : UITableView!, cellForRowAt indexPath : IndexPath ) -> UITableViewCell
    {
        
        if indexPath.row == 0
        {
            let result = tableView.dequeueReusableCell( withIdentifier : "FilterTableViewCell", for: indexPath )
            self.configure(cell: result, for: indexPath )
            return result
        }
        else
        {
            let result = tableView.dequeueReusableCell( withIdentifier : "FilterTableViewTreeCell", for: indexPath )
            self.configure(cell: result, for: indexPath )
            return result
        }
    }
    
    
    func select( _ tableView : UITableView!, didSelectRowAt indexPath : IndexPath )
    {
        if self._isExpanded
        {
            self._isExpanded = false
            tableView.deleteRows( at: [ IndexPath( row : 1, section : indexPath.section ) ], with : .none )
        }
        else
        {
            self._isExpanded = true
            tableView.insertRows( at : [ IndexPath( row : 1, section : indexPath.section ) ], with : .none )
        }
    }
    
    
    func configure( cell : UITableViewCell!, for indexPath : IndexPath )
    {}
    
    
    func expansionData( _ tableView : UITableView!, for indexPath : IndexPath ) -> DataObject
    {
        return DataObject( "Loading", -1 )
    }
    
    
    fileprivate let _name : String
    fileprivate var _selectedItem : String?
    fileprivate var _isExpanded : Bool
    fileprivate var _selectedIndex : Int?
    fileprivate var selectedCategory : String?

}


internal class CategoryFilter : ExpandibleFilter
{
    init( _ tableView : UITableView, _ index : Int )
    {
        self._index = index
        self._tableView = tableView
        super.init( name: "Category" )
        self._dataObject = self.getCategoryDataObjects( )
        self.getFirstCategory(cat: self._dataObject.children[0])

    }
    
    func getCategoryDataObjects() -> DataObject!
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.appDelegateHomeData
    }
    
    func getFirstCategory(cat : DataObject) {
        if cat.children.count == 0 {
            self.itemSelected(name: cat.name, index: Int(cat.identifier))
        }
        else
        {
            self.getFirstCategory(cat: cat.children[0])
        }
    }

    override func animateClose( )
    {
        if let cell = self._mainCell
        {
            cell.close( )
        }
    }
    
    
    override func expansionData( _ tableView : UITableView!, for indexPath : IndexPath ) -> DataObject
    {
        return self._dataObject
    }
    
    
    override func heightForExpandableCell( ) -> CGFloat
    {
        return CGFloat( 200 * self._dataObject.expandedCount( ) )
    }
    
    
    override func configure( cell : UITableViewCell!, for indexPath : IndexPath )
    {
        if indexPath.row == 0
        {
            if let c : FilterTableViewCell = cell as? FilterTableViewCell
            {
                self._mainCell = c
                c.categoryLabel.text = self.name
            }
        }
        else
        {
            if let c : FilterTableViewTreeCell = cell as? FilterTableViewTreeCell
            {
                c.treeView.register( UINib.init( nibName : "CollapsableTableViewCell", bundle : nil ), forCellReuseIdentifier: "CollapsableTableViewCell")
                
                c.treeView.delegate = self
                c.treeView.dataSource = self
                c.treeView.separatorStyle = .init( 0 )
                c.treeView.reloadData()
            }
        }
    }
    
    
    override func cell( _ tableView : UITableView!, cellForRowAt indexPath : IndexPath ) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let result = tableView.dequeueReusableCell( withIdentifier : "FilterTableViewCell", for: indexPath )
            self.configure(cell: result, for: indexPath )
            return result
        }
        else
        {
            let result = tableView.dequeueReusableCell( withIdentifier : "FilterTableViewTreeCell", for: indexPath )
            self.configure(cell: result, for: indexPath )
            return result
        }
    }
    
    
    fileprivate var _index : Int
    fileprivate var _tableView : UITableView
    fileprivate var _mainCell : FilterTableViewCell?
    fileprivate var _dataObject : DataObject!
}


extension CategoryFilter : RATreeViewDataSource, RATreeViewDelegate
{
    func treeView(_ treeView: RATreeView, willExpandRowForItem item: Any)
    {
        if let item = item as? DataObject
        {
            if item.expanded == false
            {
                item.expanded = true
                ( treeView.cell(forItem: item) as? CollapsableTableViewCell )?.open()
            }
            if item.children.count == 0
            {
                let cell  = ( treeView.cell(forItem: item) as? CollapsableTableViewCell )
                cell?.customTitleLabel.textColor = UIColor.red
            }
            var frame = self._tableView.frame
            frame.size.height = self._tableView.contentSize.height
            self._tableView.frame = frame
            //treeView.reloadRows( forItems: [ item ], with: RATreeViewRowAnimationNone )
        }
    }
    
    
    func treeView(_ treeView: RATreeView, didExpandRowForItem item: Any) {
        if item is DataObject
        {
            var frame = self._tableView.frame
            frame.size.height = self._tableView.contentSize.height
            self._tableView.frame = frame
            //treeView.reloadRows( forItems: [ item ], with: RATreeViewRowAnimationNone )
        }
    }
    
    
    func treeView(_ treeView: RATreeView, didCollapseRowForItem item: Any) {
        if item is DataObject
        {
            var frame = self._tableView.frame
            frame.size.height = self._tableView.contentSize.height
            self._tableView.frame = frame
            //treeView.reloadRows( forItems: [ item ], with: RATreeViewRowAnimationNone )
        }
    }
    
    
    func treeView(_ treeView: RATreeView, willCollapseRowForItem item: Any)
    {
        if let item = item as? DataObject
        {
            item.expanded = false
            ( treeView.cell(forItem: item) as? CollapsableTableViewCell )?.close()
            var frame = self._tableView.frame
            frame.size.height = self._tableView.contentSize.height
            self._tableView.frame = frame
            //treeView.reloadRows( forItems: [ item ], with: RATreeViewRowAnimationNone )
        }
    }
    
    
    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if let item = item as? DataObject {
            return item.children.count
        } else {
            return _dataObject.children.count-1
        }
    }
    
    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        if let item = item as? DataObject {
            return item.children[index]
        } else {
            return _dataObject.children[index] as AnyObject
        }
    }
    
    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell
    {
        let cell = treeView.dequeueReusableCell(withIdentifier: "CollapsableTableViewCell") as! CollapsableTableViewCell
        let item = item as! DataObject
        if(item.isExpandable())
        {
            cell.button.isHidden = false
        }
        else
        {
            cell.button.isHidden = true
        }

        let level = treeView.levelForCell(forItem: item)
        let detailsText = "Number of children \(item.children.count)"
        cell.selectionStyle = .none
        cell.setup(withTitle: item.name, detailsText: detailsText, level: level, additionalButtonHidden: false)
        return cell
    }
    
    func  treeView(_ treeView: RATreeView, didSelectRowForItem item: Any)
    {
        if let dobject : DataObject = item as? DataObject
        {
            self.itemSelected( name : dobject.name, index : Int( dobject.identifier ) )
//            self._mainCell?.selectedCategory.text = dobject.name
            if((dobject.identifier != -1) && (dobject.isExpandable() == false))
            {
            }
        }
    }
}


internal class BrandsFilter : ExpandibleFilter
{
    init( _ tableView : UITableView, _ index : Int )
    {
        self._index = index
        self._tableView = tableView
        super.init( name: "Brand" )
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.getFirstCategory(cat: appDelegate.appDelegateHomeData.children[0])
    }
    
    func getFirstCategory(cat : DataObject) {
        if cat.children.count == 0 {
            self.selectedCategory =  String(format:"%d", cat.identifier)
            self.getBrandsDataObjects()
        }
        else
        {
            self.getFirstCategory(cat: cat.children[0])
        }
    }

    func getBrandsDataObjects(){
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Loading..", animated: true)
            let dict = ["categoryid": self.selectedCategory]
            let paramArray: [Any] = ["custom_api.brandexistincat",dict]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject!)

                 self._dataObject = responseObject! as! Array
                SwiftLoader.hide()
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print((error?.localizedDescription)!)
                SwiftLoader.hide()
//                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
//            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    override func heightForExpandableCell( ) -> CGFloat
    {
        return CGFloat( 200 * self._dataObject.count )
    }
    
    
    override func expansionData( _ tableView : UITableView!, for indexPath : IndexPath ) -> DataObject
    {
        return (self._dataObject as AnyObject) as! DataObject
    }
    
    
    override func forceReset( )
    {
        super.forceReset()
        self._selectedBrandIndex = nil
    }
    
    
    override func configure( cell : UITableViewCell!, for indexPath : IndexPath )
    {
        if indexPath.row == 0
        {
            if let c : FilterTableViewCell = cell as? FilterTableViewCell
            {
                self._mainCell = c
                c.categoryLabel.text = self.name

            }
        }
        else
        {
            if let c : FilterTableViewTableCell = cell as? FilterTableViewTableCell
            {
                c.tableView.register( UINib.init( nibName : "CollapsableTableViewCell", bundle : nil ), forCellReuseIdentifier: "CollapsableTableViewCell")
                c.tableView.delegate = self
                c.tableView.dataSource = self
                c.tableView.separatorStyle = .none
                c.tableView.allowsMultipleSelection = false
                c.tableView.tableFooterView = UIView()
                UITableViewCell.appearance().tintColor = UIColor.green
                c.tableView.reloadData()
            }
        }
    }
    
    
    override func cell( _ tableView : UITableView!, cellForRowAt indexPath : IndexPath ) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let result = tableView.dequeueReusableCell( withIdentifier : "FilterTableViewCell", for: indexPath )
            self.configure(cell: result, for: indexPath )
            return result
        }
        else
        {
            let result = tableView.dequeueReusableCell( withIdentifier : "FilterTableViewTableCell", for: indexPath )
            self.configure(cell: result, for: indexPath )
            return result
        }
    }
    
    
    fileprivate var _index : Int
    fileprivate var _tableView : UITableView
    fileprivate var _mainCell : FilterTableViewCell?
    fileprivate var _dataObject : Array<Any>!
    fileprivate var _selectedBrandIndex : Int?
}


extension BrandsFilter : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        UITableViewCell.appearance().tintColor = UIColor.green
        return self._dataObject.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollapsableTableViewCell", for: indexPath ) as! CollapsableTableViewCell
        cell.customTitleLabel.text = (self._dataObject[ indexPath.row ] as! NSDictionary).value(forKey: "product_option_value") as! String?
        cell.button.image = nil
        cell.accessoryType = .none
        cell.leadingSpace.constant = 0
        cell.accessoryView = UIView( )
        UITableViewCell.appearance().tintColor = UIColor.green
        if let index = self._selectedBrandIndex
        {
            if indexPath.row == index
            {
                cell.customTitleLabel.font = UIFont(name: "Quicksand-Bold", size: 15 )!
                let image = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20 ) )
                image.image = UIImage( named : "filter_checkbox" )
                cell.button.image =  UIImage( named : "filter_checkbox" )//accessoryView = image
            }
            else{
                cell.customTitleLabel.font = UIFont(name: "Quicksand-Regular", size: 15 )!
            }
        }
        else
        {
            cell.customTitleLabel.font = UIFont(name: "Quicksand-Regular", size: 15 )!
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self._mainCell?.selectedCategory.text = self._dataObject.children[ indexPath.row ].name
        self.itemSelected(name: ((self._dataObject[ indexPath.row ] as! NSDictionary).value(forKey: "product_option_value") as! String), index: Int((self._dataObject[ indexPath.row ] as! NSDictionary).value(forKey: "product_option_id") as! String)! )
        if tableView.cellForRow(at: indexPath ) != nil
        {
            if let index = self._selectedBrandIndex
            {
                self._selectedBrandIndex = indexPath.row
                tableView.reloadRows(at: [ IndexPath( row : index, section : 0 ), indexPath ], with: .none )
            }
            else
            {
                self._selectedBrandIndex = indexPath.row
                tableView.reloadRows(at: [ indexPath ], with: .none )
            }
        }
    }
}


class FilterViewController : UIViewController,UIGestureRecognizerDelegate
{
    
    @IBOutlet weak var filterTable :  UITableView!
    fileprivate var filters = [ Filter ]( )

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.filterTable.separatorStyle = .none
        self.title = "Filter"
        
        self.navigationController!.navigationBar.barStyle = UIBarStyle.default
        self.navigationController!.navigationBar.tintColor = UIColor.white
       self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(self.leftBarBtnClick))
        
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Clear all", style:.plain, target:self, action:#selector(self.clearButtonClicked))
        filters.append( CategoryFilter( self.filterTable, 0 ) )
        filters.append( BrandsFilter( self.filterTable, 1 ) )
        filters[ 0 ].registerNib( self.filterTable )
        filterTable.autoresizingMask = UIViewAutoresizing(rawValue:UIViewAutoresizing.flexibleWidth.rawValue | UIViewAutoresizing.flexibleHeight.rawValue)
        filterTable.delegate = self;
        filterTable.dataSource = self;
        filterTable.rowHeight = UITableViewAutomaticDimension
        filterTable.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
        
    func leftBarBtnClick()
    {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
    }
    
    @IBAction func applyClicked( sender : UIButton )
    {
        var categoryId : Int64?
        var brandId : Int?
        var categoryName : String?
        var brandName : String?
        for filter in filters
        {
            if filter.name == "Category"
            {
                if let id = filter.selectedIndex
                {
                    categoryName = filter.selectedItem
                    categoryId = Int64(id)
                }
            }
            if filter.name == "Brand"
            {
                if let id = filter.selectedIndex
                {
                    brandId = id
                    brandName = filter.selectedItem
                }
            }
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
        vc.brandId = brandId
        vc.categoryId = categoryId
        if let cn = categoryName
        {
            vc.title = cn
        }
        else
        {
            if let bn = brandName
            {
                vc.title = bn
            }
            else
            {
                vc.title = "Search"
            }
        }
        self.frostedViewController.hideMenuViewController( )
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    
    func clearButtonClicked()
    {
        for filter in self.filters
        {
            filter.forceReset( )
        }
        self.filterTable.reloadData()
    }
}

extension FilterViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        self.filterTable.contentInset = UIEdgeInsets( top : 0, left : 0, bottom : 0, right : 0 )
        return self.filters.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.filters[ section ].isExpanded
        {
            return 2
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {

        return self.filters[ indexPath.section ].cell( tableView, cellForRowAt : indexPath )
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 60
        }
        return self.filters[ indexPath.section ].heightForExpandableCell( )
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var index = 0
      //  let selectedCell = tableView.cellForRow(at: indexPath) as! FilterTableViewCell
        for filter in self.filters
        {
            if index != indexPath.section
            {
                filter.forceClose( tableView, indexPath : IndexPath( row : 0, section : index ) )
            }
            else
            {
                self.filters[ indexPath.section ].select( tableView, didSelectRowAt : indexPath )
            }
            index += 1
        }
    }
    
}


//
//  HomeViewController.swift
//  ToDool
//
//  Created by Pranav on 9/12/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}


class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    //var data : DataObject!
    var panGesture : UIPanGestureRecognizer?
    @IBOutlet var tableView : UITableView!
    @IBOutlet var searchBar : UISearchBar!
    @IBOutlet var container : UIView!
    var menuViewController : LeftViewController!
    var appDelegate : AppDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate

        if((appDelegate.appDelegateHomeData == nil) || (appDelegate.appDelegateMenuData == nil))
        {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        menuViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        menuViewController.categoryWebService()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(reloadPrimaryCategories(notification:)), name: .reloadPrimaryCategories, object: nil)
        
        self.searchBarConfig()
        
        if (UserDefaults.standard.value(forKey: "cartArray") != nil){
            if (UserDefaults.standard.object(forKey: k_userID) != nil){
                if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
                    self.addProductsToCart()
                }
                else{
                    self.createCart()
                }
            }
            else{
                if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
                    self.addProductsToCart()
                }
            }
        }
        
        let attributes = [NSFontAttributeName: UIFont (name: "Quicksand-Bold", size: 17)!];
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal);
        UINavigationBar.appearance().titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 17)!, NSForegroundColorAttributeName : UIColor.white]
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "navigation_landingscreen" ), style: .plain, target: self, action: #selector(HomeViewController.leftNavigationBarButtonClicked) )
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage( named: "filter_landingdcreen" ), style: .plain, target: self, action: #selector(self.filterBtnClicked))
        
        panGesture = UIPanGestureRecognizer()
        panGesture = UIPanGestureRecognizer(target: self, action:#selector(self.panGestureRecognized(sender:)))
        self.view.addGestureRecognizer(panGesture!)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = true
        tableView.estimatedRowHeight = 83.5
        tableView.register(UINib.init(nibName: "HomePageCategoryCell", bundle: nil), forCellReuseIdentifier: "HomePageCategoryCell")
    }
    
    func createCart()
    {
        if (Reachability.isConnectedToNetwork() == true){
            let dict = ["customer_id": UserDefaults.standard.string(forKey: k_userID)]
            
            let paramArray: [Any] = ["custom_api.custquoteid",dict]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                let response = responseObject as! NSString
                print(response)
                UserDefaults.standard.set(response, forKey: k_quoteID)
                self.addProductsToCart()
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print((error?.localizedDescription)!)
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func addProductsToCart()
    {
        if (Reachability.isConnectedToNetwork() == true){
            let arrCartProducts = (UserDefaults.standard.value(forKey: "cartArray") as! NSArray)
            let arrProducts = NSMutableArray()
            var object : NSDictionary!
            for i in 0...(arrCartProducts.count)-1 {
                object = ["product_id": (((arrCartProducts[i]) as! NSDictionary).value(forKey: "productId"))!,
                          "sku": (((arrCartProducts[i]) as! NSDictionary).value(forKey: "sku"))!,
                          "qty": ((arrCartProducts[i]) as! NSDictionary).value(forKey: "quantity")!,
                          "options" : "",
                          "bundle_option" : "",
                          "bundle_option_qty" : "",
                          "links" : ""]
                arrProducts.add(object)
            }
            let paramArray: [Any] = ["cart_product.add", UserDefaults.standard.string(forKey: k_quoteID)!,arrProducts]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                if responseObject as! Bool == true{
                    UserDefaults.standard.removeObject(forKey: "cartCount")
                    UserDefaults.standard.removeObject(forKey: "cartArray")
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 18)!, NSForegroundColorAttributeName    : UIColor.white ]
       self.navigationController!.navigationBar.barStyle = UIBarStyle.default
      self.navigationController!.navigationBar.tintColor = UIColor.white
       self.navigationController!.navigationBar.barTintColor = navigationColor
        self.navigationController!.navigationBar.isTranslucent = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func leftNavigationBarButtonClicked( )
    {
        self.frostedViewController.presentMenuViewController( )
        self.view.endEditing(true)
    }
    
    func panGestureRecognized(sender : UIPanGestureRecognizer)
    {
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    func filterBtnClicked()
    {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
//            ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc,animated: true )
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyCartViewcontroller") as! MyCartViewcontroller
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }

    //Search Bar Delegates
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        self.view.endEditing(true);
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchProductsWebService(searchString: searchBar.text!)
    }
    
    func searchBarConfig()
    {
        searchBar.backgroundColor = UIColor.clear
        searchBar.tintColor = UIColor.white
        searchBar.barStyle = .default
        searchBar.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0);
        searchBar.delegate = self
        let textFieldInsideSearchBar = searchBar?.value(forKey: "searchField") as? UITextField
        searchBar?.setSearchFieldBackgroundImage( UIImage( color : UIColor.white, size : CGSize( width : 1, height : 15 ) ), for: UIControlState.normal )
        textFieldInsideSearchBar?.textColor = UIColor.darkGray
        textFieldInsideSearchBar?.tintColor = UIColor.black
        textFieldInsideSearchBar?.background = UIImage()
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        textFieldInsideSearchBar?.layer.cornerRadius = 5.0
        textFieldInsideSearchBar?.clipsToBounds = true
        textFieldInsideSearchBar?.font  = UIFont (name: "Quicksand-Regular", size: 15)
    }
    

    //Table view Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {

        if(appDelegate.appDelegateHomeData != nil)
        {
            return appDelegate.appDelegateHomeData.children.count
        }
        else
        {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageCategoryCell") as! HomePageCategoryCell
        cell.selectionStyle = .none
        
        if(indexPath.row == 0)
        {
            cell.backImage?.image = UIImage(named : "home_tools_background")
        }
        
        if(appDelegate.appDelegateHomeData != nil)
        {
            cell.foreText?.text = appDelegate.appDelegateHomeData.children[indexPath.row].name
        }
    
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (Reachability.isConnectedToNetwork() == true) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            vc.categoryID = appDelegate.appDelegateHomeData.children[indexPath.row].identifier
            vc.childrens = appDelegate.appDelegateHomeData.children[indexPath.row].children
            vc.title = appDelegate.appDelegateHomeData.children[indexPath.row].name
            
            ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
  
    
     func reloadPrimaryCategories(notification: NSNotification) {
      self.tableView.reloadData()
        
    }
    
    func searchProductsWebService( searchString: String)
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Searching...", animated: true)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
            
            let dict = ["page_no": 1, "count": "50", "serachterm" : searchString] as [String : Any]
            let paramArray: [Any] = ["custom_api.productsearch",dict]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                
                if (responseObject != nil){
                    let products = (responseObject as! NSArray) as Array
                    let attributes = ["msrp", "price", "color", "size", "image"]
                    let calls = NSMutableArray()
                    for product in products {
                        calls.add(["catalog_product_attribute_media.list", product.value(forKey: "product_id") as! NSString])
                        calls.add(["catalog_product.info", product.value(forKey: "product_id") as! NSString, 1, attributes])
                    }
                    Magento.multiCall((calls as NSArray) as! [Any], success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                        var newProductParts : NSArray
                        newProductParts=responseObject as! NSArray
                        
                        (products as NSArray).enumerateObjects({ ( product, index, stop) in
                            var predicate: NSPredicate?
                            predicate = NSPredicate(format: "exclude == '0'")
                            if(((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject != nil){
                                let image : NSDictionary = ((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject as! NSDictionary
                                (product as! NSMutableDictionary).setObject(image.object(forKey: "url") as Any, forKey: "image" as  NSCopying)
                            }
                            (product as! NSMutableDictionary).addEntries(from: newProductParts[index+index+1] as! [AnyHashable : Any])
                        })
                        
                        SwiftLoader.hide( )
                        self.searchBar.setShowsCancelButton(false, animated: true)
                        self.searchBar.text = ""
                        self.view.endEditing(true);

                        if products.count>0{
                            vc.arrayProducts = products
                            vc.totalCountSearch = (((vc.arrayProducts[0]as! NSDictionary).value(forKey: "totalcount") as! NSInteger))
                            vc.searchString = searchString
                            vc.isHome = true
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            for i in 0...appDelegate.appDelegateHomeData.children.count-1 {
                                
                                if (appDelegate.appDelegateHomeData.children[i].name == "Brands"){
                                    vc.brands = appDelegate.appDelegateHomeData.children[i].children
                                }
                            }

                            ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
                        }
                        else{
                            self.view.makeToast(message: "No product found!", duration: 3.0, position: HRToastPositionTop as AnyObject )
                        }
                        
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                        SwiftLoader.hide()
                        
                    })
                }
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                SwiftLoader.hide()
            })
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

}


extension Notification.Name {
    static let reloadPrimaryCategories = Notification.Name("reloadPrimaryCategories")
}


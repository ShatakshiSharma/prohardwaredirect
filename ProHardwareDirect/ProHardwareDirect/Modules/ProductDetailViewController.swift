//
//  ProductDetailViewController.swift
//  ToDool
//
//  Created by Yogita on 19/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData

class ProductDetailViewController : UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate
{
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName:UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var originalPrice: UILabel!
    @IBOutlet weak var discountGroup: UILabel!
    @IBOutlet weak var qtyTextField: UITextField!
    @IBOutlet weak var productNumber: UILabel!
    @IBOutlet weak var productBrand: UILabel!
    @IBOutlet weak var productDetails: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var qtyView: UIView!
    
    
    var quantityCount = 0
    var quantityTextField : UITextField?
    var alertString : String?
    var isCartItem = false
    var badgeButton : MIBadgeButton?
    var index : Int!
    var productDict : NSDictionary!
    var brands : [DataObject]!
    weak var activeTextField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        
        
        //-----------Add Badge-----------------
        badgeButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 34, height: 34
        ))
        badgeButton?.setImage( UIImage( named: "cart_productdetail" ), for: UIControlState.normal)
        badgeButton?.addTarget(self, action:  #selector(self.rightNavigationBarButtonClicked), for: UIControlEvents.touchUpInside)
        
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: badgeButton!)
        self.navigationItem.rightBarButtonItem = barButton
        
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(self.hideKeyboardOnTap(_:)) )
        self.view.addGestureRecognizer( gesture )
        
        let qtyGesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(self.addItemQuantity(_:)) )
        self.qtyView.addGestureRecognizer( qtyGesture )
        
        //----Populate data-----
        self.populateData()

        if (isCartItem == true) {
            self.qtyTextField?.text = String(quantityCount)
            self.alertString = "Update"
            self.addToCartBtn.isHidden = true
            self.navigationItem.rightBarButtonItem = nil
        }
        else{
            self.alertString = "Add"
        }
    }
    
    func leftNavigationBarButtonClicked() {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
    }
    
    func rightNavigationBarButtonClicked() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "MyCartViewcontroller") as! MyCartViewcontroller
         ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    func addItemQuantity( _ gesture : UITapGestureRecognizer )
    {
        if isCartItem == true{
            let qtyAlert = UIAlertController(title: "Enter Quantity", message: "", preferredStyle: .alert)
            qtyAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            qtyAlert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                let currentValue = Int( (self.quantityTextField?.text!)! )
                if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
                    self.updateProductInCart (quantity: currentValue!)
                }
                else{
                    Utility.addItemQuantiity(productId: self.productDict.value(forKey: "productId") as! String, quantity: currentValue!)
                }
            }))
            qtyAlert.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.delegate = self
                textField.placeholder = "Quantity"
                textField.text =  String(self.quantityCount)
                textField.keyboardType = UIKeyboardType.numberPad
                self.quantityTextField = textField
            })
            present(qtyAlert, animated: true, completion: nil)

        }
        else{
            let qtyAlert = UIAlertController(title: "Enter Quantity", message: "", preferredStyle: .alert)
            
            qtyAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            qtyAlert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                if self.quantityTextField?.text != nil
                {
                    self.quantityCount = Int((self.quantityTextField?.text!)!)!
                    if self.quantityCount > 0
                    {
                        self.qtyTextField.text = String(self.quantityCount)
                    }
                    else
                    {
                        self.view.makeToast(message: "Please enter a valid quantity", duration: 2.0, position: HRToastPositionTop as AnyObject )
                    }
                }
                else
                {
                    self.view.makeToast(message: "Please enter a quantity", duration: 2.0, position: HRToastPositionTop as AnyObject )
                }
            }))
            qtyAlert.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.delegate = self
                textField.placeholder = "Quantity"
                textField.keyboardType = UIKeyboardType.numberPad
                self.quantityTextField = textField
            })
            present(qtyAlert, animated: true, completion: nil)
        }
    }
    
    func updateProductInCart (quantity : Int)
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Updating..", animated: true)
            
            let arrProducts = NSMutableArray()
            var object : NSDictionary!
            object = ["product_id": (self.productDict.value(forKey: "product_id"))!,
                      "sku": (self.productDict.value(forKey: "sku"))!,
                      "qty": quantity,
                      "options" : "",
                      "bundle_option" : "",
                      "bundle_option_qty" : "",
                      "links" : ""]
            arrProducts.add(object)
            let paramArray: [Any] = ["cart_product.update", UserDefaults.standard.string(forKey: k_quoteID)!,arrProducts]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                SwiftLoader.hide()

                if responseObject as! Bool == true{
                    self.qtyTextField.text = String(quantity)
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    // MARK:----Textfield delegates----
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
    }
    
    
    func hideKeyboardOnTap( _ gesture : UITapGestureRecognizer )
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
    }
    
    
    func  populateData()->Void
    {
        self.productName.text = self.productDict.value(forKey: "name") as? String
        
        if (( self.productDict.value(forKey: "you_pay"))! as AnyObject).floatValue != 0.0{
            self.discountPrice.text = "$" + String (((self.productDict.value(forKey: "you_pay"))! as AnyObject).floatValue)
            
            let offer = ((((( self.productDict.value(forKey: "price"))! as AnyObject).floatValue) - ((( self.productDict.value(forKey: "you_pay"))! as AnyObject).floatValue))/((( self.productDict.value(forKey: "price"))! as AnyObject).floatValue)) * 100
            
            self.discountGroup.text = "Cashback " + String(Int(ceil(offer))) + "%"
        }
        self.originalPrice.text = "$" + String ((( self.productDict.value(forKey: "price"))! as AnyObject).floatValue)
        self.productNumber.text = self.productDict.value(forKey: "sku") as! String?
        
        if ( (( self.productDict.value(forKey: "brand") as! String?)) != nil )
        {
            let brandIdentifier : [Any] = self.productDict.value(forKey: "category_ids") as! [Any]
            for j in 0...brandIdentifier.count-1 {
                for i in 0...self.brands.count-1 {
                    if self.brands[i].identifier == (brandIdentifier[j] as AnyObject).int64Value{
                        self.productBrand.text = self.brands[i].name
                    }
                }
            }
        }

        
        if let url = self.productDict.value(forKey: "image")
        {
            self.productImage.sd_setImage( with : URL( string : url as! String )!, placeholderImage : UIImage( named : "product1")! )
        }
        self.productDetails.text = self.productDict.value(forKey: "description") as! String?
    }
    
    @IBOutlet weak var addToCartBtn: UIButton!
    
    @IBAction func addToCartBtnClick(_ sender: UIButton) {
        if self.quantityTextField?.text != nil
        {
            let quantity:Int? = Int((self.quantityTextField?.text!)!)
            if quantity! > 0
            {
                if (self.productDict.value(forKey: "image") != nil)
                {
                    Utility.addProductToCart(productId :self.productDict.value(forKey: "product_id") as! String, quantity : self.quantityCount, name : self.productDict.value(forKey: "name") as! String, sku : self.productDict.value(forKey: "sku") as! String, discountedPrice : self.productDict.value(forKey: "you_pay") as! String, originalPrice : String ((( self.productDict.value(forKey: "price"))! as AnyObject).floatValue), imgUrl: self.productDict.value(forKey: "image") as! String)
                }
                else
                {
                    Utility.addProductToCart(productId :self.productDict.value(forKey: "product_id") as! String, quantity : self.quantityCount, name : self.productDict.value(forKey: "name") as! String, sku : self.productDict.value(forKey: "sku") as! String, discountedPrice : self.productDict.value(forKey: "you_pay") as! String, originalPrice : String ((( self.productDict.value(forKey: "price"))! as AnyObject).floatValue), imgUrl: "product1")
                }
            }
            else{
                self.view.makeToast(message: "Please Enter a valid quantity", duration: 2.0, position: HRToastPositionTop as AnyObject )
            }
        }
        else
        {
            self.view.makeToast(message: "Please Enter a quantity", duration: 2.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    
}


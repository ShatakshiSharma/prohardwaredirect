//
//  ProductsViewController.swift
//  ToDool
//
//  Created by Yogita on 20/09/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//
import UIKit
import CoreData


class ProductsViewController : UIViewController, UISearchBarDelegate, UITextFieldDelegate
{
    @IBOutlet weak var tableView : UITableView!
    var searchController : UISearchController!
    var searchText : String?
    var quantity : Int = 0
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var cartCountView: UIView!
    @IBOutlet weak var cartCountLabel: UILabel!
    
    
    @IBOutlet weak var footerViewBottom: NSLayoutConstraint!
    
    
    var categoryId : Int64?
    var brandId : Int?
    var cartCount : Int = 0
    var isScrolling : Bool?
    var quantityTextField : UITextField!
    
    var arrayProducts = Array<Any>()
    var arrayProductsWithoutSearch = Array<Any>()

    var appDelegate : AppDelegate!
    var brands : [DataObject]!
    
    var activityView : UIView!
    var activity : UIActivityIndicatorView!
    var lastPageBool : Bool?
    var currentPage : NSInteger?
    var totalCount : NSInteger?
    var currentPageSearch : NSInteger?
    var totalCountSearch  : NSInteger?
    var lastPageBoolSearch : Bool?
    var searchString : String?
    var isHome : Bool?
    
    override func viewDidLoad() {
    
    super.viewDidLoad()
    
    appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    if (cartCount == 0)
    {
    self.cartCountView.isHidden = true
    }
    
    isScrolling = false
    lastPageBool = false
    currentPage = 1
    currentPageSearch = 1
    lastPageBoolSearch = false

    self.navigationController!.navigationBar.barStyle = UIBarStyle.default
    self.navigationController!.navigationBar.tintColor = UIColor.white
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
    NotificationCenter.default.addObserver(self, selector: #selector(updateCartCount(notification:)), name: .cartCount, object: nil)

    //Add shadow to footer
    self.footerView.layer.shadowColor = UIColor.black.cgColor
    self.footerView.layer.shadowOpacity = 0.5
    self.footerView.layer.shadowOffset = CGSize.zero
    self.footerView.layer.shadowRadius = 4
    
    self.cartViewRound()
    
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage( named: "dark_kart_icon" ), style: .plain, target: self, action: #selector(ProductsViewController.rightNavigationBarButtonClicked) )
    tableView.register(UINib.init(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductTableViewCell")
    tableView.autoresizingMask = UIViewAutoresizing(rawValue:UIViewAutoresizing.flexibleWidth.rawValue | UIViewAutoresizing.flexibleHeight.rawValue)
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = UIColor.clear
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.estimatedRowHeight = 100.0
    
    activityView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100))
    activity = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    activity.center = activityView.center
    activity.color=UIColor.black
    activityView.addSubview(activity)
        
    if totalCountSearch != nil {
            currentPageSearch = 2
            tableView.reloadData()
        }
        else{
            self.searchBarConfig()
            self.productsListWebService()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.searchController?.searchBar.backgroundImage = UIImage()
        
        if(UserDefaults.standard.value(forKey: "cartCount") != nil ){
            cartCount = UserDefaults.standard.value(forKey: "cartCount") as! Int
        }
        if (cartCount == 0)
        {
            self.cartCountView.isHidden = true
        }
        else
        {
            self.cartCountView.isHidden = false
            self.cartCountLabel.text = String(cartCount)
            
        }
    }

    
    
    func leftNavigationBarButtonClicked( )
    {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
        self.view.endEditing(true)
    }
    
    
    func rightNavigationBarButtonClicked( )
    {
    }
    
    func updateCartCount(notification: NSNotification) {
        
        if(UserDefaults.standard.value(forKey: "cartCount") != nil ){
            cartCount = UserDefaults.standard.value(forKey: "cartCount") as! Int
            
            if (cartCount == 0)
            {
                self.cartCountView.isHidden = true
            }
            else
            {
                self.cartCountView.isHidden = false
                self.cartCountLabel.text = String(describing:cartCount)
            }
        }
        else{
            self.cartCount = 0
            if (cartCount == 0)
            {
                self.cartCountView.isHidden = true
            }
        }
    }

    
    @IBAction func filterBtnClick(_ sender: UIButton) {
       // self.view.makeToast(message: "Wait! Work in progress", duration: 3.0, position: HRToastPositionTop as AnyObject )
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
     }
    
    @IBAction func cartBtnClick(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyCartViewcontroller") as! MyCartViewcontroller
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    // Searchbar delegates
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        self.view.endEditing(true)
        self.arrayProducts = self.arrayProductsWithoutSearch
        searchString = nil
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        currentPageSearch = 1;
        lastPageBoolSearch = false
        searchString = searchBar.text!
        searchProductsWebService(searchString: searchString!)
    }
    
    // MARK:------TextFieldDelegate------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 3
    }
    
    
    // scroll view delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if (isScrolling == false) {
            self.isScrolling = true
            self.footerViewBottom.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
                self.isScrolling = false
            }
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(isScrolling == false){
            self.isScrolling = true
            self.footerViewBottom.constant = -self.footerView.frame.size.height
            UIView.animate(withDuration: 0.2, delay: 2, options: [UIViewAnimationOptions.curveEaseIn, UIViewAnimationOptions.allowUserInteraction], animations: {
                self.view.layoutIfNeeded()
            }) { (true) in
                self.isScrolling = false
            }
        }
    }
    
    func configureCell( cell : ProductTableViewCell, indexPath : IndexPath )
    {
            cell.productName.text = (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "name") as! String?
                if ( (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "category_ids") as? [Any] != nil )
                {
                    let brandIdentifier : [Any] = (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "category_ids") as! [Any]
                    for j in 0...brandIdentifier.count-1 {
                        for i in 0...self.brands.count-1 {
                            if self.brands[i].identifier == (brandIdentifier[j] as AnyObject).int64Value{
                                cell.brandName.text = self.brands[i].name
                            }
                        }
                    }
                }
                if (( (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "you_pay"))! as AnyObject).floatValue != 0.0{
                    cell.originalPrice.text = "$" + String ((( (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "price"))! as AnyObject).floatValue)
                    
                    let offer = ((((( (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "price"))! as AnyObject).floatValue) - ((( (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "you_pay"))! as AnyObject).floatValue))/((( (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "price"))! as AnyObject).floatValue)) * 100
                    
                    cell.offerLabel.text = "Cashback " + String(Int(ceil(offer))) + "%"
                }
        
                cell.discountPrice.text = "$" + String ((( (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "you_pay"))! as AnyObject).floatValue)
        
        
                let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(self.addItemQuantity(_:)) )
                cell.cartView.addGestureRecognizer( gesture )
                cell.cartView.tag = indexPath.row
                if let url = (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "image")
                {
                    cell.productImage.sd_setImage( with : URL( string : url as! String )!, placeholderImage : UIImage( named : "product1")! )
                }
                else{
                    cell.productImage.image = UIImage( named : "product1")!
        }
    }
    
    func addItemQuantity( _ gesture : UITapGestureRecognizer )
    {
        let qtyAlert = UIAlertController(title: "Enter Quantity", message: "", preferredStyle: .alert)
        
        qtyAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        qtyAlert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            if self.quantityTextField?.text != nil
            {
                //if (self.quantityTextField?.text?.characters.count)! > 0
                // {
                self.quantity = Int((self.quantityTextField?.text!)!)!
                if self.quantity > 0
                {
                    if ((self.arrayProducts[(gesture.view?.tag)!]as! NSDictionary).value(forKey: "image") != nil)
                    {
                        Utility.addProductToCart(productId :(self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "product_id") as! String, quantity : self.quantity, name : (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "name") as! String, sku : (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "sku") as! String, discountedPrice : (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "you_pay") as! String, originalPrice : String ((( (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "price"))! as AnyObject).floatValue), imgUrl: (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "image") as! String)
                    }
                    else
                    {
                        Utility.addProductToCart(productId :(self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "product_id") as! String, quantity : self.quantity, name : (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "name") as! String, sku : (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "sku") as! String, discountedPrice : (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "you_pay") as! String, originalPrice : String ((( (self.arrayProducts[ (gesture.view?.tag)! ]as! NSDictionary).value(forKey: "price"))! as AnyObject).floatValue), imgUrl: "product1")
                    }
                    
                }
                else
                {
                    self.view.makeToast(message: "Please enter a valid quantity", duration: 3.0, position: HRToastPositionTop as AnyObject )
                }
                //}
                //else{
                //self.view.makeToast(message: "Please enter a valid quantity", duration: 3.0, position: HRToastPositionTop as AnyObject )
                // }
            }
            else
            {
                self.view.makeToast(message: "Please enter a quantity", duration: 3.0, position: HRToastPositionTop as AnyObject )
            }
            
        }))
        qtyAlert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.delegate = self
            textField.placeholder = "Quantity"
            textField.keyboardType = UIKeyboardType.numberPad
            self.quantityTextField = textField
            
        })
        present(qtyAlert, animated: true, completion: nil)
    }
    
    func cartViewRound ( )->Void
    {
        self.cartCountView.layer.borderWidth = 0
        self.cartCountView.layer.masksToBounds = false
        self.cartCountView.layer.cornerRadius = 7
        self.cartCountView.clipsToBounds = true
    }
    
    
    
    func searchBarConfig()
    {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.dimsBackgroundDuringPresentation = false
        searchController?.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController?.searchBar
        self.searchController?.searchBar.text = ""
        
        self.searchController?.searchBar.barStyle = .default;
        self.searchController?.searchBar.searchBarStyle = .minimal;
        self.searchController?.searchBar.setSearchFieldBackgroundImage( UIImage( color : UIColor.white, size : CGSize( width : 1, height : 15 ) ), for: UIControlState.normal )
        
        self.searchController?.searchBar.backgroundColor = UIColor.black
        self.searchController?.searchBar.tintColor = UIColor.white
        self.searchController?.searchBar.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0);
        
        self.searchController.searchBar.delegate=self
        
        let textFieldInsideSearchBar = self.searchController?.searchBar.value(forKey: "searchField") as? UITextField
        
        
        self.searchController?.searchBar.setSearchFieldBackgroundImage( UIImage( color : UIColor.white, size : CGSize( width : 1, height : 15 ) ), for: UIControlState.normal )
        textFieldInsideSearchBar?.textColor = UIColor.darkGray
        textFieldInsideSearchBar?.tintColor = UIColor.black
        textFieldInsideSearchBar?.background = UIImage()
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        textFieldInsideSearchBar?.layer.cornerRadius = 5.0
        textFieldInsideSearchBar?.clipsToBounds = true
        textFieldInsideSearchBar?.font  = UIFont (name: "Quicksand-Regular", size: 15)
        definesPresentationContext = true
        
    }
    
    func searchProductsWebService( searchString: String)
    {
        if (Reachability.isConnectedToNetwork() == true){
            if currentPageSearch==1 {
                SwiftLoader.show(title: "Searching...", animated: true)
            }
            
            let paramArray: [Any]
            
            if isHome == true {
                let dict = ["page_no": currentPageSearch!, "count": "50", "serachterm" : searchString] as [String : Any]
                paramArray = ["custom_api.productsearch",dict]
            }
            else if brandId != nil{
                let dict = ["categoryid" : String(format:"%d", categoryId!), "brandid" : String(format:"%d", brandId!), "serachterm" : searchString] as [String : Any]
                paramArray = ["custom_api.productsearchinbrandcat",dict]
            }
            else{
                let dict = ["page_no": currentPageSearch!, "count": "50", "categoryid" : String(format:"%d", categoryId!), "serachterm" : searchString] as [String : Any]
                paramArray = ["custom_api.productsearchbycat",dict]
            }

            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                
                if (responseObject != nil){
                    let products = (responseObject as! NSArray) as Array
                    let attributes = ["msrp", "price", "color", "size", "image"]
                    let calls = NSMutableArray()
                    for product in products {
                        calls.add(["catalog_product_attribute_media.list", product.value(forKey: "product_id") as! NSString])
                        calls.add(["catalog_product.info", product.value(forKey: "product_id") as! NSString, 1, attributes])
                    }
                    Magento.multiCall((calls as NSArray) as! [Any], success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                        var newProductParts : NSArray
                        newProductParts=responseObject as! NSArray
                        
                        (products as NSArray).enumerateObjects({ ( product, index, stop) in
                            var predicate: NSPredicate?
                            predicate = NSPredicate(format: "exclude == '0'")
                            if(((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject != nil){
                                let image : NSDictionary = ((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject as! NSDictionary
                                (product as! NSMutableDictionary).setObject(image.object(forKey: "url") as Any, forKey: "image" as  NSCopying)
                            }
                            (product as! NSMutableDictionary).addEntries(from: newProductParts[index+index+1] as! [AnyHashable : Any])
                        })
                        SwiftLoader.hide( )

                        if products.count>0{
                            if self.currentPageSearch==1 {
                                self.arrayProducts = products
                                self.tableView.tableFooterView = self.activityView
                            }
                            else{
                                self.arrayProducts = self.arrayProducts + products
                            }

                            if self.brandId == nil{
                                self.totalCountSearch = (((self.arrayProducts[0]as! NSDictionary).value(forKey: "totalcount") as! NSInteger))
                                self.currentPageSearch = self.currentPageSearch! + 1
                            }
                            else{
                                self.totalCountSearch = self.arrayProducts.count
                            }

                            print(self.arrayProducts)
                            
                            for i in 0...self.appDelegate.appDelegateHomeData.children.count-1 {
                                
                                if (self.appDelegate.appDelegateHomeData.children[i].name == "Brands"){
                                    self.brands = self.appDelegate.appDelegateHomeData.children[i].children
                                }
                            }

                            self.tableView.reloadData()
                        }
                        else{
                            self.view.makeToast(message: "No product found!", duration: 3.0, position: HRToastPositionTop as AnyObject )
                        }
                        
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                        SwiftLoader.hide()
                        
                    })
                }
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                SwiftLoader.hide()
            })
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    func productsListWebService()
    {
        if (Reachability.isConnectedToNetwork() == true){
            if currentPage==1 {
                SwiftLoader.show(title: "Please wait...", animated: true)
            }
            print(categoryId!)
            let paramArray : [Any]
            if brandId != nil{
                let dict = ["categoryid" : String(format:"%d", categoryId!), "brandid" : String(format:"%d", brandId!)] as [String : Any]
                paramArray = ["custom_api.productexistinbrandcat",dict]
            }
            else{
                let dict = ["page_no": currentPage!, "count": "50", "categoryid" : String(format:"%d", categoryId!)] as [String : Any]
                 paramArray = ["custom_api.productcatpagination",dict]
            }
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                if (responseObject != nil ){
                    let products = (responseObject as! NSArray) as Array
                    let attributes = ["msrp", "price", "color", "size", "image"]
                    let calls = NSMutableArray()
                    for product in products {
                        calls.add(["catalog_product_attribute_media.list", product.value(forKey: "product_id") as! NSString])
                        calls.add(["catalog_product.info", product.value(forKey: "product_id") as! NSString, 1, attributes])
                    }
                    Magento.multiCall((calls as NSArray) as! [Any], success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                        var newProductParts : NSArray
                        newProductParts=responseObject as! NSArray
                        
                        (products as NSArray).enumerateObjects({ ( product, index, stop) in
                            var predicate: NSPredicate?
                            predicate = NSPredicate(format: "exclude == '0'")
                            if(((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject != nil){
                                let image : NSDictionary = ((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject as! NSDictionary
                                (product as! NSMutableDictionary).setObject(image.object(forKey: "url") as Any, forKey: "image" as  NSCopying)
                            }
                            (product as! NSMutableDictionary).addEntries(from: newProductParts[index+index+1] as! [AnyHashable : Any])
                        })
                        SwiftLoader.hide( )
                        
                        if products.count>0{
                            if self.currentPage==1 {
                                self.arrayProducts = products
                                self.tableView.tableFooterView = self.activityView
                            }
                            else{
                                self.arrayProducts = self.arrayProducts + products
                            }
                            if self.brandId == nil{
                                self.totalCount = (((self.arrayProducts[0]as! NSDictionary).value(forKey: "totalcount") as! NSInteger))
                                self.currentPage = self.currentPage! + 1
                            }
                            else{
                                 self.totalCount = self.arrayProducts.count
                            }
                            print(self.arrayProducts)
                            self.arrayProductsWithoutSearch = self.arrayProducts
                            
                            for i in 0...self.appDelegate.appDelegateHomeData.children.count-1 {
                                
                                if (self.appDelegate.appDelegateHomeData.children[i].name == "Brands"){
                                    self.brands = self.appDelegate.appDelegateHomeData.children[i].children
                                }
                            }
                            self.tableView.reloadData()
                           // self.getCartTotal()
                        }
                        else{
                            self.view.makeToast(message: "No product found!", duration: 3.0, position: HRToastPositionTop as AnyObject )
                        }
                        
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                        SwiftLoader.hide()
                        
                    })
                }
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                SwiftLoader.hide()
            })
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
}

extension ProductsViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell", for: indexPath)  as! ProductTableViewCell
        cell.selectionStyle = .none
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        configureCell(cell: cell, indexPath: indexPath)
        
        if (searchString != nil) {
            if(lastPageBoolSearch==false)
            {
                if(indexPath.row==self.arrayProducts.count-1 )
                {
                    if(self.arrayProducts.count < totalCountSearch!)
                    {
                        if self.searchController == nil && self.currentPageSearch == 2
                        {
                            self.tableView.tableFooterView = self.activityView
                        }

                        activity.isHidden=false
                        activity.startAnimating()
                        searchProductsWebService(searchString: searchString!)
                    }
                    else
                    {
                        lastPageBoolSearch=true
                        activity.isHidden=true
                        activity.stopAnimating()
                        
                        self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                    }
                }
            }
        }
        else{
            if(lastPageBool==false)
            {
                if(indexPath.row==self.arrayProducts.count-1 )
                {
                    if(self.arrayProducts.count < totalCount!)
                    {
                        activity.isHidden=false
                        activity.startAnimating()
                        productsListWebService()
                    }
                    else
                    {
                        lastPageBool=true
                        activity.isHidden=true
                        activity.stopAnimating()
                        
                        self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                    }
                }
            }

        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayProducts.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        vc.title = (self.arrayProducts[ indexPath.row ]as! NSDictionary).value(forKey: "name") as! String?
        vc.productDict = self.arrayProducts[ indexPath.row ] as! NSDictionary
        vc.brands = self.brands
        
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }}

extension Notification.Name {
    static let cartCount = Notification.Name("cartCount")
}


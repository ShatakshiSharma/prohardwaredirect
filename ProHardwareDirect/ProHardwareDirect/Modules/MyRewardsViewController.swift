
//
//  MyRewardsViewController.swift
//  ProHardwareDirect
//
//  Created by Yogita on 29/12/16.
//  Copyright © 2016 Mobrill. All rights reserved.
//

class MyRewardsViewController : UIViewController
{
    var panGesture : UIPanGestureRecognizer?
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrRewards : [NSDictionary] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes = [NSFontAttributeName: UIFont (name: "Quicksand-Bold", size: 17)!];
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal);
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_register" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        
        panGesture = UIPanGestureRecognizer()
        panGesture = UIPanGestureRecognizer(target: self, action:#selector(self.panGestureRecognized(sender:)))
        self.view.addGestureRecognizer(panGesture!)
        
        getRewardsWebService()
        
        tableView.register(UINib.init(nibName: "MyRewardsTableViewCell", bundle: nil), forCellReuseIdentifier: "MyRewardsTableViewCell")
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 18)!, NSForegroundColorAttributeName    : UIColor.white ]
        self.navigationController!.navigationBar.barStyle = UIBarStyle.default
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = navigationColor
        self.navigationController!.navigationBar.isTranslucent = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func leftNavigationBarButtonClicked( )
    {
        _ = ( self.frostedViewController.contentViewController as? UINavigationController )?.popToRootViewController(animated: true)
        self.view.endEditing(true)
    }
    
    
    func panGestureRecognized(sender : UIPanGestureRecognizer)
    {
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    func configure( cell : MyRewardsTableViewCell, indexPath : NSIndexPath )
    {
        let reward = arrRewards[indexPath.row]
        
        cell.Rewardtitle.text = reward.value(forKey: "title") as! String?
        cell.points.text = (reward.value(forKey: "point_amount") as! String?)! + " pts"
        cell.expDate.text = reward.value(forKey: "expiration_date") as! String?
    }

    func getRewardsWebService()
    {
        if (Reachability.isConnectedToNetwork() == true)
        {
            SwiftLoader.show(title: "Loading Categories...", animated: true)
            
            let paramArray: [Any] = ["rewardpoints_transaction.list",["customer_id": UserDefaults.standard.string(forKey: k_userID)!]]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in                print(responseObject!)
                SwiftLoader.hide()
                self.arrRewards = (responseObject as! [NSDictionary])
                if self.arrRewards.count>0{
                    self.tableView.reloadData()
                }
                else{
                    self.view.makeToast(message: "No Rewards", duration: 3.0, position: HRToastPositionTop as AnyObject )
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print(error as Any)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                
            })
        }
            
        else
        {
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

}


extension MyRewardsViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyRewardsTableViewCell", for: indexPath)  as! MyRewardsTableViewCell
        cell.selectionStyle = .none
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        configure(cell: cell, indexPath: indexPath as NSIndexPath)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrRewards.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
      
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }}


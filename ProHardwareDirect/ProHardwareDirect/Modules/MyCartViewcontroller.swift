//
//  MyCartViewcontroller.swift
//  ToDool
//
//  Created by Yogita on 20/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData

class MyCartViewcontroller : UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var emptyCartLabel: UILabel!
    
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var earnLabel:UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var discountValueLabel:UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var shippingTitleLbl: UILabel!
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var earningPoints: UILabel!
    @IBOutlet weak var shippingPrice: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var grandTotalExTaxes: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var grandTotalInclTaxes: UILabel!
    weak var activeTextField: UITextField?
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var tableHeight : NSLayoutConstraint!
    @IBOutlet weak internal var lineTop: NSLayoutConstraint!
    @IBOutlet weak var earnTop: NSLayoutConstraint!
    @IBOutlet weak internal var shoppingyOriginYView: NSLayoutConstraint!
    @IBOutlet weak internal var shoppingyOriginYLine: NSLayoutConstraint!
    
    var quantityTextField : UITextField!
    var isDiscount = false
    var isCashback = false
    var qtyTextField : UITextField!
    var appDelegate : AppDelegate!
    var billingAddressValue : String!
    var shippingAddressValue : String!

    var arrCart : [NSDictionary] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "My Cart"
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage( named: "cross" ), style: .plain, target: self, action: #selector(self.rightNavigationBarButtonClicked) )
        
        tableView.register(UINib.init(nibName: "MyCartTableViewCell", bundle: nil), forCellReuseIdentifier: "MyCartTableViewCell")
        tableView.autoresizingMask = UIViewAutoresizing(rawValue:UIViewAutoresizing.flexibleWidth.rawValue | UIViewAutoresizing.flexibleHeight.rawValue)
        tableView.backgroundColor = UIColor.clear
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140

        Utility.addPaddingToTextField(textfield: self.couponTextField)
        Utility.textFieldBorderColor(self.couponTextField, color:  UIColor( hex : 0xDBDCDD )!)
        let attributes = [
            NSForegroundColorAttributeName: UIColor( hex : 0x303030 ),
            NSFontAttributeName : UIFont(name: "Quicksand-Regular", size: 12)!         ]
        self.couponTextField.attributedPlaceholder = NSAttributedString(string: "Add coupon", attributes:attributes)
        
        //Gesture
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(self.hideKeyboardOnTap(_:)) )
        gesture.delegate = self
        self.view.addGestureRecognizer( gesture )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.scrollView.isHidden = true
        self.emptyCartLabel.isHidden = true
        self.navigationItem.rightBarButtonItem?.isEnabled = false

            if (UserDefaults.standard.value(forKey: k_quoteID) != nil ){
                if (UserDefaults.standard.object(forKey: "cartArray") != nil){
                    self.addProductsToCart()
                }
                else
                {
                    if (Reachability.isConnectedToNetwork() == true){
                        SwiftLoader.show(title: "Loading..", animated: true)
                        self.getCart()
                    }
                    else{
                        self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
                    }
                }
            }
            else{
                 if (UserDefaults.standard.object(forKey: "cartArray") != nil){
                    arrCart = UserDefaults.standard.value(forKey: "cartArray") as! [NSDictionary]
                    
                    self.scrollView.isHidden = false
                    self.emptyCartLabel.isHidden = true
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                    self.tableView.reloadData()
                }
                else
                 {
                    self.scrollView.isHidden = true
                    self.emptyCartLabel.isHidden = false
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
            }
    }
    
    func addProductsToCart()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Loading..", animated: true)
            
            let arrCartProducts = (UserDefaults.standard.value(forKey: "cartArray") as! NSArray)
            let arrProducts = NSMutableArray()
            var object : NSDictionary!
            for i in 0...(arrCartProducts.count)-1 {
                object = ["product_id": (((arrCartProducts[i]) as! NSDictionary).value(forKey: "productId"))!,
                          "sku": (((arrCartProducts[i]) as! NSDictionary).value(forKey: "sku"))!,
                          "qty": ((arrCartProducts[i]) as! NSDictionary).value(forKey: "quantity")!,
                          "options" : "",
                          "bundle_option" : "",
                          "bundle_option_qty" : "",
                          "links" : ""]
                arrProducts.add(object)
            }
            let paramArray: [Any] = ["cart_product.add", UserDefaults.standard.string(forKey: k_quoteID)!,arrProducts]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                if responseObject as! Bool == true{
                    UserDefaults.standard.removeObject(forKey: "cartCount")
                    UserDefaults.standard.removeObject(forKey: "cartArray")
                    self.getCart()
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func getCart()
    {
        let paramArray: [Any] = ["cart.info", UserDefaults.standard.string(forKey: k_quoteID)!]
        
        Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
            print(responseObject!)
            
            if (((responseObject as! NSDictionary).value(forKey: "items") as! NSArray).count > 0 ){
                
                self.grandTotalInclTaxes.text = "$" + String(format: "%.2f", ((responseObject as! NSDictionary).value(forKey: "grand_total") as AnyObject).floatValue)
                self.grandTotalExTaxes.text =  "$" + String(format: "%.2f", ((responseObject as! NSDictionary).value(forKey: "subtotal_with_discount") as AnyObject).floatValue)
                self.totalPrice.text =  "$" + String(format: "%.2f", ((responseObject as! NSDictionary).value(forKey: "subtotal") as AnyObject).floatValue)
                self.taxLabel.text =  "$" + String(format: "%.2f", ((((responseObject as! NSDictionary).value(forKey: "shipping_address") as! NSDictionary).value(forKey: "tax_amount")) as AnyObject).floatValue)
                self.couponTextField.text = ((responseObject as! NSDictionary).value(forKey: "coupon_code") as! String)
                if ((((responseObject as! NSDictionary).value(forKey: "shipping_address") as! NSDictionary).value(forKey: "base_discount_amount")) != nil && (((responseObject as! NSDictionary).value(forKey: "shipping_address") as! NSDictionary).value(forKey: "base_discount_amount")) as! String != "0.0000")
                {
                    self.discountValueLabel.text =  "-$" + String(format: "%.2f", abs(((((responseObject as! NSDictionary).value(forKey: "shipping_address") as! NSDictionary).value(forKey: "base_discount_amount")) as AnyObject).floatValue))
                    self.isDiscount = true
                }

                if ((((responseObject as! NSDictionary).value(forKey: "billing_address") as! NSDictionary).value(forKey: "free_shipping")) as! String) == "0"
                {
                    self.shippingTitleLbl.text = ""
                }
                else{
                    self.shippingPrice.text = "$" + String(format: "%.2f", ((((responseObject as! NSDictionary).value(forKey: "billing_address") as! NSDictionary).value(forKey: "shipping_amount")) as AnyObject).floatValue)
                }
//                shippingAddressValue =
                
                    let products = ((responseObject as! NSDictionary).value(forKey: "items") as! NSArray) as Array
                    let attributes = ["msrp", "price", "color", "size", "image"]
                    let calls = NSMutableArray()
                    for product in products {
                        calls.add(["catalog_product_attribute_media.list", product.value(forKey: "product_id") as! NSString])
                        calls.add(["catalog_product.info", product.value(forKey: "product_id") as! NSString, 1, attributes])
                    }
                    Magento.multiCall((calls as NSArray) as! [Any], success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                        var newProductParts : NSArray
                        newProductParts=responseObject as! NSArray
                        
                        (products as NSArray).enumerateObjects({ ( product, index, stop) in
                            var predicate: NSPredicate?
                            predicate = NSPredicate(format: "exclude == '0'")
                            if(((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject != nil){
                                let image : NSDictionary = ((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject as! NSDictionary
                                (product as! NSMutableDictionary).setObject(image.object(forKey: "url") as Any, forKey: "image" as  NSCopying)
                            }
                            (product as! NSMutableDictionary).addEntries(from: newProductParts[index+index+1] as! [AnyHashable : Any])
                        })
                        SwiftLoader.hide( )
                        self.arrCart = (products  as! [NSDictionary])
                        print(self.arrCart)
                        
                        self.scrollView.isHidden = false
                        self.emptyCartLabel.isHidden = true
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                        self.tableView.reloadData()
                        self.updateViewConstraints()
                        
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                        SwiftLoader.hide()
                    })
            }
            else{
                SwiftLoader.hide()

                self.scrollView.isHidden = true
                self.emptyCartLabel.isHidden = false
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
            print("errorrr:",(error?.localizedDescription)!)
            SwiftLoader.hide()
            self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
        })
    }
    
    func deleteProductFromCart(index : Int)
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Deleting..", animated: true)
            
            let arrProducts = NSMutableArray()
            var object : NSDictionary!
            object = ["product_id": ((arrCart[index]).value(forKey: "product_id"))!,
                      "sku": ((arrCart[index]).value(forKey: "sku"))!,
                      "qty": ((arrCart[index]).value(forKey: "qty"))!,
                      "options" : "",
                      "bundle_option" : "",
                      "bundle_option_qty" : "",
                      "links" : ""]
            arrProducts.add(object)
            let paramArray: [Any] = ["cart_product.remove", UserDefaults.standard.string(forKey: k_quoteID)!,arrProducts]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                if responseObject as! Bool == true{
                    self.getCart()
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func updateProductInCart(index : Int, quantity : Int)
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Updating..", animated: true)
            
            let arrProducts = NSMutableArray()
            var object : NSDictionary!
            object = ["product_id": ((arrCart[index]).value(forKey: "product_id"))!,
                      "sku": ((arrCart[index]).value(forKey: "sku"))!,
                      "qty": quantity,
                      "options" : "",
                      "bundle_option" : "",
                      "bundle_option_qty" : "",
                      "links" : ""]
            arrProducts.add(object)
            let paramArray: [Any] = ["cart_product.update", UserDefaults.standard.string(forKey: k_quoteID)!,arrProducts]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                if responseObject as! Bool == true{
                    self.getCart()
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func emptyCart()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Deleting..", animated: true)
            
            let arrProducts = NSMutableArray()
            var object : NSDictionary!
            for index in 0...(arrCart.count)-1 {
                object = ["product_id": ((arrCart[index]).value(forKey: "product_id"))!,
                          "sku": ((arrCart[index]).value(forKey: "sku"))!,
                          "qty": ((arrCart[index]).value(forKey: "qty"))!,
                          "options" : "",
                          "bundle_option" : "",
                          "bundle_option_qty" : "",
                          "links" : ""]
                arrProducts.add(object)
            }
            let paramArray: [Any] = ["cart_product.remove", UserDefaults.standard.string(forKey: k_quoteID)!,arrProducts]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                SwiftLoader.hide()

                if responseObject as! Bool == true{
                    self.arrCart = []
                    self.tableView.reloadData()
                    self.scrollView.isHidden = true
                    self.emptyCartLabel.isHidden = false
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        if isDiscount {
            self.shoppingyOriginYView.priority = 999
            self.shoppingyOriginYLine.priority = 800
            self.discountLabel.text = "Discount"
            self.earnLabel.isHidden = true
            self.earningPoints.isHidden = true
            self.lineLabel.isHidden = true
        }
        else
        {
            self.shoppingyOriginYView.priority = 800
            self.shoppingyOriginYLine.priority = 999
            self.discountLabel.text = ""
            self.discountValueLabel.text = ""
            self.earningPoints.text = ""
        }
        self.tableHeight.constant = tableView.contentSize.height
    }
    
    func getButtonIndexPath(button: UIButton) ->IndexPath
    {
        let buttonFrame = button.convert(button.bounds, to: tableView)
        return tableView.indexPathForRow(at: buttonFrame.origin)!
    }
    
    func configure( cell : MyCartTableViewCell, indexPath : NSIndexPath )
    {
        if((UserDefaults.standard.value(forKey: k_quoteID)) != nil)
        {
            let product = arrCart[indexPath.row]
            
            cell.productName.text = product.value(forKey: "name") as! String?
            cell.productNumber.text = product.value(forKey: "sku") as! String?
            cell.qtyTextField.text = String(product.value(forKey: "qty") as! Int)
            cell.originalPrice.text = "$" + String(format: "%.2f", (product.value(forKey: "price")as AnyObject).floatValue)
            cell.earningLabel.text = ""
            cell.discountPrice.text = "$" + String(format: "%.2f", (product.value(forKey: "row_total") as AnyObject).floatValue)
            if ((product.value(forKey: "image") != nil) && !(product.value(forKey: "image") as! String == "product1" ))
            {
                cell.productImage.sd_setImage( with : URL( string : (product.value(forKey: "image") as! String )), placeholderImage : UIImage( named : "product1")!)
            }
        }
        else if((UserDefaults.standard.object(forKey: "cartArray")) != nil)
        {
            let product = arrCart[indexPath.row]
            
            cell.productName.text = product.value(forKey: "name") as! String?
            cell.productNumber.text = product.value(forKey: "sku") as! String?
            cell.qtyTextField.text = String(product.value(forKey: "quantity") as! Int)
            cell.originalPrice.text = ""
            cell.earningLabel.text = ""
            cell.discountPrice.text = "$" + (product.value(forKey: "price") as! String?)!
            if ((product.value(forKey: "img_url") != nil) && !(product.value(forKey: "img_url") as! String == "product1" ))
            {
                cell.productImage.sd_setImage( with : URL( string : (product.value(forKey: "img_url") as! String )), placeholderImage : UIImage( named : "product1")!)
            }
        }
        self.updateViewConstraints()
    }
    
    // MARK:--------IBAction-------
    
    func leftNavigationBarButtonClicked() {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
    }
    
    func rightNavigationBarButtonClicked() {
        let alert = UIAlertController(title: "", message: "Are you sure you want to clear cart?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
                self.emptyCart()
            }
            else{
                UserDefaults.standard.removeObject(forKey: "cartCount")
                UserDefaults.standard.removeObject(forKey: "cartArray")
                self.tableView.reloadData()
                self.scrollView.isHidden = true
                self.emptyCartLabel.isHidden = false
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                NotificationCenter.default.post(name: .cartCount, object: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func detailBtnClick(_ sender: UIButton) {
        //        var indexpath  : IndexPath!
        //        indexpath = self.getButtonIndexPath(button: sender)
        //        if (self.activeTextField != nil)
        //        {
        //            self.activeTextField?.resignFirstResponder()
        //            self.activeTextField = nil
        //        }
        //        else{
        //
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        //            vc.product = self.kartList[ indexpath.row ].product
        //            ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
        //        }
    }
    
    func qtyBtnClick(_ sender: UIButton) {
        var indexpath  : IndexPath!
        indexpath = self.getButtonIndexPath(button: sender)
        let cell = self.tableView.cellForRow(at: indexpath)
        let txtfld = cell?.contentView.viewWithTag(1 + indexpath.row) as! UITextField
        let quantity = txtfld.text
        
        let qtyAlert = UIAlertController(title: "Enter Quantity", message: "", preferredStyle: UIAlertControllerStyle.alert)
        qtyAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        qtyAlert.addAction(UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            let currentValue = Int( self.quantityTextField.text! )!
            if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
                self.updateProductInCart(index: indexpath.row, quantity: currentValue)
            }
            else{
                let product = self.arrCart[indexpath.row]
                Utility.addItemQuantiity(productId: product.object(forKey: "productId") as! String, quantity: currentValue)
                self.tableView.reloadData()
                self.updateViewConstraints()
            }
        }))
        qtyAlert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.delegate = self
            textField.placeholder = "Quantity"
            textField.text = quantity
            textField.keyboardType = UIKeyboardType.numberPad
            self.quantityTextField = textField
        })
        
        present(qtyAlert, animated: true, completion: nil)
    }
    
    @IBAction func applyCouponBtnClick(_ sender: UIButton) {
        if (UserDefaults.standard.string(forKey: k_userID) != nil){
            if (Reachability.isConnectedToNetwork() == true){
                self.activeTextField?.resignFirstResponder()
                SwiftLoader.show(title: "Applying Coupon..", animated: true)
                
                let paramArray: [Any] = ["cart_coupon.add", UserDefaults.standard.string(forKey: k_quoteID)!,couponTextField.text!]
                
                Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                    print(responseObject as! Bool)
                    self.getCart()
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    print("errorrr:",(error?.localizedDescription)!)
                    SwiftLoader.hide()
                    self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
                })
            }
            else{
                self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
            }
        }
        else
        {
            self.view.makeToast(message: k_please_login, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    @IBAction func checkoutBtnCLick(_ sender: UIButton) {
       // self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderReviewViewController") as! OrderReviewViewController
        vc.arrCart = arrCart
        vc.subtotalValue = self.grandTotalExTaxes.text
        vc.grandtotalValue = self.grandTotalInclTaxes.text
        vc.shippingHandlingValue = self.shippingPrice.text
        vc.discountValue = self.discountValueLabel.text
        vc.taxValue = self.taxLabel.text
        vc.couponValue = self.couponTextField.text
        
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    // MARK:----Textfield delegates----
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.quantityTextField?.text != nil
        {
            if textField == self.quantityTextField {
                let currentCharacterCount = textField.text?.characters.count ?? 0
                if (range.length + range.location > currentCharacterCount){
                    return false
                }
                let newLength = currentCharacterCount + string.characters.count - range.length
                return newLength <= 3
            }
        }
        return true
    }
    
    func hideKeyboardOnTap( _ gesture : UITapGestureRecognizer )
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if(touch.view?.isDescendant(of: tableView))!
        {
            return false
        }
        else{
            return true
        }
    }
}


extension MyCartViewcontroller : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
            if arrCart.count > 0 {
                return arrCart.count
            }
            else{
                return 0
            }
        }
        else if (UserDefaults.standard.value(forKey: "cartArray") != nil)
        {
            return arrCart.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "MyCartTableViewCell"
        let cell: MyCartTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyCartTableViewCell
        cell.qtyTextField.delegate = self
        //   cell.detailBtn.addTarget(self, action:#selector(self.detailBtnClick(_:)), for: UIControlEvents.touchUpInside)
        cell.qtyBtn.addTarget(self, action:#selector(self.qtyBtnClick(_:)), for: UIControlEvents.touchUpInside)
        cell.qtyTextField.tag = 1 + indexPath.row
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        configure( cell: cell, indexPath:  indexPath as NSIndexPath )
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        
        vc.quantityCount = self.arrCart[ indexPath.row ].value(forKey: "qty") as! Int
        vc.isCartItem = true
        vc.index = indexPath.row
        vc.productDict = self.arrCart[ indexPath.row ]
        vc.title = (self.arrCart[ indexPath.row ]).value(forKey: "name") as! String?
        for i in 0...self.appDelegate.appDelegateHomeData.children.count-1 {
            if (self.appDelegate.appDelegateHomeData.children[i].name == "Brands"){
                vc.brands = self.appDelegate.appDelegateHomeData.children[i].children
            }
        }
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
                deleteProductFromCart(index: indexPath.row)
            }
            else
            {
                if(arrCart.count == 1)
                {
                    UserDefaults.standard.removeObject(forKey: "cartCount")
                    UserDefaults.standard.removeObject(forKey: "cartArray")
                    self.tableView.reloadData()
                    self.scrollView.isHidden = true
                    self.emptyCartLabel.isHidden = false
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                    NotificationCenter.default.post(name: .cartCount, object: nil)
                }
                else
                {
                    let product = arrCart[indexPath.row]
                    Utility.deleteCartProduct(productId: product.object(forKey: "productId") as! String)
                    self.tableView.reloadData()
                    self.updateViewConstraints()
                }
            }
        }
    }
    
    
}

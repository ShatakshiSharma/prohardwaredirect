//
//  ShippingAddressViewController.swift
//  ToDool
//
//  Created by Yogita on 27/09/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class ShippingAddressViewController : UIViewController, UIGestureRecognizerDelegate, UISearchBarDelegate, UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet var topDistance: NSLayoutConstraint!
    
    @IBOutlet var popUpLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var addressLineTwoTxtFld: UITextField!
    @IBOutlet weak var addressLineOneTxtFld: UITextField!
    @IBOutlet weak var postcodeTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var billCheckImage: UIImageView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var viewBottom : NSLayoutConstraint!
    @IBOutlet weak var CountryViewBG: UIView!
    @IBOutlet weak var CountryView: UIView!
    
    @IBOutlet weak var CountryTableView: UITableView!
    
    
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var textFieldInsideSearchBar : UITextField?
    var countryArray : NSArray!
    var stateArray : NSArray!
    var filteredCountryArray : [String]!
    var filteredStateArray : [String]!
    var countryNameArray : [String]!
    var stateNameArray : [String]!

    var selectedCountry : NSString!
    var selectedState : NSString!
    var isState : Bool!
    var selectedCountryCode : NSString!
    var selectedStateCode : NSString!

    var isStateTextFieldActive : Bool!
    var isCountryTextFieldActive : Bool!
    var isFilterCountry : Bool!
    var isFilterState : Bool!
    var isBilling : Bool!
    var isShipping : Bool!

    var isShippingAdded : Bool!
    var isBillingAdded : Bool!
    var isCart : Bool!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Add Address"
        
        self.countryArray = []
        self.countryNameArray = []
        self.filteredCountryArray = []
        self.filteredStateArray = []
        self.stateNameArray = []

        isStateTextFieldActive = false
        isCountryTextFieldActive = false
        isState = false
        isFilterCountry = false
        isFilterState = false
        isShipping = false
        isBilling = false

        self.CountryViewBG.backgroundColor=UIColor.black.withAlphaComponent(0.7)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Done", style:.plain, target:self, action:#selector(doneBtnClicked))
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(self.hideKeyboardOnTap(_:) ))
        self.contentView.addGestureRecognizer( gesture )
        
        self.searchBarConfig()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.CountryTableView.register(UINib.init(nibName: "CountryListTableViewCell", bundle: nil), forCellReuseIdentifier: "CountryListTableViewCell")
        
        for subview in self.contentView.subviews
        {
            if subview is UITextField
            {
                let textField = subview as! UITextField
                textField.addTarget(self, action: #selector(self.textFieldShouldReturn(_:)), for: UIControlEvents.editingDidEndOnExit)
                
                textField.addTarget(self, action: #selector(self.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingDidBegin)
            }
        }
        
        //----------TextFieldBorder-----------//
        
        Utility.textFieldBorderColor(self.firstNameTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.lastNameTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.mobileTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.addressLineOneTxtFld,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.addressLineTwoTxtFld,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.cityTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.countryTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.stateTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.postcodeTextField,color: textfieldLineBlackColor!)
        
        
        //-----------Add Padding-----------//
        Utility.addPaddingToTextField(textfield: self.firstNameTextField)
        Utility.addPaddingToTextField(textfield: self.lastNameTextField)
        Utility.addPaddingToTextField(textfield: self.mobileTextField)
        Utility.addPaddingToTextField(textfield: self.addressLineOneTxtFld)
        Utility.addPaddingToTextField(textfield: self.addressLineTwoTxtFld)
        Utility.addPaddingToTextField(textfield: self.cityTextField)
        Utility.addPaddingToTextField(textfield: self.countryTextField)
        Utility.addPaddingToTextField(textfield: self.stateTextField)
        Utility.addPaddingToTextField(textfield: self.postcodeTextField)
        
        self.countryArray = []
        self.stateArray = []
        self.isStateTextFieldActive = true
        self.countryListWebService()
        self.selectedCountry = "Australia"
        self.selectedCountryCode = "AU"
        self.countryTextField.text = self.selectedCountry as String?
        self.stateTextField.text = "Select State"
        self.stateListWebService(countryCode: self.selectedCountryCode)
        
        self.addClearButton()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    
    func validateAddressFields() ->Bool
    {
        let success = true
        
        if ( Utility.isEmpty(self.firstNameTextField.text!) )
        {
            self.view.makeToast(message: k_firstname_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(((self.firstNameTextField.text?.characters.count)! >= 2) && ((self.firstNameTextField.text?.characters.count)! <= 35)))
        {
            self.view.makeToast(message: k_firstname_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.lastNameTextField.text!) )
        {
            self.view.makeToast(message: k_lastname_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(((self.lastNameTextField.text?.characters.count)! >= 2) && ((self.lastNameTextField.text?.characters.count)! <= 35)))
        {
            self.view.makeToast(message: k_lastname_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.mobileTextField.text!) )
        {
            self.view.makeToast(message: k_mob_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
    
        else if (!((self.mobileTextField.text?.characters.count)! == 14))
        {
            self.view.makeToast(message: k_mob_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
       
        if ( Utility.isEmpty(self.addressLineOneTxtFld.text!) )
        {
            self.view.makeToast(message: k_address_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!((self.addressLineOneTxtFld.text?.characters.count)! >= 2))
        {
            self.view.makeToast(message: k_address_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.cityTextField.text!) )
        {
            self.view.makeToast(message: k_city_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!((self.cityTextField.text?.characters.count)! >= 2))
        {
            self.view.makeToast(message: k_city_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.postcodeTextField.text!) )
        {
            self.view.makeToast(message: k_postcode_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!((self.postcodeTextField.text?.characters.count)! == 6))
        {
            self.view.makeToast(message: k_postcode_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.stateTextField.text!) || (self.stateTextField.text! == "Select State"))
        {
            self.view.makeToast(message: k_state_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!((self.stateTextField.text?.characters.count)! >= 2))
        {
            self.view.makeToast(message: k_state_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        if ( Utility.isEmpty(self.countryTextField.text!) )
        {
            self.view.makeToast(message: k_country_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!((self.countryTextField.text?.characters.count)! >= 2))
        {
            self.view.makeToast(message: k_country_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
       
        
        return success;
        
    }

    
    // MARK:----Table view delegates----
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if(isCountryTextFieldActive == true)
        {
            if(self.isFilterCountry == true)
            {
                return self.filteredCountryArray.count
            }
            else
            {
                return (countryArray?.count)!
            }
        }
        
        if(isStateTextFieldActive == true)
        {
            if(self.isFilterState == true)
            {
                return self.filteredStateArray.count
            }
            else
            {
                return (stateArray?.count)!
            }
        }
            
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "CountryListTableViewCell"
        let cell: CountryListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? CountryListTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.gray
        
        if(isCountryTextFieldActive == true)
        {
            if(self.isFilterCountry == true)
            {
                cell.countryName.text = self.filteredCountryArray[indexPath.row]
            }
            else
            {
                cell.countryName.text = (countryArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as? String
            }
        }
        
        if(isStateTextFieldActive == true)
        {
            if(self.isFilterState == true)
            {
                cell.countryName.text = self.filteredStateArray[indexPath.row]
            }
            else
            {
                cell.countryName.text = (stateArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as? String
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(isCountryTextFieldActive == true)
        {
            if(self.isFilterCountry == true)
            {
              self.getCountryCode(selectedCountry: self.filteredCountryArray[indexPath.row] )
            }
            
            else
            {
                self.selectedCountry = (countryArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! NSString!
                self.selectedCountryCode = (countryArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "iso2_code") as! NSString!
            }
        }
        
        if(isStateTextFieldActive == true)
        {
            if(self.isFilterState == true)
            {
                self.getState(selectedState: self.filteredStateArray[indexPath.row])

            }
            else{
                self.selectedState = (stateArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! NSString!
                self.selectedStateCode = (stateArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "region_id") as! NSString!

            }
        }
        
    }
    
    
    func filterTableViewForEnterText(searchText: String) {
        if(isStateTextFieldActive == true){
            let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
            let array = (self.stateNameArray as NSArray).filtered(using: searchPredicate)
            self.filteredStateArray = array as! [String]
            self.CountryTableView.reloadData()
        }
        else{
            let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
            let array = (self.countryNameArray as NSArray).filtered(using: searchPredicate)
            self.filteredCountryArray = array as! [String]
            self.CountryTableView.reloadData()
        }
    }
    
    
    @IBAction func shipBtnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        switch sender.tag {
        case 200:
            self.checkImage.image = UIImage( named: "checkbox_myprofile")
            sender.tag = 100
            isShipping = true

            break
            
        case 100:
            self.checkImage.image = UIImage( named: "uncheck_myprofile")
            sender.tag = 200
            isShipping = false

            break
            
        default:
            break
        }
    }
    
    
    @IBAction func billBtnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        switch sender.tag {
        case 500:
            self.billCheckImage.image = UIImage( named: "checkbox_myprofile")
            sender.tag = 600
            isBilling = true
            break
            
        case 600:
            self.billCheckImage.image = UIImage( named: "uncheck_myprofile")
            sender.tag = 500
            isBilling = false

            break
            
        default:
            break
        }
    }
    
    // MARK:---- Done/Cancel BtnClick----
    
    @IBAction func doneBtnClick(_ sender: UIButton) {
        
        self.isFilterCountry = false
        self.isFilterState = false
        
        self.filteredCountryArray = []
        self.filteredStateArray = []

        if(!(selectedCountry == "" && selectedCountryCode == "") || !(selectedState == ""))
        {
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.view.endEditing(true)
       
        self.viewBottom.constant = -self.CountryView.frame.size.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.CountryViewBG.isHidden = true
        }) { (result) in
        }
        
        if (isCountryTextFieldActive == true)
        {
            self.countryTextField.text = self.selectedCountry as String?
            self.stateListWebService(countryCode: self.selectedCountryCode)
        }
        if(isStateTextFieldActive == true)
        {
            self.stateTextField.text = self.selectedState as String?
        }
        }
    }
    
    @IBAction func cancelBtnClick(_ sender: UIButton) {
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.isFilterCountry = false
        self.isFilterState = false

        self.filteredCountryArray = []
        self.filteredStateArray = []

        self.view.endEditing(true)
        self.viewBottom.constant = -self.CountryView.frame.size.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.CountryViewBG.isHidden = true
        }) { (result) in
        }
    }
    
    
    func doneBtnClicked()
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
        if (Reachability.isConnectedToNetwork() == true)
        {
            if(self.validateAddressFields())
            {
                if isCart == true {
                    addCartAddresses()
                }
                else
                {
                    if(isBilling == true && isBillingAdded == true)
                    {
                        self.view.makeToast(message: k_billing_address_added, duration: 3.0, position: HRToastPositionTop as AnyObject )
                    }
                    else if(isShipping == true && isShippingAdded == true)
                    {
                        self.view.makeToast(message: k_shipping_address_added, duration: 3.0, position: HRToastPositionTop as AnyObject )
                    }
                    else
                    {
                        addAddressWebService()
                    }
                }
            }
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    func addCartAddresses()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Loading..", animated: true)
            
            var arrStreet = NSArray()
            arrStreet = [addressLineOneTxtFld.text!, addressLineTwoTxtFld.text!]
            
            var mode : String!
            if isBillingAdded == true {
                mode = "billing"
            }
            else if isShippingAdded == true{
                mode = "shipping"
            }
            let dict = ["mode" : mode,
                        "firstname": firstNameTextField.text!,
                        "lastname": lastNameTextField.text!,
                        "street": arrStreet ,
                        "city": cityTextField.text!,
                        "country_id" : self.selectedCountryCode,
                        "region" : self.selectedStateCode,
                        "postcode" : postcodeTextField.text!,
                        "telephone" : mobileTextField.text!,
                        "is_default_billing" : isShipping,
                        "is_default_shipping" : isBilling] as [String : Any]

            let paramArray: [Any] = ["cart_customer.addresses", UserDefaults.standard.string(forKey: k_quoteID)!, [dict]]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject!)
                SwiftLoader.hide()

            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func addAddressWebService()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Please wait...", animated: true)
            
            var arrStreet = NSArray()
            arrStreet = [addressLineOneTxtFld.text!, addressLineTwoTxtFld.text!]
            
            let dict = ["firstname": firstNameTextField.text!,
                        "lastname": lastNameTextField.text!,
                        "street": arrStreet ,
                        "city": cityTextField.text!,
                        "country_id" : self.selectedCountryCode,
                        "region" : self.selectedStateCode,
                        "postcode" : postcodeTextField.text!,
                        "telephone" : mobileTextField.text!,
                        "is_default_billing" : isShipping,
                        "is_default_shipping" : isBilling] as [String : Any]

            let paramArray: [Any] = ["customer_address.create",UserDefaults.standard.object(forKey: k_userID)!, dict]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as Any)
                
                _ = ( self.frostedViewController.contentViewController as? UINavigationController )?.popViewController(animated: true)
                
                SwiftLoader.hide( self.CountryTableView.reloadData())
                
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                SwiftLoader.hide()
            })
            
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
            
        }
    }

    func leftNavigationBarButtonClicked( )
    {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
        self.view.endEditing(true)
    }

    
    
    //MARK:-------SearchBarDelegates----------
    
    func searchBar(_ searchBar: UISearchBar,textDidChange searchText: String)
    {
        if(searchText.characters.count > 0)
        {
            if(isCountryTextFieldActive == true)
            {
                self.isFilterCountry = true
            }
            else{
                self.isFilterState = true
            }
        }
        else
        {
            if(isCountryTextFieldActive == true)
            {
                self.isFilterCountry = false
            }
            else{
                self.isFilterState = false
            }
        }
        
        self.filterTableViewForEnterText(searchText: searchText)
    }
    
    
    // MARK:----Keyboard Func----
    
    func hideKeyboardOnTap( _ gesture : UITapGestureRecognizer )
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        self.keyboardIsShowing = true
        
        if let info = notification.userInfo {
            self.keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.keyboardAnimationTime = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            self.arrangeViewOffsetFromKeyboard()
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardIsShowing = false
        self.returnViewToInitialFrame()
    }
    
    func arrangeViewOffsetFromKeyboard()
    {
        if let tf = self.activeTextField
        {
            var frame = self.scrollView.frame
            if let rect : CGRect = self.keyboardFrame
            {
                frame.size.height -= rect.size.height
            }
            
            let union = frame.union( tf.frame )
            var diff = union.size.height - frame.size.height
            if tf ==  textFieldInsideSearchBar  {
                self.viewBottom.constant = self.CountryView.frame.height - 85
            }
            else {
                if( diff > 0 )
                {
                    diff += self.kPreferredTextFieldToKeyboardOffset
                    self.topDistance.constant = -30 - diff
                    self.view.setNeedsUpdateConstraints()
                    UIView.animate(withDuration: self.keyboardAnimationTime!, animations:  {
                        self.view.layoutIfNeeded()
                    })
                }
            }
            
        }
    }
    
    func returnViewToInitialFrame()
    {
        self.topDistance.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        });
    }
    
    // MARK:----Textfield delegates----
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
        if(self.keyboardIsShowing)
        {
            self.arrangeViewOffsetFromKeyboard()
        }
        
        if(textField == self.stateTextField)
        {
            if(self.isState == true)
            {
                self.popUpLabel.text = "Select State"

                self.searchBar.text = ""
                self.isStateTextFieldActive = true
                self.isCountryTextFieldActive = false
                
                self.CountryTableView.reloadData()
                
                self.navigationItem.leftBarButtonItem?.isEnabled = false
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                self.navigationController?.setNavigationBarHidden(true, animated: false)
                self.CountryViewBG.isHidden = false
                
                self.viewBottom.constant = 0
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return false
    }
    
    var kPreferredTextFieldToKeyboardOffset: CGFloat = 20.0
    var keyboardFrame: CGRect?
    var keyboardAnimationTime : TimeInterval?
    var keyboardIsShowing: Bool = false
    weak var activeTextField: UITextField?
    
    @IBAction func countryListBtnClick(_ sender: AnyObject) {
        
        self.popUpLabel.text = "Select Country"
        
        self.searchBar.text = ""
        isStateTextFieldActive = false
        isCountryTextFieldActive = true
        selectedCountry = ""
        selectedCountryCode = ""
        
        self.CountryTableView.reloadData()
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.CountryViewBG.isHidden = false
        
        self.viewBottom.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func countryListWebService()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Please wait...", animated: true)
            let paramArray: [Any] = ["country.list"]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as Any)
                
                self.countryArray = responseObject as! NSArray
                
                for i in 0...self.countryArray.count-1
                {
                    self.countryNameArray.append((self.countryArray.object(at: i) as! NSDictionary).object(forKey: "name") as! String)
                }
                
                SwiftLoader.hide( self.CountryTableView.reloadData())
                
                
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                    SwiftLoader.hide()
            })
            
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
            
        }
    }

    func stateListWebService(countryCode : NSString!)
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Please wait...", animated: true)
    
            let paramArray: [Any] = ["region.list",countryCode]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! NSArray!)
                
                self.stateArray = responseObject as! NSArray
                self.stateNameArray = []
                
                if (self.stateArray.count > 0)
                {
                    for i in 0...self.stateArray.count - 1
                    {
                        if((((self.stateArray.object(at: i) as! NSDictionary).object(forKey: "name") as? String)?.characters.count)!>0)
                        {
                                self.stateNameArray.append((self.stateArray.object(at: i) as! NSDictionary).object(forKey: "name") as! String)
                                self.stateTextField.text = "Select State"
                                self.isState = true
                        
                        }
                            
                        else
                        {
                            DispatchQueue.main.async(execute: {
                                self.stateTextField.text = ""
                                self.isState = false
                            })
                            break
                        }
                    }
                    self.CountryTableView.reloadData()
                }
                else
                {
                    self.stateTextField.text = ""
                    self.isState = false
                }
                
                SwiftLoader.hide()
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                    SwiftLoader.hide()
            })
            
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
            
        }
    }
    
    
    func searchBarConfig()
    {
        searchBar.delegate = self
        searchBar.barStyle = .default;
        searchBar.barTintColor = UIColor.black
        searchBar.tintColor = UIColor.white
        searchBar.backgroundColor = UIColor.clear
        searchBar.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0);
        searchBar.setSearchFieldBackgroundImage( UIImage( color : UIColor.black, size : CGSize( width : 1, height : 15 ) ), for: UIControlState.normal )
        
        textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.darkGray
        textFieldInsideSearchBar?.background = UIImage()
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        textFieldInsideSearchBar?.layer.cornerRadius = 5.0
        textFieldInsideSearchBar?.clipsToBounds = true
        textFieldInsideSearchBar?.tintColor = UIColor.black
        textFieldInsideSearchBar?.font  = UIFont (name: "Quicksand-Regular", size: 15);
        textFieldInsideSearchBar?.placeholder = "Search"
        
    }
    
    
    func getCountryCode(selectedCountry: String!)
    {
        for i in 0...self.countryArray.count - 1
        {
            if(selectedCountry == (self.countryArray.object(at: i) as! NSDictionary).object(forKey: "name") as! String)
            {
                self.selectedCountry = (countryArray.object(at: i) as! NSDictionary).object(forKey: "name") as! NSString!
                self.selectedCountryCode = (countryArray.object(at: i) as! NSDictionary).object(forKey: "iso2_code") as! NSString!
            }
        }
    }
    
    func getState(selectedState: String!)
    {
        for i in 0...self.stateArray.count - 1
        {
            if(selectedState == (self.stateArray.object(at: i) as! NSDictionary).object(forKey: "name") as! String)
            {
                self.selectedState = (stateArray.object(at: i) as! NSDictionary).object(forKey: "name") as! NSString!
            }
        }
    }

    func addClearButton(){
        let firstNamebutton : UIButton = firstNameTextField.value(forKey: "_clearButton") as! UIButton
        firstNamebutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let lastNamebutton : UIButton = lastNameTextField.value(forKey: "_clearButton") as! UIButton
        lastNamebutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let mobbutton : UIButton = mobileTextField.value(forKey: "_clearButton") as! UIButton
        mobbutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let addOnebutton : UIButton = addressLineOneTxtFld.value(forKey: "_clearButton") as! UIButton
        addOnebutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let addTwobutton : UIButton = addressLineTwoTxtFld.value(forKey: "_clearButton") as! UIButton
        addTwobutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let citybutton : UIButton = cityTextField.value(forKey: "_clearButton") as! UIButton
        citybutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let postcodebutton : UIButton = postcodeTextField.value(forKey: "_clearButton") as! UIButton
        postcodebutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let statebutton : UIButton = stateTextField.value(forKey: "_clearButton") as! UIButton
        statebutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let countrybutton : UIButton = countryTextField.value(forKey: "_clearButton") as! UIButton
        countrybutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        
    }

    //Search Bar Delegates
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.view.endEditing(true);
    }
  
    
}


//
//  MyProfileViewController.swift
//  ToDool
//
//  Created by Yogita on 27/09/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import CoreData

class MyProfileViewController : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate
{
    var selectedRow: Int!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var pickerViewBG: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var selectGroupBtn: UIButton!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var currentPswdTextField: UITextField!
    @IBOutlet weak var newPswdTextField: UITextField!
    @IBOutlet weak var confirmPswdTextField: UITextField!
    
    @IBOutlet weak var changePswdView: UIView!
    @IBOutlet weak var selectGroupLabel: UILabel!
    @IBOutlet weak var selectGroupView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var changePswdBtn: UIButton!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var currentPswdLabel: UILabel!
    @IBOutlet weak var newPaswdLabel: UILabel!
    @IBOutlet weak var confirmPswdLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var customerGroupLabel: UILabel!
    @IBOutlet weak var selectGroupImage: UIImageView!
    @IBOutlet var saveHeightContaints: NSLayoutConstraint!
    
    @IBOutlet weak var topDistance: NSLayoutConstraint!
    
    var isChangePswdClick : Bool!
    var isEditProfileClick : Bool!
    private var _updateProfile : Bool = false
    private var _updatePassword : Bool = false
    var groupArray : NSArray!
    var userProfileResponse : NSDictionary!
    var groupID : Int!
    
    // MARK:----viewDidLoad----
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addClearButton()
        groupArray = []
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(self.hideKeyboardOnTap(_:)) )
        self.view.addGestureRecognizer( gesture )
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        for subview in self.view.subviews
        {
            if subview is UITextField
            {
                let textField = subview as! UITextField
                textField.addTarget(self, action: #selector(self.textFieldShouldReturn(_:)), for: UIControlEvents.editingDidEndOnExit)
                
                textField.addTarget(self, action: #selector(self.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingDidBegin)
            }
        }
        self.navigationItem.setLeftBarButtonItems([UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(backButtonClicked) ),UIBarButtonItem(image: UIImage( named: "navigation_landingscreen" ), style: .plain, target: self, action: #selector(menuButtonClicked) )], animated: true)
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Edit", style:.plain, target:self, action:#selector(editButtonClicked))
        
        selectedRow = 0
        self.pickerViewBG.backgroundColor=UIColor.black .withAlphaComponent(0.5)
        
        //----------TextFieldBorder-----------//
        Utility.textFieldBorderColor(self.firstNameTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.lastNameTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.emailTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.currentPswdTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.newPswdTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.confirmPswdTextField,color: textfieldLineBlackColor!)
        Utility.textFieldBorderColor(self.selectGroupView,color: textfieldLineBlackColor!)
        
        
        //-----------Add Padding-----------//
        Utility.addPaddingToTextField(textfield: self.firstNameTextField)
        Utility.addPaddingToTextField(textfield: self.lastNameTextField)
        Utility.addPaddingToTextField(textfield: self.emailTextField)
        Utility.addPaddingToTextField(textfield: self.currentPswdTextField)
        Utility.addPaddingToTextField(textfield: self.newPswdTextField)
        Utility.addPaddingToTextField(textfield: self.confirmPswdTextField)
        
        //------------HidePasswordView------------//
        self.hidePasswordView(ishide: true)
        
        self.loadGroupsWebService()
        self.updateProfileFields()
        
        //--------reset Tags------//
        self.currentPswdTextField.tag = 0
        self.newPswdTextField.tag = 0
        self.confirmPswdTextField.tag = 0
    }
    
    // MARK:----viewWillAppear-----
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        isChangePswdClick = false
        isEditProfileClick = false
        self.changePswdView.isHidden = true
    }
    
    var pickerDataSource = [ Int : String ]( );
    
    
    
    func validateMyProfileTextFields() ->Bool
    {
        var success = true
        
        if ( Utility.isEmpty(self.firstNameTextField.text!) )
        {
            self.view.makeToast(message: k_firstname_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(((self.firstNameTextField.text?.characters.count)! >= 2) && ((self.firstNameTextField.text?.characters.count)! <= 35)))
        {
            self.view.makeToast(message: k_firstname_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.lastNameTextField.text!) )
        {
            self.view.makeToast(message: k_lastname_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(((self.lastNameTextField.text?.characters.count)! >= 2) && ((self.lastNameTextField.text?.characters.count)! <= 35)))
        {
            self.view.makeToast(message: k_lastname_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.emailTextField.text!) )
        {
            self.view.makeToast(message: k_email_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(Utility.validateEmail( self.emailTextField.text!)))
        {
            self.view.makeToast(message: k_email_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if (isChangePswdClick == true && isEditProfileClick == true) {
            success = self.validatePasswordTexfields()
        }
        
        return success;
    }
    
    func validatePasswordTexfields()-> Bool
    {
        let success = true
        
        if ( Utility.isEmpty(self.currentPswdTextField.text!) )
        {
            self.view.makeToast(message: k_pass_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.newPswdTextField.text!) )
        {
            self.view.makeToast(message: k_new_pass_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (self.currentPswdTextField.text == self.newPswdTextField.text)
        {
            self.view.makeToast(message: k_invalid_New_Pass, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
            
        else if (!(Utility.validatePassword( self.newPswdTextField.text! )))
        {
            self.view.makeToast(message: k_pass_length, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.confirmPswdTextField.text!) )
        {
            self.view.makeToast(message: k_pass_2_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(self.confirmPswdTextField.text == self.newPswdTextField.text))
        {
            self.view.makeToast(message: k_pass_Match, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        return success;
        
    }
    
    
    
    // MARK:----PickerViewDelegates----
    
    func pickerView(_: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return (groupArray[row] as! NSDictionary).value(forKey: "label") as! String?
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return groupArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if( component == 0 )
        {
            return groupArray.count;
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self._updateProfile = true
        selectedRow=row
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return self.view.frame.size.width
    }
    
    // MARK:----Navigation Button IBAction----
    
    func menuButtonClicked( )
    {
        self.view.endEditing(true)
        self.frostedViewController.presentMenuViewController( )
    }
    
    func backButtonClicked( )
    {
        _ = ( self.frostedViewController.contentViewController as? UINavigationController )?.popToRootViewController(animated: true)
    }
    
    func editButtonClicked( )
    {
        UIView.transition(with: self.currentPswdLabel , duration: 0.5, options: .transitionCrossDissolve, animations: {() -> Void in
            self.changePswdView.isHidden = false
            }, completion: { _ in })
        self.enableTextFields(isEnable: true)
        isEditProfileClick = true
        self.navigationItems()
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        self.keyboardIsShowing = true
        
        if let info = notification.userInfo {
            self.keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.keyboardAnimationTime = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            self.arrangeViewOffsetFromKeyboard()
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardIsShowing = false
        self.returnViewToInitialFrame()
    }
    
    func arrangeViewOffsetFromKeyboard()
    {
        if let tf = self.activeTextField
        {
            var frame = self.scrollView.frame
            if let rect : CGRect = self.keyboardFrame
            {
                frame.size.height -= rect.size.height
            }
            
            let union = frame.union( tf.frame )
            var diff = union.size.height - frame.size.height
            print(diff)
            if( diff > 0 )
            {
                diff += self.kPreferredTextFieldToKeyboardOffset
                self.topDistance.constant = diff + 20
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: self.keyboardAnimationTime!, animations:  {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func returnViewToInitialFrame()
    {
        self.topDistance.constant = -37
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        });
    }
    
    func hideKeyboardOnTap( _ gesture : UITapGestureRecognizer )
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
    }
    
    
    // MARK:----Textfield delegates----
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
        if textField === self.emailTextField || textField === self.firstNameTextField || textField === self.lastNameTextField
        {
            self._updateProfile = true
        }
        if textField === self.currentPswdTextField || textField === self.newPswdTextField || textField === self.confirmPswdTextField
        {
            self._updatePassword = true
        }
        if(self.keyboardIsShowing)
        {
            self.arrangeViewOffsetFromKeyboard()
        }
    }
    
    
    
    var kPreferredTextFieldToKeyboardOffset: CGFloat = 20.0
    var keyboardFrame: CGRect?
    var keyboardAnimationTime : TimeInterval?
    var keyboardIsShowing: Bool = false
    weak var activeTextField: UITextField?
    
    
    // MARK:---- Pickerview Done/Cancel BtnClick----
    
    @IBAction func doneBtnClick(_ sender: UIButton) {
        
        self.selectGroupLabel.text = (groupArray[selectedRow] as! NSDictionary).value(forKey: "label") as! String?
        self.pickerViewBottom.constant = -self.pickerView.frame.size.height
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (result) in
            self.open()
            self.pickerViewBG.isHidden = true
        }
    }
    
    @IBAction func cancelBtnClick(_ sender: UIButton) {
        self.pickerViewBottom.constant = -self.pickerView.frame.size.height
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (result) in
            self.open()
            self.pickerViewBG.isHidden = true
        }
    }
    
    // MARK:----selectGroupBtnClick----
    
    @IBAction func selectGroupBtnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        self.close()
        
        self.pickerViewBG.isHidden=false
        self.pickerView.reloadAllComponents()
        self.pickerView.selectRow(selectedRow, inComponent: 0, animated: true)
        self.pickerViewBottom.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    // MARK:--------cancelButtonClick-------
    
    func cancelButtonClicked()
    {
        isEditProfileClick = false
        self.enableTextFields(isEnable: false)
        self.hidePasswordDetails(bool: true)
        self.checkImage.image = UIImage( named: "uncheck_myprofile")
        self.navigationItems()
        
    }
    
    
    // MARK:----saveBtnClick----
    
    @IBAction func saveBtnClick(_ sender: UIButton) {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
        
        if (isChangePswdClick == false && isEditProfileClick == true) {
            self.setUserProfileWebService()
        }
        
        if (self.isChangePswdClick == true && self.isEditProfileClick == true)
        {
            self.setUserProfileWebService()
            self.changePasswordWebService()
        }
        
    }
    
    
    // MARK:----changePswdBtnClick----
    
    @IBAction func changePswdBtnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        switch sender.tag {
        case 100:
            isChangePswdClick = true
            self.checkImage.image = UIImage( named: "checkbox_myprofile")
            self.hidePasswordView(ishide: false)
            sender.tag = 200
            
            //--------reset Tags------//
            self.currentPswdTextField.tag = 103
            self.newPswdTextField.tag = 104
            self.confirmPswdTextField.tag = 105
            
            break
            
        case 200:
            isChangePswdClick = false
            self.checkImage.image = UIImage( named: "uncheck_myprofile")
            self.hidePasswordView(ishide: true)
            sender.tag = 100
            
            //--------reset Tags------//
            self.currentPswdTextField.tag = 0
            self.newPswdTextField.tag = 0
            self.confirmPswdTextField.tag = 0
            
            break
            
        default:
            break
        }
    }
    
    // MARK:----hidePasswordView----
    
    func hidePasswordView(ishide:Bool) -> Void {
        
        if(ishide == true)
        {
            self.emailTextField.returnKeyType = .done
            //--------reset Tags------//
            self.currentPswdTextField.tag = 0
            self.newPswdTextField.tag = 0
            self.confirmPswdTextField.tag = 0
        }
        else
        {
            self.emailTextField.returnKeyType = .next
            //--------reset Tags------//
            self.currentPswdTextField.tag = 103
            self.newPswdTextField.tag = 104
            self.confirmPswdTextField.tag = 105
        }
        
        self.clearPswdTextFields(isClear: true)
        
        UIView.transition(with: self.currentPswdLabel , duration: 0.5, options: .transitionCrossDissolve, animations: {() -> Void in
            self.currentPswdLabel.isHidden = ishide
            }, completion: { _ in })
        
        UIView.transition(with: self.currentPswdTextField , duration: 0.5, options: .transitionCrossDissolve, animations: {() -> Void in
            self.currentPswdTextField.isHidden = ishide
            }, completion: { _ in })
        
        UIView.transition(with: self.newPaswdLabel , duration: 1.0, options: .transitionCrossDissolve, animations: {() -> Void in
            self.newPaswdLabel.isHidden = ishide
            }, completion: { _ in })
        
        UIView.transition(with: self.newPswdTextField , duration: 1.0, options: .transitionCrossDissolve, animations: {() -> Void in
            self.newPswdTextField.isHidden = ishide
            }, completion: { _ in })
        
        UIView.transition(with: self.confirmPswdLabel , duration: 1.5, options: .transitionCrossDissolve, animations: {() -> Void in
            self.confirmPswdLabel.isHidden = ishide
            }, completion: { _ in })
        
        UIView.transition(with: self.confirmPswdTextField , duration: 1.5, options: .transitionCrossDissolve, animations: {() -> Void in
            self.confirmPswdTextField.isHidden = ishide
            }, completion: { _ in })
        
        //self.saveBtn.isHidden = ishide
        
        self.saveHeightContaints.isActive =  !ishide
        
        
    }
    
    
    // MARK:----Animate image----
    
    func close( )
    {
        UIView.animate(withDuration: 0.3) {
            self.selectGroupImage.transform = CGAffineTransform.init(rotationAngle: CGFloat( -M_PI ) )}
    }
    
    func open( )
    {
        UIView.animate(withDuration: 0.3) {
            self.selectGroupImage.transform = CGAffineTransform.identity}
    }
    
    
    // MARK:----clear/enableTextFields----
    
    func clearPswdTextFields(isClear: Bool) -> Void {
        self.currentPswdTextField.text=""
        self.newPswdTextField.text=""
        self.confirmPswdTextField.text=""
    }
    
    
    func enableTextFields(isEnable:Bool) -> Void {
        self.firstNameTextField.isUserInteractionEnabled = isEnable
        self.lastNameTextField.isUserInteractionEnabled = isEnable
        self.emailTextField.isUserInteractionEnabled = isEnable
        self.selectGroupBtn.isUserInteractionEnabled = isEnable
        // self.saveBtn.isHidden = !isEnable
    }
    
    func hidePasswordDetails(bool:Bool) {
        self.changePswdView.isHidden = bool
        self.hidePasswordView(ishide: bool)
    }
    
    
    // MARK:----------Navigation items-------
    
    func navigationItems()
    {
        if (isEditProfileClick == true){
            self.navigationItem.leftBarButtonItems = nil
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title:"Cancel", style:.plain, target:self, action:#selector(cancelButtonClicked))
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Save", style:.plain, target:self, action:#selector(saveBtnClick(_:)))
        }
            
        else{
            self.navigationItem.setLeftBarButtonItems([UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(backButtonClicked) ),UIBarButtonItem(image: UIImage( named: "navigation_landingscreen" ), style: .plain, target: self, action: #selector(menuButtonClicked) )], animated: true)
            self.navigationController!.interactivePopGestureRecognizer?.delegate = self
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Edit", style:.plain, target:self, action:#selector(editButtonClicked))
        }
        
    }
    
    
    // MARK:----clear/enableTextFields----
    
    func setUserProfileWebService()
    {
        if (Reachability.isConnectedToNetwork() == true)
        {
            if (self.activeTextField != nil)
            {
                self.activeTextField?.resignFirstResponder()
                self.activeTextField = nil
            }
            if(self.validateMyProfileTextFields())
            {
                SwiftLoader.show(title: "Please wait...", animated: true)
                
                let selectedGroup = self.selectGroupLabel.text! as String
                var selectedGroupID : Int!
                for i in 0...self.groupArray.count-1
                {
                    if((self.groupArray[i] as! NSDictionary).value(forKey: "label") as! String == selectedGroup)
                    {
                        selectedGroupID = Int((self.groupArray[i] as! NSDictionary).value(forKey: "value") as! String)
                    }
                }
                
                let dict = ["email": self.emailTextField.text! as String, "firstname": self.firstNameTextField.text! as String, "lastname": self.lastNameTextField.text! as String, "group_id": selectedGroupID] as [String : Any]
                let paramArray: [Any] = ["customer.update",UserDefaults.standard.object(forKey: k_userID) as! String,dict]
                
                Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                    print(responseObject as Any)
                    
                    let userProfileDict = NSMutableDictionary()
                    
                    userProfileDict.setValue(self.firstNameTextField.text! as String, forKey: "firstname")
                    userProfileDict.setValue(self.lastNameTextField.text! as String, forKey: "lastname")
                    userProfileDict.setValue(self.emailTextField.text! as String, forKey: "email")
                    
                    UserDefaults.standard.set(userProfileDict, forKey: k_userProfileDict)
                    UserDefaults.standard.set(selectedGroup, forKey: k_userGroup)
                    
                    
                    if (!(self.isChangePswdClick == true && self.isEditProfileClick == true))
                    {
                        DispatchQueue.main.async(execute: {
                            self.isEditProfileClick = false
                            self.navigationItems()
                            self.enableTextFields(isEnable: false)
                            self.hidePasswordDetails(bool: true)
                            self.checkImage.image = UIImage( named: "uncheck_myprofile")
                        })
                    }
                    SwiftLoader.hide()
                    
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        self.isEditProfileClick = true
                        self.navigationItems()
                        
                        SwiftLoader.hide()
                        self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                        
                })
            }
        }
        else
        {
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    func addClearButton(){
        let firstNamebutton : UIButton = firstNameTextField.value(forKey: "_clearButton") as! UIButton
        firstNamebutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let lastNamebutton : UIButton = lastNameTextField.value(forKey: "_clearButton") as! UIButton
        lastNamebutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let emailbutton : UIButton = emailTextField.value(forKey: "_clearButton") as! UIButton
        emailbutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let currenPassbutton : UIButton = currentPswdTextField.value(forKey: "_clearButton") as! UIButton
        currenPassbutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let newPassbutton : UIButton = newPswdTextField.value(forKey: "_clearButton") as! UIButton
        newPassbutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        let confirmPassbutton : UIButton = confirmPswdTextField.value(forKey: "_clearButton") as! UIButton
        confirmPassbutton.setImage(UIImage( named: "cross-grey" ), for: UIControlState.normal)
        
    }
    
    func loadGroupsWebService()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Please wait...", animated: true)
            let paramArray: [Any] = ["custom_api.customergroup"]
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                let response = responseObject as! NSArray
                self.groupArray = response
                print(self.groupArray)
                self.getUserProfileWebService()
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    SwiftLoader.hide()
                    print((error?.localizedDescription)!)
                    self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    func getUserProfileWebService()
    {
        if (Reachability.isConnectedToNetwork() == true){
            let paramArray: [Any] = ["customer.info",UserDefaults.standard.object(forKey: k_userID) as! String]
            SwiftLoader.show(title: "Please wait...", animated: true)
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                self.userProfileResponse = responseObject as! NSDictionary
                print(self.userProfileResponse)
                UserDefaults.standard.set(responseObject as! NSDictionary, forKey: k_userProfileDict)
                self.userProfileResponse = UserDefaults.standard.object(forKey: k_userProfileDict) as! NSDictionary!
                
                self.groupID = Int((self.userProfileResponse.value(forKey: "group_id") as? String!)!)
                
                for i in 0...self.groupArray.count-1
                {
                    if(Int((self.groupArray[i] as! NSDictionary).value(forKey: "value") as! String) == self.groupID)
                    {
                        self.selectedRow = i
                        UserDefaults.standard.set((self.groupArray[i] as! NSDictionary).value(forKey: "label"), forKey: k_userGroup)
                        DispatchQueue.main.async {
                        self.selectGroupLabel.text =  UserDefaults.standard.object(forKey: k_userGroup) as? String
                        }
                        break
                    }
                }
                self.updateProfileFields()
                SwiftLoader.hide()
                
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    SwiftLoader.hide()
                    print((error?.localizedDescription)!)
                    self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    
    func updateProfileFields()
    {
        DispatchQueue.main.async {
        if((UserDefaults.standard.object(forKey: k_userProfileDict) as! NSDictionary!) != nil)
        {
                self.userProfileResponse = UserDefaults.standard.object(forKey: k_userProfileDict) as! NSDictionary!
                self.firstNameTextField.text = self.userProfileResponse.value(forKey: "firstname") as? String
                self.lastNameTextField.text = self.userProfileResponse.value(forKey: "lastname") as? String
                self.emailTextField.text = self.userProfileResponse.value(forKey: "email") as? String
                self.selectGroupLabel.text =  UserDefaults.standard.object(forKey: k_userGroup) as? String
        }
        }
    }
    
    func changePasswordWebService()
    {
        if (Reachability.isConnectedToNetwork() == true)
        {
            if (self.activeTextField != nil)
            {
                self.activeTextField?.resignFirstResponder()
                self.activeTextField = nil
            }
            if(self.validatePasswordTexfields())
            {
                SwiftLoader.show(title: "Please wait...", animated: true)
                let dict = ["customeremail": self.emailTextField.text! as String, "password": self.currentPswdTextField.text! as String, "newpassword": self.newPswdTextField.text! as String]
                let paramArray: [Any] = ["custom_api.resetpassword",dict]
                
                Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                    print(responseObject as Any)
                    
                    DispatchQueue.main.async(execute: {
                        self.isEditProfileClick = false
                        self.navigationItems()
                        self.enableTextFields(isEnable: false)
                        self.hidePasswordDetails(bool: true)
                        self.checkImage.image = UIImage( named: "uncheck_myprofile")
                    })
                    
                    SwiftLoader.hide()
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        self.isEditProfileClick = true
                        self.navigationItems()
                        self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                })
            }
        }
        else
        {
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
        
        
    }
    
    
}

//
//  AddressBookViewController.swift
//  ToDool
//
//  Created by Yogita on 05/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

class AddressBookViewController : UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet var bottomDistance: NSLayoutConstraint!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var addAddressView: UIView!
    @IBOutlet weak var addressTableView: UITableView!
    var listShipping = NSMutableArray()
    var listBilling = NSMutableArray()

    var isBilling : Bool!
    var isShipping : Bool!
    
    var reponseArray = NSArray()

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.barStyle = UIBarStyle.default
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = navigationColor
        self.navigationController!.navigationBar.isTranslucent = false

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "navigation_landingscreen" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Edit", style:.plain, target:self, action:#selector(editButtonClicked))
        self.title = "Address Book"
        
        let attributes = [NSFontAttributeName: UIFont(name:"Quicksand-Regular", size: 14)!]
        segmentController.setTitleTextAttributes(attributes,
                                                 for: .normal)
        
        
        //TableView Configuration
        addressTableView.register(UINib.init(nibName: "AddressBookTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressBookTableViewCell")
        addressTableView.autoresizingMask = UIViewAutoresizing(rawValue:UIViewAutoresizing.flexibleWidth.rawValue | UIViewAutoresizing.flexibleHeight.rawValue)
        addressTableView.delegate = self;
        addressTableView.dataSource = self;
        addressTableView.backgroundColor = UIColor.clear
        self.addressTableView.rowHeight = UITableViewAutomaticDimension
        self.addressTableView.estimatedRowHeight = 100.0
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        self.listShipping = []
        self.listBilling = []
        self.isShipping = false
        self.isBilling = false
        
        getAddressListWebService()
    }
    
    func getAddressListWebService()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Please wait...", animated: true)
            
            let paramArray: [Any] = ["customer_address.list",UserDefaults.standard.object(forKey: k_userID)!]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as Any)
                
                if((responseObject) != nil){
                    
                    self.reponseArray = responseObject as! NSArray
                    if(self.reponseArray.count > 0)
                    {
                    for i in 0...self.reponseArray.count-1{
                        if(((self.reponseArray.object(at: i) as! NSDictionary).value(forKey: "is_default_billing") as! Bool) == true ){
                            self.isBilling = true
                            self.listBilling.add(self.reponseArray.object(at: i))

                        }
                        if (((self.reponseArray.object(at: i) as! NSDictionary).value(forKey: "is_default_shipping")as! Bool) == true ){
                            self.isShipping = true
                            self.listShipping.add(self.reponseArray.object(at: i))
                        }
                    }
                    }
                    print(self.listBilling)
                    self.addressTableView.reloadData()
                }
                
                SwiftLoader.hide( )
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                SwiftLoader.hide()
            })
            
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
            
        }
    }

    // Table view delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if segmentController.selectedSegmentIndex == 0 {
            return self.listBilling.count
        }
        else{
            return self.listShipping.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "AddressBookTableViewCell"
        let cell: AddressBookTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AddressBookTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        configure( cell: cell, indexPath:  indexPath as NSIndexPath )
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    //Cell Configuration
    
    func configure( cell : AddressBookTableViewCell, indexPath : NSIndexPath )
    {
        if segmentController.selectedSegmentIndex == 0 {

            cell.nameLabel.text = ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "firstname") as? String)! + " " + ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "lastname") as? String)!
            cell.addressLabel.text = self.address(AddressOne: (((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "street") as? String)?.replacingOccurrences(of: "\n", with: ", "))!, AddressTwo: nil, City: ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "city") as? String)!, State: ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "region") as? String)!, Pincode: ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "postcode") as? String)!, Country: ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "country_id") as? String)!, Mobile: ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "telephone") as? String)!)
        }
        else{
            cell.nameLabel.text = ((listShipping[ indexPath.row ] as! NSDictionary).value(forKey: "firstname") as? String)! + " " + ((listShipping[ indexPath.row ] as! NSDictionary).value(forKey: "lastname") as? String)!
            cell.addressLabel.text = self.address(AddressOne: (((listShipping[ indexPath.row ] as! NSDictionary).value(forKey: "street") as? String)?.replacingOccurrences(of: "\n", with: ", "))!, AddressTwo: nil, City: ((listShipping[ indexPath.row ] as! NSDictionary).value(forKey: "city") as? String)!, State: ((listBilling[ indexPath.row ] as! NSDictionary).value(forKey: "region") as? String)!, Pincode: ((listShipping[ indexPath.row ] as! NSDictionary).value(forKey: "postcode") as? String)!, Country: ((listShipping[ indexPath.row ] as! NSDictionary).value(forKey: "country_id") as? String)!, Mobile: ((listShipping[ indexPath.row ] as! NSDictionary).value(forKey: "telephone") as? String)!)
        }
    }
    
    
    func leftNavigationBarButtonClicked( )
    {
        self.frostedViewController.presentMenuViewController( )
        self.view.endEditing(true)
    }
    
    func editButtonClicked()
    {
        self.view.endEditing(true)
        self.bottomDistance.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func segmentControlClick(_ sender: UISegmentedControl) {
        
        if(segmentController.selectedSegmentIndex==0)
        {
            let transition = CATransition()
            transition.type = kCATransitionPush
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeForwards
            transition.duration = 0.5
            transition.subtype = kCATransitionFromLeft
            self.addressTableView.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            // Update your data source here
            self.addressTableView.reloadData()
            
        }
            
        else if(segmentController.selectedSegmentIndex==1){
            let transition = CATransition()
            transition.type = kCATransitionPush
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeForwards
            transition.duration = 0.5
            transition.subtype = kCATransitionFromRight
            self.addressTableView.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            // Update your data source here
            self.addressTableView.reloadData()
        }
    }
    
    
    @IBAction func addAddressBtnClick(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        addressTableView.reloadData()
        
        if(self.isBilling == true && self.isShipping == true)
        {
           self.view.makeToast(message: k_billing_shipping_address_added, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
        else
        {
        let vc = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
            vc.isShippingAdded = isShipping
            vc.isBillingAdded = isBilling
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
            self.view.endEditing(false)
            self.bottomDistance.constant = -60
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }

        }
    }
    
    func address(AddressOne add1:String?, AddressTwo add2: String?,  City city: String?, State state: String?, Pincode pincode: String?, Country country: String?, Mobile mobile: String? ) ->String
    {
        
        var addressString : String
        addressString = ""
        
        if (self.checkNilandEmptyString(string: add1)) {
            addressString = addressString + add1! + ", "
        }
        if (self.checkNilandEmptyString(string: add2)) {
            addressString = addressString + add1! + "\n"
        }
        
        if (self.checkNilandEmptyString(string: city)) {
            addressString = addressString + city! + " - "
        }
        
        if (self.checkNilandEmptyString(string:pincode)) {
            addressString = addressString + pincode! + "\n"
        }
        
        if (self.checkNilandEmptyString(string: state)) {
            addressString = addressString + state! + ", "
        }
        
        if (self.checkNilandEmptyString(string:country)) {
            addressString = addressString + country! + "\n"
        }
        
        if (self.checkNilandEmptyString(string:mobile)) {
            addressString = addressString + "Mobile: " + mobile! + "\n"
        }
        
        return addressString
        
    }
    
    func checkNilandEmptyString (string:String?)->Bool{
        if ((string == nil) || (string?.isEmpty)! || (string?.characters.count) == 0){
            return false;
        }
        return true;
        
        
    }
}



//
//  LeftViewController.swift
//  ToDool
//
//  Created by Pranav on 9/12/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = toValue
        rotateAnimation.duration = duration
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.fillMode = kCAFillModeForwards
        
        if let delegate: CAAnimationDelegate = completionDelegate as? CAAnimationDelegate {
            rotateAnimation.delegate = delegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}



class LeftViewController : UIViewController,RATreeViewDataSource,RATreeViewDelegate
{
    @IBOutlet weak var treeView : RATreeView!
    //var data : DataObject!
    // var homeData : DataObject!
    
    var childrens = [DataObject]()
    var categoryDetailsDict : NSDictionary!
    var categoryArray = NSMutableArray()
    var root: DataObject!
    var root_id : Int!
    
    var response : NSDictionary!
    @IBOutlet weak var header : UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userImageView: UIView!
    var dataObject = [DataObject]()
    
    
    var cellDescriptors: NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableData(notification:)), name: .reload, object: nil)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if((appDelegate.appDelegateHomeData == nil) || (appDelegate.appDelegateMenuData == nil))
        {
            categoryArray = []
            self.categoryWebService()
            self.commonInit( category: DataObject( "Loading Categories...", -1 ))
            treeView.reloadData()
        }
        view.backgroundColor = .white
        setupTreeView()
    }
    
    
    
    func reloadTableData(notification: NSNotification) {
        categoryArray = []
        self.categoryWebService()
        self.commonInit( category: DataObject( "Loading Categories...", -1 ))
        treeView.reloadData()
    }
    
    
    func setupTreeView() -> Void {
        treeView.register(UINib.init(nibName: "CollapsableTableViewCell", bundle: nil), forCellReuseIdentifier: "CollapsableTableViewCell")
        treeView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        treeView.delegate = self;
        treeView.dataSource = self;
        treeView.treeFooterView = UIView()
        treeView.backgroundColor = .clear
        view.addSubview(treeView)
        self.treeView.reloadData()
        
    }
    
    //MARK: RATreeView data source
    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let item = item as? DataObject {
            return item.children.count
        } else {
            return appDelegate.appDelegateMenuData.children.count
        }
    }
    
    
    func treeView(_ treeView: RATreeView, willExpandRowForItem item: Any)
    {
        if let item = item as? DataObject
        {
            item.expanded = true
            ( treeView.cell(forItem: item) as? CollapsableTableViewCell )?.open()
            //treeView.reloadRows( forItems: [ item ], with: RATreeViewRowAnimationNone )
        }
    }
    
    
    func treeView(_ treeView: RATreeView, willCollapseRowForItem item: Any)
    {
        if let item = item as? DataObject
        {
            item.expanded = false
            ( treeView.cell(forItem: item) as? CollapsableTableViewCell )?.close()
            //treeView.reloadRows( forItems: [ item ], with: RATreeViewRowAnimationNone )
        }
    }
    
    
    
    func  treeView(_ treeView: RATreeView, didSelectRowForItem item: Any) {
        
        if let dobject : DataObject = item as? DataObject
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var vc : UIViewController?
            
            if(dobject.identifier == -1){
            switch( dobject.name )
            {
            case  "SignIn":
                vc = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
                
            case  "My Account":
                if(UserDefaults.standard.object(forKey: k_userID) != nil)
                {
                    vc = storyboard.instantiateViewController(withIdentifier:"MyProfileViewController") as! MyProfileViewController
                }
                else
                {
                    self.view.makeToast(message: k_please_login, duration: 3.0, position: HRToastPositionTop as AnyObject )
                }
                
            case "Address Book":
                if(UserDefaults.standard.object(forKey: k_userID) != nil)
                {
                    vc = storyboard.instantiateViewController(withIdentifier:"AddressBookViewController") as! AddressBookViewController
                }
                else
                {
                    self.view.makeToast(message: k_please_login, duration: 3.0, position: HRToastPositionTop as AnyObject )
                }
                
            case "Contact Us":
                vc = storyboard.instantiateViewController(withIdentifier:"ContactUsViewController") as! ContactUsViewController
                
            case "My Rewards":
                if(UserDefaults.standard.object(forKey: k_userID) != nil)
                {
                   vc = storyboard.instantiateViewController(withIdentifier:"MyRewardsViewController") as! MyRewardsViewController
                }
                else
                {
                    self.view.makeToast(message: k_please_login, duration: 3.0, position: HRToastPositionTop as AnyObject )
                }
               
                
    
            case "Terms & Conditions":
                vc = storyboard.instantiateViewController(withIdentifier:"TermsAndConditionViewController") as! TermsAndConditionViewController
                
            case "Privacy Policy":
                
                vc = storyboard.instantiateViewController(withIdentifier:"PrivacyPolicyViewController") as! PrivacyPolicyViewController
                
            case  "About Us":
                vc = storyboard.instantiateViewController(withIdentifier:"AboutUsViewController") as! AboutUsViewController
                
            case  "Logout":
                
                let alertController = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction!) in
                    
                }
                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction!) in
                    
                    self.dismiss(animated: true, completion: nil)
                    self.frostedViewController?.hideMenuViewController()
                    vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    UserDefaults.standard.removeObject(forKey: k_userID)
                    UserDefaults.standard.removeObject(forKey: k_userGroup)
                    UserDefaults.standard.removeObject(forKey: k_userProfileDict)
                    
                    NotificationCenter.default.post(name: .reload, object: nil)
                    
                }
                alertController.addAction(OKAction)
                
                self.present(alertController, animated: true, completion:nil)
                
            default:
                break;
            }
                
                
                if let vc = vc
                {
                    self.frostedViewController.hideMenuViewController( )
                    ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
                }

            }
            else if((dobject.identifier != -1) && (dobject.isExpandable() == false))
            {
                let vc = storyboard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
                vc.categoryId = dobject.identifier
                vc.title = dobject.name
                self.frostedViewController.hideMenuViewController( )
                ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
   
            }
            
            
        }
        
        
    }
    
    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let item = item as? DataObject {
            return item.children[index]
        } else {
            return appDelegate.appDelegateMenuData.children[index] as AnyObject
        }
    }
    
    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {
        let cell = treeView.dequeueReusableCell(withIdentifier: String(describing: CollapsableTableViewCell.self)) as! CollapsableTableViewCell
        treeView.separatorColor = UIColor.clear
        let item = item as! DataObject
        if(item.isExpandable())
        {
            cell.button.isHidden = false
        }
        else
        {
            cell.button.isHidden = true
            
        }
        let level = treeView.levelForCell(forItem: item)
        let detailsText = "Number of children \(item.children.count)"
        cell.selectionStyle = .none
        cell.setup(withTitle: item.name, detailsText: detailsText, level: level, additionalButtonHidden: false)
        return cell
    }
    
    func commonInit( category : DataObject = DataObject( "Loading Categories ...", -1 )){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //let object6 = DataObject("Loading categories..", -1)
        let object7 = DataObject("My Account", -1)
        let object8 = DataObject("Address Book", -1)
        let object9 = DataObject("My Rewards", -1)
        
        let object10 = DataObject("Account Settings", -1, [object7, object8, object9])
        
        let object11 = DataObject("About Us", -1)
        let object12 = DataObject("Terms & Conditions", -1)
        let object13 = DataObject("Privacy Policy", -1)
        let object14 = DataObject("Contact Us", -1)
        
        
        let object15 = DataObject("About Todool", -1, [object11, object12, object13, object14])
        
        var object16 : DataObject!
        if(UserDefaults.standard.object(forKey: k_userID) != nil)
        {
            object16 = DataObject("Logout", -1)
            
        }
        else
        {
            object16 = DataObject("SignIn", -1)
            
        }
        
        var object1 = [ DataObject ]()
        if category.children.count>0{
           // for i in 0...category.children.count-1{
            for i in 0...2{
                if i < category.children.count {
                    object1.append(category.children[i])
                    
                    //if (i == category.children.count-1) {
                    if (i == 2) {
                        appDelegate.appDelegateHomeData = DataObject("Root",-1,object1)
                        
                        object1.append(object10)
                        object1.append(object15)
                        object1.append(object16)
                        
                        appDelegate.appDelegateMenuData = DataObject( "Root", -1, object1 )
                        NotificationCenter.default.post(name: .reloadPrimaryCategories, object: nil)
                        
                        if(self.treeView != nil)
                        {
                            self.treeView.reloadData( )
                            
                        }
                    }
                }
                else{
                    break
                }
            }
        }
        else{
            appDelegate.appDelegateHomeData = DataObject("Root",-1,[category])
            
            appDelegate.appDelegateMenuData = DataObject( "Root", -1, [ category, object10, object15, object16!] )
            NotificationCenter.default.post(name: .reloadPrimaryCategories, object: nil)
            
            if(self.treeView != nil)
            {
                self.treeView.reloadData( )
                
            }        }
    }
    
    func getDataObjectsById( _ index : Int, list : [ DataObject ] ) -> DataObject?
    {
        for object in list
        {
            if object.identifier == Int64( index )
            {
                return object
            }
        }
        return nil
    }
    
    
    func showCategories(){
        do {
            let categories = categoryArray
            var list = [ DataObject ]( )
            var result : DataObject?
            for cate in categories
            {
                let object = DataObject((cate as! NSDictionary).value(forKey: "name") as! String, Int64((cate as! NSDictionary).value(forKey: "category_id") as! Int))
                
                if result == nil
                {
                    result = object
                }
                list.append( object )
                if let parentId = (cate as! NSDictionary).value(forKey: "parent_id")
                {
                    if let parent = self.getDataObjectsById( parentId as! Int, list: list )
                    {
                        if ((cate as! NSDictionary).value(forKey: "is_active") as! String == "1") {
                            parent.children.append( object )
                            object._parent = parent
                        }
                    }
                }
            }
            if let result = result
            {
                self.commonInit( category: DataObject( "Categories", -1, result.children[ 0 ].children ))
            }
            
        }
    }
    
    
    
    func getcategories(response : NSDictionary?)
    {
        if(response != nil)
        {
            var name : String
            var level : Int
            var children = [AnyObject]()
            var parent_id : Int
            var category_id : Int
            var is_active : String
            var position : Int
            var childCount : Int
            
            var object : NSDictionary!
            
            level = Int(response?.object(forKey: "level") as! String)!
            name = response?.object(forKey: "name") as! String
            parent_id = Int(response?.object(forKey: "parent_id") as! String)!
            category_id = Int(response?.object(forKey: "category_id") as! String)!
            is_active = response?.object(forKey: "is_active") as! String
            position = Int(response?.object(forKey: "position") as! String)!
            children = response?.object(forKey: "children") as! [AnyObject]
            
            childCount = children.count
            
            object =  ["name":name, "parent_id" : parent_id,"category_id" : category_id, "level" : level, "position" : position, "childCount" : childCount, "is_active":is_active ]
            categoryArray.add(object)
            if (response?.object(forKey: "children") != nil)
            {
                children = response?.object(forKey: "children") as! [AnyObject]
                
                if(children.count > 0)
                {
                    for i in 0...children.count-1
                    {
                        let dict = children[i] as! NSDictionary
                        self.getcategories(response: dict)
                    }
                }
            }
            
            self.showCategories()
        }
    }
    
    func categoryWebService()
    {
        if (Reachability.isConnectedToNetwork() == true)
        {
            SwiftLoader.show(title: "Loading Categories...", animated: true)
            var response : NSDictionary!
            
            let paramArray: [Any] = ["catalog_category.tree"]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                response = responseObject as! NSDictionary
                self.getcategories(response: response)
                SwiftLoader.hide()
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    print(error as Any)
                    SwiftLoader.hide()
                    self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )

            })
        }
            
        else
        {
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
}

extension Notification.Name {
    static let reload = Notification.Name("reload")
}


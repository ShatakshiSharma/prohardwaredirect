//
//  PrivacyPolicyViewController.swift
//  ToDool
//
//  Created by Yogita on 05/10/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

class PrivacyPolicyViewController : UIViewController,UIGestureRecognizerDelegate
{
    var panGesture : UIPanGestureRecognizer?
    
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes = [NSFontAttributeName: UIFont (name: "Quicksand-Bold", size: 17)!];
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal);
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_register" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        
        self.termsaAndConditionsWebservice()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 18)!, NSForegroundColorAttributeName    : UIColor.white ]
        self.navigationController!.navigationBar.barStyle = UIBarStyle.default
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = navigationColor
        self.navigationController!.navigationBar.isTranslucent = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func leftNavigationBarButtonClicked( )
    {
        _ = ( self.frostedViewController.contentViewController as? UINavigationController )?.popToRootViewController(animated: true)
        self.view.endEditing(true)
    }
    
    
    func panGestureRecognized(sender : UIPanGestureRecognizer)
    {
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    func termsaAndConditionsWebservice()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(animated: true)
            let dict = ["identifier": "privacy-policy"]
            let paramArray: [Any] = ["custom_api.cmspagecontent",dict]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                let response = responseObject as! NSArray
                SwiftLoader.hide()
                let fileUrl = Foundation.URL(string: MAGENTO_BASE_URL)
                self.webView.loadHTMLString((response.object(at: 0) as AnyObject).value(forKey: "content") as! String, baseURL: fileUrl)
                
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    SwiftLoader.hide()
                    self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
        
    }}


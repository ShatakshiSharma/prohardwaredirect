//
//  LoginViewController.swift
//  ToDool
//
//  Created by Pranav on 9/13/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class LoginViewController : UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate
{
    @IBOutlet weak var email : UITextField!
    @IBOutlet weak var password : UITextField!
    @IBOutlet weak var login : UIButton!
    @IBOutlet weak var forgotPassword : UIButton!
    @IBOutlet weak var expressCheckoutBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var topDistance : NSLayoutConstraint!
    var panGesture : UIPanGestureRecognizer?
    
    //MARK:------ViewWillAppear--------
 
    override func viewWillAppear(_ animated: Bool) {

        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 18)!, NSForegroundColorAttributeName    : UIColor.white ]
        self.navigationController!.navigationBar.barStyle = UIBarStyle.default
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = UIColor.clear
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        
        self.clearTextFields()
        }
    
    //MARK:------ViewDidLoad--------

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_register" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        let hideKeyboardOnTapGesture = UITapGestureRecognizer( target:  self, action: #selector(LoginViewController.hideKeyboardOnTap(_:)) )
        self.view.addGestureRecognizer( hideKeyboardOnTapGesture )
        
        // Set- TextFieldBorder color & Radius
        Utility.textFieldBorderColor(self.email,color: textfieldLineWhiteColor!)
        Utility.textFieldBorderColor(self.password,color: textfieldLineWhiteColor!)
        
        //-----------Add Padding-----------//
        Utility.addPaddingToTextField(textfield: self.email)
        Utility.addPaddingToTextField(textfield: self.password)
   
        self.addButtonBorder(button: self.registerBtn)
        
        self.addClearButton()
        
    }
    
    
    
    func leftNavigationBarButtonClicked( )
    {
        _ = ( self.frostedViewController.contentViewController as? UINavigationController )?.popToRootViewController(animated: true)
        self.view.endEditing(true)
    }
    
    
    //MARK:------Validation--------

        func validateLoginFields() ->Bool
        {
            let success = true
        
        if ( Utility.isEmpty(self.email.text!) )
        {
         self.view.makeToast(message: k_email_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
         return false;
        }
        else if (!Utility.validateEmail( self.email.text! ))
        {
        self.view.makeToast(message: k_email_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
        return false;
        }
        
        else if ( Utility.isEmpty(self.password.text!) )
        {
            self.view.makeToast(message: k_pass_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!Utility.validatePassword( self.password.text! ))
        {
            self.view.makeToast(message: k_pass_length, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        return success;
 
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
        
        if(self.keyboardIsShowing)
        {
            self.arrangeViewOffsetFromKeyboard()
        }
        
        
        if(textField == email)
        {
            email.rightViewMode = UITextFieldViewMode.always
        }
                
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    //MARK:------Keyboard Func--------

    func hideKeyboardOnTap( _ sender : UITapGestureRecognizer )
    {
        email.resignFirstResponder()
        password.resignFirstResponder()
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        self.keyboardIsShowing = true
        
        if let info = notification.userInfo {
            self.keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.keyboardAnimationTime = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            self.arrangeViewOffsetFromKeyboard()
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardIsShowing = false
        
        self.returnViewToInitialFrame()
    }
    
    func arrangeViewOffsetFromKeyboard()
    {
        if let tf = self.activeTextField
        {
            var frame = self.view.frame
            if let rect : CGRect = self.keyboardFrame
            {
                frame.size.height -= rect.size.height
            }
            
            let union = frame.union( tf.frame )
            var diff = union.size.height - frame.size.height
            if( diff > 0 )
            {
                diff += self.kPreferredTextFieldToKeyboardOffset
                self.topDistance.constant = 30 - diff
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: self.keyboardAnimationTime!, animations:  {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func returnViewToInitialFrame()
    {
        self.topDistance.constant = 30
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        });
    }
    
    var kPreferredTextFieldToKeyboardOffset: CGFloat = 20.0
    var keyboardFrame: CGRect?
    var keyboardAnimationTime : TimeInterval?
    var keyboardIsShowing: Bool = false
    weak var activeTextField: UITextField?
    
    
    
    //MARK:------IBAction--------
    
    @IBAction func loginClicked( _ sender : UIButton )
    {
        email.resignFirstResponder()
        password.resignFirstResponder()
        
        if (Reachability.isConnectedToNetwork() == true){
            if(self.validateLoginFields())
            {
                SwiftLoader.show(title: "Please wait..", animated: true)
                let dict = ["customeremail": self.email.text!,
                            "password": self.password.text!]
                
                let paramArray: [Any] = ["custom_api.customerlogin",dict]
                
                Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                    let response = responseObject as! NSString
                    print(response)
                    UserDefaults.standard.set(response, forKey: k_userID)
                    self.createCart()
                    NotificationCenter.default.post(name: .reload, object: nil)
                    SwiftLoader.hide()
                    ( self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true)
                    
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        print((error?.localizedDescription)!)
                        SwiftLoader.hide()
                        self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
                })
            }
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    @IBAction func forgotPasswordClicked( _ sender : UIButton )
    {
        if (Reachability.isConnectedToNetwork() == true)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
            ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
           }
    
    
    @IBAction func registerBtnClick(_ sender: UIButton) {
        if (Reachability.isConnectedToNetwork() == true) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
       
        let vc = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }

    }
    
    func createCart()
    {
        if (Reachability.isConnectedToNetwork() == true){
            let dict = ["customer_id": UserDefaults.standard.string(forKey: k_userID)]
            
            let paramArray: [Any] = ["custom_api.custquoteid",dict]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                let response = responseObject as! NSString
                print(response)
                UserDefaults.standard.set(response, forKey: k_quoteID)
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print((error?.localizedDescription)!)
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }


    //MARK:------ClearTextFields--------
    
    func clearTextFields() {
        self.email.text=""
        self.password.text=""
    }
    
    
    func addButtonBorder(button : UIButton)
    {
        button.layer.cornerRadius = 23
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
    }

    func addClearButton(){
        let emailbutton : UIButton = email.value(forKey: "_clearButton") as! UIButton
        emailbutton.setImage(UIImage( named: "textfieldCross" ), for: UIControlState.normal)
        let passbutton : UIButton = password.value(forKey: "_clearButton") as! UIButton
        passbutton.setImage(UIImage( named: "textfieldCross" ), for: UIControlState.normal)

    }
    
}

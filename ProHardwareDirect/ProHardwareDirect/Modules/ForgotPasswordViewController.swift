//
//  ForgotPasswordViewController.swift
//  ToDool
//
//  Created by Yogita on 27/09/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class ForgotPasswordViewController : UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var topDistance : NSLayoutConstraint!
    @IBOutlet weak var resetPswdBtn: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    //MARK:-----viewDidLoad---------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Forgot Password"
        
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(RegisterViewController.hideKeyboardOnTap(_:)) )
        self.view.addGestureRecognizer( gesture )
         self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_register" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        
        //-----------Textfield Border color-----------//
        Utility.textFieldBorderColor(self.emailTextField,color: UIColor(hex: 0xe6eaeb )!)
        
        //-----------Add Padding-----------//
        Utility.addPaddingToTextField(textfield: self.emailTextField)

        NotificationCenter.default.addObserver(self, selector: #selector(ForgotPasswordViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ForgotPasswordViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        for subview in self.view.subviews
        {
            if subview is UITextField
            {
                let textField = subview as! UITextField
                textField.addTarget(self, action: #selector(ForgotPasswordViewController.textFieldShouldReturn(_:)), for: UIControlEvents.editingDidEndOnExit)
                
                textField.addTarget(self, action: #selector(ForgotPasswordViewController.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingDidBegin)
            }
        }
        
            //Add Clear Button
            let emailButton : UIButton = emailTextField.value(forKey: "_clearButton") as! UIButton
            emailButton.setImage(UIImage( named: "textfieldCross" ), for: UIControlState.normal)
        

    }
    
    func hideKeyboardOnTap( _ gesture : UITapGestureRecognizer )
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
    }
    
    
    //MARK:-----Validations---------
    
    func validateEmail( ) -> Bool
    {
        if let text = self.emailTextField.text
        {
            return Utility.validateEmail( text )
        }
        return  false
    }
    
    //MARK:-----Keyborad Functions---------
    
    func keyboardWillShow(notification: NSNotification)
    {
        self.keyboardIsShowing = true
        
        if let info = notification.userInfo {
            self.keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.keyboardAnimationTime = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            self.arrangeViewOffsetFromKeyboard()
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardIsShowing = false
        self.returnViewToInitialFrame()
    }
    
    func arrangeViewOffsetFromKeyboard()
    {
        if let tf = self.activeTextField
        {
            var frame = self.view.frame
            if let rect : CGRect = self.keyboardFrame
            {
                frame.size.height -= rect.size.height
            }
            
            let union = frame.union( tf.frame )
            var diff = union.size.height - frame.size.height
            if( diff > 0 )
            {
                diff += self.kPreferredTextFieldToKeyboardOffset
                self.topDistance.constant = 10 - diff
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: self.keyboardAnimationTime!, animations:  {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func returnViewToInitialFrame()
    {
        self.topDistance.constant = 10
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        });
    }
    
    //MARK:-----TextFieldDelegates---------
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
        
        if(self.keyboardIsShowing)
        {
            self.arrangeViewOffsetFromKeyboard()
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if( textField == emailTextField )
        {
            if ( !Utility.isEmpty(emailTextField.text!) )
            {
                if( validateEmail( ) == true )
                {
                    return true
                }
                else
                {
                    self.view.makeToast(message: k_email_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
                    return false
                }
            }
            else
            {
                self.view.makeToast(message: k_email_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
                return false
            }
        }
        else
        {
            return false
        }
    }
    
    var kPreferredTextFieldToKeyboardOffset: CGFloat = 20.0
    var keyboardFrame: CGRect?
    var keyboardAnimationTime : TimeInterval?
    var keyboardIsShowing: Bool = false
    weak var activeTextField: UITextField?
    
    
    //MARK:-----IBAction---------
    
    func leftNavigationBarButtonClicked( )
    {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
         self.view.endEditing(true)
    }
    
    
    @IBAction func resetPswdBtnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        if (Reachability.isConnectedToNetwork() == true)
        {
        self.activeTextField = nil
        if (!Utility.isEmpty(emailTextField.text!))
        {
            if( validateEmail( ) == true )
            {
                SwiftLoader.show(title: "Please wait..", animated: true)

                let dict = ["email": self.emailTextField.text! as String]
                
                let paramArray: [Any] = ["custom_api.forgetpassword",dict]
                
                  Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                    SwiftLoader.hide()
                    print(responseObject as! Bool)
                    if(responseObject as! Bool == true)
                    {
                        let alertController = UIAlertController(title:nil, message: k_forgotPassLink, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                           ( self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                    {
                     self.view.makeToast(message: k_emailid_notExist , duration: 3.0, position: HRToastPositionTop as AnyObject )
                    }
                   
                    
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        print((error?.localizedDescription)!)
                        SwiftLoader.hide()
                        self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
                })

                
                }
            else
            {
                self.view.makeToast(message: k_email_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            }
        }
        else
        {
            self.view.makeToast(message: k_email_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
        
        }
        
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
        
    }
    
}

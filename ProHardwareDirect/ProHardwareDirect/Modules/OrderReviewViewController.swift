//
//  OrderReviewViewController.swift
//  ToDool
//
//  Created by Yogita on 16/11/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//


import UIKit
import eWAYPaymentsSDK

class OrderReviewViewController : UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate
{

    
    @IBOutlet weak var discountImage: UIImageView!
    @IBOutlet weak var rewardsImage: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var discountValueLabel: UILabel!
    @IBOutlet weak var rewardsLabel: UILabel!
    @IBOutlet weak var rewardsValueLabel: UILabel!
    @IBOutlet weak var subTotalValueLabel: UILabel!
    @IBOutlet weak var ShippingValueLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var taxValueLabel: UILabel!
    @IBOutlet weak var billingAddressLabel: UILabel!
    @IBOutlet weak var billingAddressValueLabel: UILabel!
    @IBOutlet weak var shippingAddressLabel: UILabel!
    @IBOutlet weak var shippingAddressValueLabel: UILabel!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var payNowButton: UIButton!

    var appDelegate : AppDelegate!

    var arrCart : [NSDictionary]!
    var subtotalValue : String!
    var grandtotalValue : String!
    var taxValue : String!
    var shippingHandlingValue : String!
    var discountValue : String!
    var cashbackValue : String!
    var couponValue : String!
    var billingAddressValue : String!
    var shippingAddressValue : String!
    
    weak var activeTextField: UITextField?
    @IBOutlet var tableHeight : NSLayoutConstraint!

override func viewDidLoad() {
    
    super.viewDidLoad()
    appDelegate = UIApplication.shared.delegate as! AppDelegate

    self.title = "Order Review"
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
    self.navigationController!.interactivePopGestureRecognizer?.delegate = self
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage( named: "cross" ), style: .plain, target: self, action: #selector(self.rightNavigationBarButtonClicked) )
    
    tableView.register(UINib.init(nibName: "OrderReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderReviewTableViewCell")
    tableView.autoresizingMask = UIViewAutoresizing(rawValue:UIViewAutoresizing.flexibleWidth.rawValue | UIViewAutoresizing.flexibleHeight.rawValue)
    tableView.backgroundColor = UIColor.clear
    tableView.allowsMultipleSelectionDuringEditing = false
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 140
    
    Utility.addPaddingToTextField(textfield: self.couponTextField)
    Utility.textFieldBorderColor(self.couponTextField, color:  UIColor( hex : 0xDBDCDD )!)
    let attributes = [
        NSForegroundColorAttributeName: UIColor( hex : 0x303030 ),
        NSFontAttributeName : UIFont(name: "Quicksand-Regular", size: 12)!         ]
    self.couponTextField.attributedPlaceholder = NSAttributedString(string: "Add coupon", attributes:attributes)
    
    refreshData()
    
    Utility.addPaddingToTextField(textfield: self.commentTextField)
    Utility.textFieldBorderColor(self.commentTextField, color:  UIColor( hex : 0xDBDCDD )!)
    self.commentTextField.attributedPlaceholder = NSAttributedString(string: "Leave Comment", attributes:attributes)

    //Gesture
    let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(self.hideKeyboardOnTap(_:)) )
    gesture.delegate = self
    self.view.addGestureRecognizer( gesture )
}

    override func viewWillAppear(_ animated: Bool) {
    
    }
    
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        self.tableHeight.constant = tableView.contentSize.height
    
    }
    
    func refreshData()  {
        subTotalValueLabel.text = subtotalValue
        grandTotalLabel.text = grandtotalValue
        discountValueLabel.text = discountValue
        ShippingValueLabel.text = shippingHandlingValue
        taxValueLabel.text = taxValue
        rewardsValueLabel.text = cashbackValue
        couponTextField.text = couponValue
    }
    
    func getCart()
    {
        let paramArray: [Any] = ["cart.info", UserDefaults.standard.string(forKey: k_quoteID)!]
        
        Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
            print(responseObject!)
            
            if (((responseObject as! NSDictionary).value(forKey: "items") as! NSArray).count > 0 ){
                
                self.grandtotalValue =  "$" + String(format: "%.2f", ((responseObject as! NSDictionary).value(forKey: "subtotal_with_discount") as AnyObject).floatValue)
                self.subtotalValue =  "$" + String(format: "%.2f", ((responseObject as! NSDictionary).value(forKey: "subtotal") as AnyObject).floatValue)
                self.taxValue =  "$" + String(format: "%.2f", ((((responseObject as! NSDictionary).value(forKey: "shipping_address") as! NSDictionary).value(forKey: "tax_amount")) as AnyObject).floatValue)
                self.couponValue = ((responseObject as! NSDictionary).value(forKey: "coupon_code") as! String)
                
                self.refreshData()
                
                let products = ((responseObject as! NSDictionary).value(forKey: "items") as! NSArray) as Array
                let attributes = ["msrp", "price", "color", "size", "image"]
                let calls = NSMutableArray()
                for product in products {
                    calls.add(["catalog_product_attribute_media.list", product.value(forKey: "product_id") as! NSString])
                    calls.add(["catalog_product.info", product.value(forKey: "product_id") as! NSString, 1, attributes])
                }
                Magento.multiCall((calls as NSArray) as! [Any], success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                    var newProductParts : NSArray
                    newProductParts=responseObject as! NSArray
                    
                    (products as NSArray).enumerateObjects({ ( product, index, stop) in
                        var predicate: NSPredicate?
                        predicate = NSPredicate(format: "exclude == '0'")
                        if(((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject != nil){
                            let image : NSDictionary = ((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject as! NSDictionary
                            (product as! NSMutableDictionary).setObject(image.object(forKey: "url") as Any, forKey: "image" as  NSCopying)
                        }
                        (product as! NSMutableDictionary).addEntries(from: newProductParts[index+index+1] as! [AnyHashable : Any])
                    })
                    SwiftLoader.hide( )
                    self.arrCart = (products  as! [NSDictionary])
                    print(self.arrCart)
                    
                    self.scrollView.isHidden = false
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                    self.tableView.reloadData()
                    self.updateViewConstraints()
                    
                }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                    self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                    SwiftLoader.hide()
                })
            }
            else{
                SwiftLoader.hide()
                self.scrollView.isHidden = true
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
            print("errorrr:",(error?.localizedDescription)!)
            SwiftLoader.hide()
            self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
        })
    }

    func emptyCart()
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Deleting..", animated: true)
            
            let arrProducts = NSMutableArray()
            var object : NSDictionary!
            for index in 0...(arrCart.count)-1 {
                object = ["product_id": ((arrCart[index]).value(forKey: "product_id"))!,
                          "sku": ((arrCart[index]).value(forKey: "sku"))!,
                          "qty": ((arrCart[index]).value(forKey: "qty"))!,
                          "options" : "",
                          "bundle_option" : "",
                          "bundle_option_qty" : "",
                          "links" : ""]
                arrProducts.add(object)
            }
            let paramArray: [Any] = ["cart_product.remove", UserDefaults.standard.string(forKey: k_quoteID)!,arrProducts]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                SwiftLoader.hide()
                
                if responseObject as! Bool == true{
                    (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
                }
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func getShippingList() {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Loading..", animated: true)

            let paramArray: [Any] = ["cart_shipping.list", UserDefaults.standard.string(forKey: k_quoteID)!]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject!)
                SwiftLoader.hide()
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    func setShippingMethod() {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Loading..", animated: true)
            
            let paramArray: [Any] = ["cart_shipping.method", UserDefaults.standard.string(forKey: k_quoteID)!, "freeshipping_freeshipping"]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject!)
                SwiftLoader.hide()
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func getPaymentList() {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Loading..", animated: true)
            
            let paramArray: [Any] = ["cart_payment.list", UserDefaults.standard.string(forKey: k_quoteID)!]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject!)
                SwiftLoader.hide()
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
    
    func setPaymentMethod() {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Loading..", animated: true)
            
            var object : NSDictionary!
            object = ["po_number": "",
                      "method": "ewayrapid_ewayone",
                      "cc_cid": "123",
                      "cc_owner" : "Deepika KOundal",
                      "cc_number" : "4444333322221111",
                      "cc_type" : "VI",
                      "cc_exp_year" : "2022",
                      "cc_exp_month" : "10"]
            
            let paramArray: [Any] = ["cart_payment.method", UserDefaults.standard.string(forKey: k_quoteID)!,object]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject!)
                self.createOrder()
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    func createOrder() {
        if (Reachability.isConnectedToNetwork() == true){
            
            let paramArray: [Any] = ["cart.order", UserDefaults.standard.string(forKey: k_quoteID)!]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject!)
                SwiftLoader.hide()
                
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }

    
    func makePayment() {
       let arrNVpair = NSMutableArray()
        
        let nvpair1 = NVpair()
        nvpair1.name = "cards"
        nvpair1.value = "4444333322221111"
        
        let nvpair2 = NVpair()
        nvpair2.name = "CVN"
        nvpair2.value = "123"
        
        arrNVpair.add(nvpair1)
        arrNVpair.add(nvpair2)
        
        RapidAPI.encryptValues(arrNVpair as Any as! [Any], completed: { (encryptValuesResponse: EncryptValuesResponse?) in
            if (encryptValuesResponse?.status == Success) {
                
                let values = NSMutableArray()
                for nv in (encryptValuesResponse?.values)! {
                    values.add(nv)
                }
                SwiftLoader.hide()
                print(values)
            }
        })
    }
    
    // MARK:--------IBAction-------
    
    func leftNavigationBarButtonClicked() {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
    }
    
    func rightNavigationBarButtonClicked() {
        let alert = UIAlertController(title: "", message: "Are you sure you want to cancel order?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            if (UserDefaults.standard.object(forKey: k_quoteID) != nil){
                self.emptyCart()
            }
            else{
                UserDefaults.standard.removeObject(forKey: "cartCount")
                UserDefaults.standard.removeObject(forKey: "cartArray")
                NotificationCenter.default.post(name: .cartCount, object: nil)
                (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }

    // MARK:----Textfield delegates----

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
    }

    func hideKeyboardOnTap( _ gesture : UITapGestureRecognizer )
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
    }
    
    
    func configure( cell : OrderReviewTableViewCell, indexPath : NSIndexPath )
    {
        let product = arrCart[indexPath.row]
        
        cell.productName.text = product.value(forKey: "name") as! String?
        cell.productNumber.text = product.value(forKey: "sku") as! String?
        cell.qtyTextField.text = String(product.value(forKey: "qty") as! Int)
        cell.originalPrice.text = "$" + String(format: "%.2f", (product.value(forKey: "price")as AnyObject).floatValue)
        cell.earningLabel.text = ""
        cell.discountPrice.text = "$" + String(format: "%.2f", (product.value(forKey: "row_total") as AnyObject).floatValue)
        if ((product.value(forKey: "image") != nil) && !(product.value(forKey: "image") as! String == "product1" ))
        {
            cell.productImage.sd_setImage( with : URL( string : (product.value(forKey: "image") as! String )), placeholderImage : UIImage( named : "product1")!)
        }
        
        self.updateViewConstraints()
    }

 
    @IBAction func termsButtonClick(_ sender: UIButton) {
        
        self.view.endEditing(true)
        switch sender.tag {
        case 100:
            self.checkImage.image = UIImage( named: "checkbox_myprofile")
            self.payNowButton.backgroundColor = UIColor( hex : 0x252525 )
            self.payNowButton.isUserInteractionEnabled = true
            sender.tag = 200
            break
            
        case 200:
            self.checkImage.image = UIImage( named: "uncheck_myprofile")
            self.payNowButton.backgroundColor = UIColor( hex : 0x676767 )
            self.payNowButton.isUserInteractionEnabled = false
            sender.tag = 100
            break
            
        default:
            break
        }
    }
    
    @IBAction func applyBtnClick(_ sender: UIButton) {
        if (Reachability.isConnectedToNetwork() == true){
            self.activeTextField?.resignFirstResponder()
            SwiftLoader.show(title: "Applying Coupon..", animated: true)
            
            let paramArray: [Any] = ["cart_coupon.add", UserDefaults.standard.string(forKey: k_quoteID)!,couponTextField.text!]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                print(responseObject as! Bool)
                self.getCart()
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                print("errorrr:",(error?.localizedDescription)!)
                SwiftLoader.hide()
                self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
            })
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
   
    @IBAction func paynowButtonClick(_ sender: UIButton) {
        setPaymentMethod()
    }
    
    @IBAction func billingAddressBtnClick(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
        vc.isCart = true
        vc.isBillingAdded = true
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    @IBAction func shippingAddressBtnClick(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShippingAddressViewController") as! ShippingAddressViewController
        vc.isCart = true
        vc.isShippingAdded = true
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    @IBAction func discountBtnClick(_ sender: UIButton) {
            self.view.endEditing(true)
            self.discountLabel.textColor = UIColor( hex : 0x303030 )
            self.discountValueLabel.textColor = UIColor( hex : 0x303030 )
            self.rewardsLabel.textColor = UIColor( hex : 0xb2b2b2 )
            self.rewardsValueLabel.textColor = UIColor( hex : 0xb2b2b2 )
            self.discountImage.image = UIImage( named: "radio_selected")
            self.rewardsImage.image = UIImage( named: "radio_button_unselected")
    }
  
    @IBAction func rewardsBtnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        self.discountLabel.textColor = UIColor( hex : 0xb2b2b2 )
        self.discountValueLabel.textColor = UIColor( hex : 0xb2b2b2 )
        self.rewardsLabel.textColor = UIColor( hex : 0x303030 )
        self.rewardsValueLabel.textColor = UIColor( hex : 0x303030 )
        self.discountImage.image = UIImage( named: "radio_button_unselected")
        self.rewardsImage.image = UIImage( named: "radio_selected")
    }
}

extension OrderReviewViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "OrderReviewTableViewCell"
        let cell: OrderReviewTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderReviewTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        configure( cell: cell, indexPath:  indexPath as NSIndexPath )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        
        vc.quantityCount = self.arrCart[ indexPath.row ].value(forKey: "qty") as! Int
        vc.isCartItem = true
        vc.index = indexPath.row
        vc.productDict = self.arrCart[ indexPath.row ]
        vc.title = (self.arrCart[ indexPath.row ]).value(forKey: "name") as! String?
        for i in 0...self.appDelegate.appDelegateHomeData.children.count-1 {
            if (self.appDelegate.appDelegateHomeData.children[i].name == "Brands"){
                vc.brands = self.appDelegate.appDelegateHomeData.children[i].children
            }
        }
        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

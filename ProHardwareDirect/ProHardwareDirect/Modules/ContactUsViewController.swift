//
//  ContactUsViewController.swift
//  ProHardwareDirect
//
//  Created by Yogita on 29/12/16.
//  Copyright © 2016 Mobrill. All rights reserved.
//

class ContactUsViewController : UIViewController
{
    var panGesture : UIPanGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes = [NSFontAttributeName: UIFont (name: "Quicksand-Bold", size: 17)!];
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal);
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_register" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        
        panGesture = UIPanGestureRecognizer()
        panGesture = UIPanGestureRecognizer(target: self, action:#selector(self.panGestureRecognized(sender:)))
        self.view.addGestureRecognizer(panGesture!)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Quicksand-Regular", size: 18)!, NSForegroundColorAttributeName    : UIColor.white ]
        self.navigationController!.navigationBar.barStyle = UIBarStyle.default
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = navigationColor
        self.navigationController!.navigationBar.isTranslucent = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func leftNavigationBarButtonClicked( )
    {
        _ = ( self.frostedViewController.contentViewController as? UINavigationController )?.popToRootViewController(animated: true)
        self.view.endEditing(true)
    }
    
    
    func panGestureRecognized(sender : UIPanGestureRecognizer)
    {
        self.frostedViewController.panGestureRecognized(sender)
    }
    
}

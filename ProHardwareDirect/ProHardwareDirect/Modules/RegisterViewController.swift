//
//  RegisterViewController.swift
//  ToDool
//
//  Created by Pranav on 9/13/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit


class RegisterViewController : UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var firstName : UITextField!
    @IBOutlet weak var lastName : UITextField!
    @IBOutlet weak var email : UITextField!
    @IBOutlet weak var password : UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var confirmPassword : UITextField!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var pswdView: UIView!
    @IBOutlet weak var confirmPswdView: UIView!
    @IBOutlet weak var topDistance : NSLayoutConstraint!
    @IBOutlet weak var confirmPswdShowBtn: UIButton!
    @IBOutlet weak var pswdShowBtn: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var vc : UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Register"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_register" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Save", style:.plain, target:self, action:#selector(saveClicked))
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer( target:  self, action: #selector(RegisterViewController.hideKeyboardOnTap(_:)) )
        self.contentView.addGestureRecognizer( gesture )
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        for subview in self.contentView.subviews
        {
            if subview is UITextField
            {
                let textField = subview as! UITextField
                textField.addTarget(self, action: #selector(RegisterViewController.textFieldShouldReturn(_:)), for: UIControlEvents.editingDidEndOnExit)
                
                textField.addTarget(self, action: #selector(RegisterViewController.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingDidBegin)
            }
        }
        
        //Set - UItextfield Border color & Radius
        Utility.textFieldBorderColor(self.firstName,color: textfieldLineWhiteColor!)
        Utility.textFieldBorderColor(self.lastName,color: textfieldLineWhiteColor!)
        Utility.textFieldBorderColor(self.mobileTextField,color: textfieldLineWhiteColor!)
        Utility.textFieldBorderColor(self.email,color: textfieldLineWhiteColor!)
        Utility.textFieldBorderColor(self.pswdView,color: textfieldLineWhiteColor!)
        Utility.textFieldBorderColor(self.confirmPswdView,color: textfieldLineWhiteColor!)
        
        //-----------Add Padding-----------//
        Utility.addPaddingToTextField(textfield: self.firstName)
        Utility.addPaddingToTextField(textfield: self.lastName)
        Utility.addPaddingToTextField(textfield: self.email)
        Utility.addPaddingToTextField(textfield: self.mobileTextField)
        
        
        //Check image round
        checkImage.layer.borderWidth = 0
        checkImage.layer.masksToBounds = false
        checkImage.layer.cornerRadius = 3.0
        checkImage.clipsToBounds=true
        
        //Add ClearButton
        self.addClearButton()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func leftNavigationBarButtonClicked( )
    {
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
        self.view.endEditing(true)
    }
    
    
    func validateSignupFields() ->Bool
    {
        let success = true
        
        if ( Utility.isEmpty(self.firstName.text!) )
        {
            self.view.makeToast(message: k_firstname_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(((self.firstName.text?.characters.count)! >= 2) && ((self.firstName.text?.characters.count)! <= 35)))
        {
            self.view.makeToast(message: k_firstname_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.lastName.text!) )
        {
            self.view.makeToast(message: k_lastname_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(((self.lastName.text?.characters.count)! >= 2) && ((self.lastName.text?.characters.count)! <= 35)))
        {
            self.view.makeToast(message: k_lastname_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.mobileTextField.text!) )
        {
            self.view.makeToast(message: k_mob_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!((self.mobileTextField.text?.characters.count)! == 14))
        {
            self.view.makeToast(message: k_mob_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        if ( Utility.isEmpty(self.email.text!) )
        {
            self.view.makeToast(message: k_email_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(Utility.validateEmail( self.email.text!)))
        {
            self.view.makeToast(message: k_email_Invalid, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
            
        else if ( Utility.isEmpty(self.password.text!) )
        {
            self.view.makeToast(message: k_pass_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(Utility.validatePassword( self.password.text! )))
        {
            self.view.makeToast(message: k_pass_length, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
            
        else if ( Utility.isEmpty(self.password.text!) )
        {
            self.view.makeToast(message: k_pass_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(Utility.validatePassword( self.password.text! )))
        {
            self.view.makeToast(message: k_pass_length, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
            
        else if ( Utility.isEmpty(self.confirmPassword.text!) )
        {
            self.view.makeToast(message: k_pass_2_Empty, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        else if (!(self.confirmPassword.text == self.password.text))
        {
            self.view.makeToast(message: k_pass_Match, duration: 3.0, position: HRToastPositionTop as AnyObject )
            return false;
        }
        
        return success;
        
    }
    
    
    
    @IBAction func agreeBtnClick(_ sender: UIButton) {
        switch sender.tag {
        case 300:
            self.checkImage.image = UIImage( named: "checkbox_register")
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            sender.tag = 700
            break
            
        case 700:
            self.checkImage.image = UIImage( named: "uncheck_register")
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            sender.tag = 300
            break
            
        default:
            break
        }
    }
    
    @IBAction func PswdShowBtnClick(_ sender: UIButton) {
        switch sender.tag {
        case 100:
            self.password.isSecureTextEntry = false
            self.pswdShowBtn.setTitle("Hide", for: UIControlState.normal)
            sender.tag = 500
            break
            
        case 500:
            self.password.isSecureTextEntry = true
            self.pswdShowBtn.setTitle("Show", for: UIControlState.normal)
            sender.tag = 100
            break
            
        default:
            break
        }
    }
    
    @IBAction func confirmPswdBtnClick(_ sender: UIButton) {
        switch sender.tag {
        case 200:
            self.confirmPassword.isSecureTextEntry = false
            self.confirmPswdShowBtn.setTitle("Hide", for: UIControlState.normal)
            sender.tag = 600
            break
            
        case 600:
            self.confirmPassword.isSecureTextEntry = true
            self.confirmPswdShowBtn.setTitle("Show", for: UIControlState.normal)
            sender.tag = 200
            break
            
        default:
            break
        }
        
    }
    
    func saveClicked( )
    {
        if (Reachability.isConnectedToNetwork() == true)
        {
            if (self.activeTextField != nil)
            {
                self.activeTextField?.resignFirstResponder()
                self.activeTextField = nil
            }
            if(self.validateSignupFields())
            {
            SwiftLoader.show(title: "Please wait..", animated: true)

             let dict = ["email": self.email.text! as String,
                            "password": self.password.text! as String ,
                            "firstname": self.firstName.text! as String,
                            "lastname": self.lastName.text! as String,
                            "mobile" : self.mobileTextField.text! as String,
                            "website_id": 1,
                            "store_id": 1] as [String : Any]
                
                let paramArray: [Any] = ["customer.create",dict]
                Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                    let responseId = responseObject as! String
                    print(responseId)
                    SwiftLoader.hide()
                    let alertController = UIAlertController(title:nil, message: "Registered Successfully", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true)
                    }
                    alertController.addAction(OKAction)
                    
                    self.present(alertController, animated: true, completion:nil)
                    
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        print((error?.localizedDescription)!)
                        SwiftLoader.hide()
                        self.view.makeToast(message: (error?.localizedDescription)! , duration: 3.0, position: HRToastPositionTop as AnyObject )
                })
                
            }
        }
        else{
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
        
    }
    
    func hideKeyboardOnTap( _ gesture : UITapGestureRecognizer )
    {
        if (self.activeTextField != nil)
        {
            self.activeTextField?.resignFirstResponder()
            self.activeTextField = nil
        }
    }
    

    
    func keyboardWillShow(notification: NSNotification)
    {
        self.keyboardIsShowing = true
        
        if let info = notification.userInfo {
            self.keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.keyboardAnimationTime = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            self.arrangeViewOffsetFromKeyboard()
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardIsShowing = false
        
        self.returnViewToInitialFrame()
    }
    
    func arrangeViewOffsetFromKeyboard()
    {
        if let tf = self.activeTextField
        {
            var frame = self.view.frame
            if let rect : CGRect = self.keyboardFrame
            {
                frame.size.height -= rect.size.height
            }
            var rect = tf.frame
            if tf === self.password || tf === self.confirmPassword
            {
                rect = (tf.superview?.frame)!
            }
            let union = frame.union( rect )
            var diff = union.size.height - frame.size.height
            if( diff > 0 )
            {
                diff += self.kPreferredTextFieldToKeyboardOffset
                self.topDistance.constant = 50 + diff
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: self.keyboardAnimationTime!, animations:  {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func returnViewToInitialFrame()
    {
        self.topDistance.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        });
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
        
        if(self.keyboardIsShowing)
        {
            self.arrangeViewOffsetFromKeyboard()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return false
    }
    
    var kPreferredTextFieldToKeyboardOffset: CGFloat = 20.0
    var keyboardFrame: CGRect?
    var keyboardAnimationTime : TimeInterval?
    var keyboardIsShowing: Bool = false
    weak var activeTextField: UITextField?
    
    func addClearButton(){
        let firstNamebutton : UIButton = firstName.value(forKey: "_clearButton") as! UIButton
        firstNamebutton.setImage(UIImage( named: "textfieldCross" ), for: UIControlState.normal)
        let lastNamebutton : UIButton = lastName.value(forKey: "_clearButton") as! UIButton
        lastNamebutton.setImage(UIImage( named: "textfieldCross" ), for: UIControlState.normal)
        let emailbutton : UIButton = email.value(forKey: "_clearButton") as! UIButton
        emailbutton.setImage(UIImage( named: "textfieldCross" ), for: UIControlState.normal)
        let mobilebutton : UIButton = mobileTextField.value(forKey: "_clearButton") as! UIButton
        mobilebutton.setImage(UIImage( named: "textfieldCross" ), for: UIControlState.normal)
    }
}

//
//  CategoryViewController.swift
//  ToDool
//
//  Created by Pranav on 9/12/16.
//  Copyright © 2016 Wildnet Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData


class CategoryViewController : UIViewController, UISearchBarDelegate
{
    @IBOutlet weak var tableView : UITableView!
    var categoryID : Int64!
    var childrens : [DataObject]!
    
    override func viewWillAppear(_ animated: Bool) {
        self.searchController?.searchBar.tintColor = UIColor.white
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0 )
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBarConfig()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage( named: "leftarrow_productlist" ), style: .plain, target: self, action: #selector(self.leftNavigationBarButtonClicked) )
        UIApplication.shared.statusBarStyle = .lightContent
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = true
        tableView.estimatedRowHeight = 83.5
        self.tableView.register( UINib.init( nibName : "CategoryTableViewCell", bundle : nil ), forCellReuseIdentifier: "CategoryTableViewCell" )
    }
    
    
    func leftNavigationBarButtonClicked() {
        self.view.endEditing(true)
        (self.frostedViewController.contentViewController as! UINavigationController ).popViewController(animated: true );
    }

    fileprivate var _lastY : CGFloat?
    fileprivate var searchController : UISearchController?
}


extension CategoryViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
        if(indexPath.row == 0)
        {
        cell.bounds = CGRect(x: 0 , y: cell.bounds.origin.y, width : cell.bounds.size.width, height : cell.bounds.size.height )
        cell.name.text = childrens[indexPath.section].name
        }
        else
        {
        cell.bounds = CGRect(x: -20 , y: cell.bounds.origin.y, width : cell.bounds.size.width, height : cell.bounds.size.height )
        cell.name.text = childrens[indexPath.section].children[indexPath.row-1].name
        }

        cell.selectionStyle = .none
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.childrens.count

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.childrens[section].children.count+1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    
        if(indexPath.row == 0 || self.childrens[indexPath.section].children[indexPath.row-1].children.count == 0)
        {
            if indexPath.row == 0
            {
                if self.childrens[indexPath.section].children.count == 0 {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
                    vc.categoryId = self.childrens[indexPath.section].identifier
                    vc.title = self.childrens[indexPath.section].name
                    ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
                }
            }
                else{
                    if self.childrens[indexPath.section].children[indexPath.row-1].children.count == 0 {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
                        vc.categoryId = self.childrens[indexPath.section].children[indexPath.row-1].identifier
                        vc.title = self.childrens[indexPath.section].children[indexPath.row-1].name
                        ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
                }
            }
        }
        
        else
        {
            if (Reachability.isConnectedToNetwork() == true) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                vc.categoryID = self.childrens[indexPath.section].children[indexPath.row-1].identifier
                vc.childrens = self.childrens[indexPath.section].children[indexPath.row-1].children
                vc.title = self.childrens[indexPath.section].children[indexPath.row-1].name
                
                ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
            }
            else{
                self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
            }
        }
    }
    
    // Searchbar delegates
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchController?.searchBar.text = ""
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchProductsWebService(searchString: searchBar.text!)
    }

    func searchBarConfig()
    {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.dimsBackgroundDuringPresentation = false
        searchController?.searchBar.barTintColor = UIColor.black
        searchController?.searchBar.backgroundColor = UIColor.black
        searchController?.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController?.searchBar
        self.searchController?.searchBar.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0);
        self.searchController?.searchBar.barStyle = .default;
        self.searchController?.searchBar.searchBarStyle = .minimal;
        self.searchController?.searchBar.setSearchFieldBackgroundImage( UIImage( color : UIColor.white, size : CGSize( width : 1, height : 15 ) ), for: UIControlState.normal )
        let textFieldInsideSearchBar =  self.searchController?.searchBar.value(forKey: "searchField") as? UITextField
        self.searchController?.searchBar.setSearchFieldBackgroundImage( UIImage( color : UIColor.white, size : CGSize( width : 1, height : 15 ) ), for: UIControlState.normal )
        textFieldInsideSearchBar?.textColor = UIColor.darkGray
        textFieldInsideSearchBar?.background = UIImage()
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        textFieldInsideSearchBar?.layer.cornerRadius = 5.0
        textFieldInsideSearchBar?.clipsToBounds = true
        textFieldInsideSearchBar?.tintColor = UIColor.black
        textFieldInsideSearchBar?.font  = UIFont (name: "Quicksand-Regular", size: 15);
        definesPresentationContext = true
        
        searchController?.searchBar.delegate=self
    }
    
    func searchProductsWebService( searchString: String)
    {
        if (Reachability.isConnectedToNetwork() == true){
            SwiftLoader.show(title: "Searching...", animated: true)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
            
            let dict = ["page_no": 1, "count": "50", "categoryid" : String(format:"%d", categoryID!), "serachterm" : searchString] as [String : Any]
            let paramArray: [Any] = ["custom_api.productsearchbycat",dict]
            
            Magento.call(paramArray, success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                
                if (responseObject != nil){
                    let products = (responseObject as! NSArray) as Array
                    let attributes = ["msrp", "price", "color", "size", "image"]
                    let calls = NSMutableArray()
                    for product in products {
                        calls.add(["catalog_product_attribute_media.list", product.value(forKey: "product_id") as! NSString])
                        calls.add(["catalog_product.info", product.value(forKey: "product_id") as! NSString, 1, attributes])
                    }
                    Magento.multiCall((calls as NSArray) as! [Any], success: { (operation: AFHTTPRequestOperation?, responseObject: Any?) in
                        var newProductParts : NSArray
                        newProductParts=responseObject as! NSArray
                        
                        (products as NSArray).enumerateObjects({ ( product, index, stop) in
                            var predicate: NSPredicate?
                            predicate = NSPredicate(format: "exclude == '0'")
                            if(((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject != nil){
                                let image : NSDictionary = ((newProductParts[index+index] as AnyObject).filtered(using: predicate!) as NSArray).lastObject as! NSDictionary
                                (product as! NSMutableDictionary).setObject(image.object(forKey: "url") as Any, forKey: "image" as  NSCopying)
                            }
                            (product as! NSMutableDictionary).addEntries(from: newProductParts[index+index+1] as! [AnyHashable : Any])
                        })
                        
                        SwiftLoader.hide( )
                        self.searchController?.searchBar.setShowsCancelButton(false, animated: true)
                        self.searchController?.searchBar.text = ""
                        self.view.endEditing(true)
                        
                        if products.count>0{
                            vc.arrayProducts = products
                            vc.totalCountSearch = (((vc.arrayProducts[0]as! NSDictionary).value(forKey: "totalcount") as! NSInteger))
                            vc.searchString = searchString
                            vc.categoryId = self.categoryID
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            for i in 0...appDelegate.appDelegateHomeData.children.count-1 {
                                
                                if (appDelegate.appDelegateHomeData.children[i].name == "Brands"){
                                    vc.brands = appDelegate.appDelegateHomeData.children[i].children
                                }
                            }
                            ( self.frostedViewController.contentViewController as! UINavigationController ).pushViewController( vc, animated: true )
                        }
                        else{
                            self.view.makeToast(message: "No product found!", duration: 3.0, position: HRToastPositionTop as AnyObject )
                        }

                        
                    }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                        self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                        SwiftLoader.hide()
                        
                    })
                }
            }, failure: { (operation: AFHTTPRequestOperation?, error: Error?) in
                self.view.makeToast(message: (error?.localizedDescription)!, duration: 3.0, position: HRToastPositionTop as AnyObject )
                SwiftLoader.hide()
            })
        }
        else{
            SwiftLoader.hide()
            self.view.makeToast(message: k_network_Error, duration: 3.0, position: HRToastPositionTop as AnyObject )
        }
    }
}

